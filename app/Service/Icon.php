<?php

namespace App\Service;

use Illuminate\Support\Str;

class Icon
{
    protected static $icons = [];

    protected static $sprite = [];

    public static function &getSprite()
    {
        return self::$sprite;
    }

    public static function renderError($path, $icon, $error)
    {
        $error = "[ERROR] @icon($path, $icon): {$error}";

        return "<div style=\"display:none\">
          <!-- $error --><script>console.error('$error')</script>
        </div>";
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public static function render($path, $icon, $size = 24, $attributes = [])
    {
        $size = $size ?? 24;

        $cacheKey = str_replace('/', '_', $path);
        $cacheKey = "{$cacheKey}--{$icon}--{$size}";

        $cachePath = storage_path('app/public/cached-icons');

        if (empty(self::$icons[ $cacheKey ])) {
            if (file_exists("{$cachePath}/{$cacheKey}-icon.svg")) {
                self::$icons[ $cacheKey ] = file_get_contents(
                    "{$cachePath}/{$cacheKey}-icon.svg"
                );

                self::$sprite[ $cacheKey ] = file_get_contents(
                    "{$cachePath}/{$cacheKey}-symbol.svg"
                );
            }

            $iconFile = Str::endsWith($icon, '.svg')
                ? $icon : "{$icon}.svg";

            $iconPath = base_path("icons/{$path}/{$iconFile}");

            if (!file_exists($iconPath)) {
                return self::renderError($path, $icon, "not found");
            }

            $iconContent = file_get_contents($iconPath);

            $iconContent = preg_replace('/<!--[.\n]+?-->/', '', $iconContent);
            $iconContent = preg_replace('/<\?xml.+?>/', '', $iconContent);
            $iconContent = preg_replace('/<\!DOCTYPE.+?>/', '', $iconContent);
            $iconContent = preg_replace('/>[\n\s]+</', '><', $iconContent);

            $captured = preg_match('/<svg(.+?)>/', $iconContent, $attrs);

            $svgAttrs = [];
            $symbolAttrs = [];

            if (false === $captured || count($attrs) < 2) {
                return self::renderError(
                    $path, $icon, 'when capturing attributes'
                );
            }

            array_shift($attrs);
            $attrs = trim(implode('', $attrs));

            $attrKeys = preg_split(
                '/(.+?)=".+?"/',
                $attrs,
                -1,
                PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
            );

            $attrVals = preg_split(
                '/.+?="(.+?)"/',
                $attrs,
                -1,
                PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
            );

            for ($idx = 0; $idx < count($attrKeys); $idx++) {
                if (
                    empty($key = trim($attrKeys[ $idx ]))
                    || $key === 'xmlns'
                    || $key === 'xmlns:xlink'
                ) {
                    continue;
                }

                if ($key === 'viewBox' || $key === 'id') {
                    $symbolAttrs[] = $key . '="' . $attrVals[ $idx ] . '"';
                } else {
                    $svgAttrs[ $key ] = $attrVals[ $idx ];
                }
            }

            if (!empty($attributes['class'])) {
                if (!empty($svgAttrs['class'])) {
                    $svgAttrs['class'] = $svgAttrs['class'] . ' ' . $attributes['class'];
                } else {
                    $svgAttrs['class'] = $attributes['class'];
                }

                unset($attributes['class']);
            }

            $svgAttrs = array_merge($svgAttrs, $attributes);

            if (!empty($size)) {
                $svgAttrs['width'] = $size;
                $svgAttrs['height'] = $size;
            }

            $captured = preg_match('/<svg.+?>(.+?)<\/svg>/', $iconContent, $svgContent);

            if (false === $captured || count($svgContent) < 2) {
                return self::renderError(
                    $path, $icon, 'when capturing content'
                );
            }

            array_shift($svgContent);
            $svgContent = implode('', $svgContent);

            $icon = '<svg';

            foreach ($svgAttrs as $key => $val) {
                $icon .= " {$key}=" . '"' . $val . '"';
            }

            $icon .= '>'
                . '<use xlink:href="#' . $cacheKey . '" />'
                . '</svg>';

            $symbol = '<symbol id="' . $cacheKey . '" '
                . implode(' ', $symbolAttrs)
                . ">{$svgContent}</symbol>";

            self::$icons[ $cacheKey ] = $icon;

            self::$sprite[ $cacheKey ] = $symbol;

            file_put_contents("{$cachePath}/{$cacheKey}-icon.svg", $icon);
            file_put_contents("{$cachePath}/{$cacheKey}-symbol.svg", $symbol);
        }

        return self::$icons[ $cacheKey ];
    }
}
