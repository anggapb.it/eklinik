<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelastarif extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="klstarif";
    protected $primaryKey="idklstarif";
    protected $guarded = [];
	public $timestamps = false;
}
