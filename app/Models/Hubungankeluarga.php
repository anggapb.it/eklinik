<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hubungankeluarga extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="hubkeluarga";
    protected $primaryKey="idhubkeluarga";
    protected $guarded = [];
	public $timestamps = false;
}
