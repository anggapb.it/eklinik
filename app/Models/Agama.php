<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="agama";
	protected $primaryKey="idagama";
	public $timestamps = false;
}
