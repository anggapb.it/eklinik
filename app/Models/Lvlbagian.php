<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lvlbagian extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="lvlbagian";
	protected $primarykey="idlvlbagian";
	protected $guarded = [];
	public $timestamps = false;
}
