<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kuitansi extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="kuitansi";
    protected $primaryKey="idkuitansi";
    protected $guarded = [];
	public $timestamps = false;
}
