<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelompokbarang extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="klpbarang";
    protected $primaryKey="idklpbrg";
    protected $guarded = [];
	public $timestamps = false;
}
