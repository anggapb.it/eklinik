<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statusdokter extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="stdokter";
    protected $primaryKey="idstdokter";
    protected $guarded = [];
	public $timestamps = false;
}
