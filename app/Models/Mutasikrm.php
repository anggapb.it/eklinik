<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mutasikrm extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="mutasikrm";
    protected $guarded = [];
    public $timestamps = false;
}
