<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penyakit extends Model
{
    use HasFactory;

	protected $connection = 'klinik_old';
    protected $table= "penyakit";
    protected $primaryKey= "idpenyakit";
    protected $guarded = [];
    public $timestamps = false;

    public function jhirarki()
    {
        return $this->hasOne('App\Models\Jenishirarki', 'idjnshirarki', 'idjnshirarki');
	}
}
