<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kamar extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="kamar";
	protected $primaryKey="idkamar";
	protected $guarded = [];
	public $timestamps = false;
}
