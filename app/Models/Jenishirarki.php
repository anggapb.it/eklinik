<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenishirarki extends Model
{
    use HasFactory;

	protected $connection = 'klinik_old';
    protected $table="jhirarki";
    protected $primarykey="idjnshirarki";
    protected $guarded = [];
    public $timestamps = false;
}
