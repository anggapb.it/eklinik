<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jampraktek extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jampraktek";
    protected $primaryKey="idjampraktek";
    protected $guarded = [];
	public $timestamps = false;
}
