<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penjamin extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="penjamin";
	protected $primaryKey="idpenjamin";
	public $incrementing=false;
	protected $guarded = [];
	public $timestamps = false;

	public function jpenjamin()
    {
        return $this->hasOne('App\Models\Jenispenjamin', 'idjnspenjamin', 'idjnspenjamin');
	}
	public function status()
    {
        return $this->hasOne('App\Models\Status', 'idstatus', 'idstatus');
	}
}
