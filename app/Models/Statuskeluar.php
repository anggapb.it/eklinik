<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statuskeluar extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="stkeluar";
    protected $primaryKey="idstkeluar";
    protected $guarded = [];
	public $timestamps = false;
}
