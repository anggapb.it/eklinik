<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistrasiDet extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="registrasidet";
    protected $guarded = [];
	public $timestamps = false;

	public function get_dokter()
    {
        return $this->hasOne('App\Models\Dokter', 'iddokter', 'iddokter');
	}

	public function get_bagian()
    {
        return $this->hasOne('App\Models\Bagian', 'idbagian', 'idbagian');
	}
    
    public function get_shift()
    {
        return $this->hasOne('App\Models\Shift', 'idshift', 'idshift');
	}
   
    public function get_cara_datang()
    {
        return $this->hasOne('App\Models\Caradatang', 'idcaradatang', 'idcaradatang');
	}
   
    public function get_reservasi()
    {
        return $this->hasOne('App\Models\Reservasi', 'idregdet', 'idregdet');
	}

}
