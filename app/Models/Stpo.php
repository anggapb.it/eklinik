<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stpo extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="stpo";
	protected $primaryKey="idstpo";
	public $incrementing=false;
	protected $guarded = [];
	public $timestamps = false;
}
