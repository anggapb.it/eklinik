<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="pendidikan";
    protected $primaryKey="idpendidikan";
    protected $guarded = [];
	public $timestamps = false;
}
