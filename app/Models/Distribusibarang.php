<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Distribusibarang extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="keluarbrg";
    protected $guarded = [];
	public $timestamps = false;

}
