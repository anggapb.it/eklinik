<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    use HasFactory;

	protected $connection = 'klinik_old';
    protected $table= "dokter";
    protected $primaryKey= "iddokter";
    protected $guarded = [];
    public $timestamps = false;

    public function jtenagamedis()
    {
        return $this->hasOne('App\Models\Jenistenagamedis', 'idjnstenagamedis', 'idjnstenagamedis');
	}
	public function jkelamin()
    {
        return $this->hasOne('App\Models\Jkelamin', 'idjnskelamin', 'idjnskelamin');
	}
	public function spesialisasi()
    {
        return $this->hasOne('App\Models\Spesialisasidokter', 'idspesialisasi', 'idspesialisasi');
	}
	public function status()
    {
        return $this->hasOne('App\Models\Status', 'idstatus', 'idstatus');
	}
	public function stdokter()
    {
        return $this->hasOne('App\Models\Statusdokter', 'idstdokter', 'idstdokter');
	}
}