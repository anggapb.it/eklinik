<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statuspelayanan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="stpelayanan";
    protected $primaryKey="idstpelayanan";
    protected $guarded = [];
	public $timestamps = false;
}
