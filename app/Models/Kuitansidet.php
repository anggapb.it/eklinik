<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kuitansidet extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="kuitansidet";
    protected $primaryKey="idkuitansidet";
    protected $guarded = [];
    public $timestamps = false;

    public function bank()
    {
        return $this->hasOne('App\Models\Bank', 'idbank', 'idbank');
	}
    public function carabayar()
    {
        return $this->hasOne('App\Models\Carabayar', 'idcarabayar', 'idcarabayar');
	}
}
