<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statuspasien extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="stpasien";
    protected $primaryKey="idstpasien";
    protected $guarded = [];
	public $timestamps = false;
}
