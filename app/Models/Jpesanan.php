<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jpesanan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jpp";
	public $timestamps = false;
}
