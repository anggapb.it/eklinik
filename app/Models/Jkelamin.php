<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jkelamin extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jkelamin";
    protected $primaryKey="idjnskelamin";
    protected $guarded = [];
	public $timestamps = false;
}
