<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="bagian";
	protected $primaryKey="idbagian";
	protected $guarded = [];
	public $timestamps = false;

	public function lvlbagian()
    {
        return $this->hasOne('App\Models\Lvlbagian', 'idlvlbagian', 'idlvlbagian');
	}
	public function jhirarki()
    {
        return $this->hasOne('App\Models\Jenishirarki', 'idjnshirarki', 'idjnshirarki');
	}
	public function jpelayanan()
    {
        return $this->hasOne('App\Models\Jenispelayanan', 'idjnspelayanan', 'idjnspelayanan');
	}
	public function bdgrawat()
    {
        return $this->hasOne('App\Models\Bidangperawatan', 'idbdgrawat', 'idbdgrawat');
	}
}
