<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenispembayaran extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="jpembayaran";
    protected $primaryKey="idjnspembayaran";
    protected $guarded = [];
	public $timestamps = false;
}
