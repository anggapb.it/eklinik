<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jsatuan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jsatuan";
	protected $primarykey="idsatuan";
	protected $guarded = [];
	public $timestamps = false;
}
