<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statuspesanan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="stpo";
    protected $primaryKey="idstpo";
    protected $guarded = [];
	public $timestamps = false;
}
