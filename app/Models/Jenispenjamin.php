<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenispenjamin extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jpenjamin";
    protected $primaryKey="idjnspenjamin";
    protected $guarded = [];
	public $timestamps = false;
}
