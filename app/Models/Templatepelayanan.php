<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Templatepelayanan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="templatepelayanan";
	protected $primaryKey="idtemplayanan";
	protected $guarded = [];
	public $timestamps = false;

	public function jhirarki()
    {
        return $this->hasOne('App\Models\Jenishirarki', 'idjnshirarki', 'idjnshirarki');
	}

	public function status()
    {
        return $this->hasOne('App\Models\Status', 'idstatus', 'idstatus');
	}
	public function templatepelayanan()
    {
        return $this->hasOne('App\Models\Templatepelayanan', 'idtemplayanan', 'idtemplayanan');
	}
}