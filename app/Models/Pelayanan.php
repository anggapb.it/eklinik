<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelayanan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="pelayanan";
	protected $primaryKey="kdpelayanan";
	public $incrementing=false;
	protected $guarded = [];
	public $timestamps = false;

	public function jpelayanan()
    {
        return $this->hasOne('App\Models\Jenispelayanan', 'idjnspelayanan', 'idjnspelayanan');
	}
	public function jhirarki()
    {
        return $this->hasOne('App\Models\Jenishirarki', 'idjnshirarki', 'idjnshirarki');
	}
	public function status()
    {
        return $this->hasOne('App\Models\Status', 'idstatus', 'idstatus');
	}
}