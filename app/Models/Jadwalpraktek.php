<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwalpraktek extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jadwalpraktek";
	protected $primaryKey="idjadwalpraktek";
	protected $guarded = [];
    public $timestamps = false;

	public function bagian()
    {
        return $this->hasOne('App\Models\Bagian', 'idbagian', 'idbagian');
	}
    public function dokter()
    {
        return $this->hasOne('App\Models\Dokter', 'iddokter', 'iddokter');
    }
    public function hari()
    {
        return $this->hasOne('App\Models\Hari', 'idhari', 'idhari');
	}
    public function shift()
    {
        return $this->hasOne('App\Models\Shift', 'idshift', 'idshift');
    }
    
}
