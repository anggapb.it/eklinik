<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenistenagamedis extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="jtenagamedis";
    protected $primaryKey="idjnstenagamedis";
    protected $guarded = [];
	public $timestamps = false;
}
