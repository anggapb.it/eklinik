<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notadet extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="notadet";
    protected $primaryKey="idnotadet";
    protected $guarded = [];
    public $timestamps = false;
}
