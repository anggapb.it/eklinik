<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carakeluar extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jcarakeluar";
    protected $primaryKey="idcarakeluar";
    protected $guarded = [];

	public $timestamps = false;
}
