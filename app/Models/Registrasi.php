<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registrasi extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="registrasi";
    protected $guarded = [];
	public $timestamps = false;

	public function get_penjamin()
    {
        return $this->hasOne('App\Models\Penjamin', 'idpenjamin', 'idpenjamin');
	}

	public function get_reg_det()
	{
		return $this->hasOne('App\Models\RegistrasiDet', 'noreg', 'noreg');
	}

	public function get_jpelayanan()
	{
		return $this->hasOne('App\Models\Jpelayanan', 'idjnspelayanan', 'idjnspelayanan');
	}

    public function get_pasien()
	{
		return $this->hasOne('App\Models\Pasien', 'norm', 'norm');
	}
	
}
