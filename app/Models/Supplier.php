<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="supplier";
    protected $primaryKey="kdsupplier";
    public $incrementing=false;
    protected $guarded = [];
    public $timestamps = false;

    public function bank()
    {
        return $this->hasOne('App\Models\Bank', 'idbank', 'idbank');
	}
	public function status()
    {
        return $this->hasOne('App\Models\Status', 'idstatus', 'idstatus');
	}
}

