<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="status";
    protected $primarykey="idstatus";
    protected $guarded = [];
    public $timestamps = false;
}

