<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Podet extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="podet";
	public $incrementing=false;
	protected $guarded = [];
	public $timestamps = false;

    public function barang()
    {
        return $this->hasOne('App\Models\Barang', 'kdbrg', 'kdbrg');
	}
}
