<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Syaratpembayaran extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="sypembayaran";
    protected $primaryKey="idsypembayaran";
    protected $guarded = [];
	public $timestamps = false;
}
