<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statusstokopname extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="stso";
    protected $primaryKey="idstso";
    protected $guarded = [];
	public $timestamps = false;
}
