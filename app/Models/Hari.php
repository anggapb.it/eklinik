<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hari extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="hari";
	protected $primaryKey="idhari";
	protected $guarded = [];
    public $timestamps = false;
    
}
