<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="barang";
	protected $primarykey="kdbrg";
	protected $guarded = [];
    public $timestamps = false;

    public function jsatuan()
    {
        return $this->hasOne('App\Models\Jsatuan', 'idsatuan', 'idsatuankcl');
	}

    public function klpbrg()
    {
        return $this->hasOne('App\Models\Kelompokbarang', 'idklpbrg', 'idklpbrg');
    }
    public function jbarang()
    {
        return $this->hasOne('App\Models\Jenisbarang', 'idjnsbrg', 'idjnsbrg');
    }
     public function jsatuan_besar()
    {
        return $this->hasOne('App\Models\Jsatuan', 'idsatuan', 'idsatuanbsr');
    }
    public function pabrik()
    {
        return $this->hasOne('App\Models\Pabrik', 'idpabrik', 'idpabrik');
    }
    public function supplier()
    {
        return $this->hasOne('App\Models\Supplier', 'kdsupplier', 'kdsupplier');
    }
     public function status()
    {
        return $this->hasOne('App\Models\Status', 'idstatus', 'idstatus');
    }
    public function barangbagian()
    {
        return $this->hasOne('App\Models\Barangbagian', 'kdbrg', 'kdbrg');
    }


}
