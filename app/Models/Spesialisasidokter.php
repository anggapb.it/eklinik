<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spesialisasidokter extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="spesialisasi";
    protected $primaryKey="idspesialisasi";
    protected $guarded = [];
	public $timestamps = false;
}
