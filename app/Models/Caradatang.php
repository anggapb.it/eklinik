<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caradatang extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="caradatang";
    protected $primaryKey="idcaradatang";
    protected $guarded = [];

	public $timestamps = false;
}
