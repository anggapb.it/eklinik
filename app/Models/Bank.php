<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="bank";
    protected $primaryKey="idbank";
    protected $guarded = [];
    public $timestamps = false;
}
