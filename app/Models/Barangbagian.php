<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barangbagian extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="barangbagian";
    public $timestamps = false;

    public function barang()
    {
        return $this->hasOne('App\Models\Barang', 'kdbrg', 'kdbrg');
	}
    public function bagian()
    {
        return $this->hasOne('App\Models\Bagian', 'idbagian', 'idbagian');
	}
}
