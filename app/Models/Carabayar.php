<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carabayar extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="carabayar";
    protected $primaryKey="idcarabayar";
    protected $guarded = [];

	public $timestamps = false;
}
