<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Goldarah extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="goldarah";
    protected $primaryKey="idgoldarah";
    protected $guarded = [];
	public $timestamps = false;
}
