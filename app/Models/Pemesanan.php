<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="pp";
	protected $primaryKey="nopp";
	public $incrementing=false;
	protected $guarded = [];
	public $timestamps = false;

    public function bagian()
    {
        return $this->hasOne('App\Models\Bagian', 'idbagian', 'idbagian');
	}
    public function stsetuju()
    {
        return $this->hasOne('App\Models\Stsetuju', 'idstsetuju', 'idstsetuju');
	}
    public function supplier()
    {
        return $this->hasOne('App\Models\Supplier', 'kdsupplier', 'kdsupplier');
	}
    public function stpo()
    {
        return $this->hasOne('App\Models\Stpo', 'idstpo', 'idstpo');
	}
    // public function pengguna()
    // {
    //     return $this->hasOne('App\User', 'userid', 'userid');
	// }
}
