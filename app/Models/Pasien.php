<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="pasien";
    protected $guarded = [];
    public $timestamps = false;

    public function jkelamin()
    {
        return $this->hasOne('App\Models\Jkelamin', 'idjnskelamin', 'idjnskelamin');
	}
    public function get_daerah()
    {
        return $this->hasOne('App\Models\Daerah', 'iddaerah', 'iddaerah');
	}
     public function stkawin()
    {
        return $this->hasOne('App\Models\Stkawin', 'idstkawin', 'idstkawin');
    }
}
