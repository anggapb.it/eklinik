<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenisbarang extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jbarang";
    protected $primaryKey="idjnsbrg";
    protected $guarded = [];
	public $timestamps = false;
}
