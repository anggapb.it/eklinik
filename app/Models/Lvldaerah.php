<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lvldaerah extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="lvldaerah";
    public $timestamps = false;
}
