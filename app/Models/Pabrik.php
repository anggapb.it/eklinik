<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pabrik extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="pabrik";
    protected $primaryKey="idpabrik";
    protected $guarded = [];

    public $timestamps = false;
}

