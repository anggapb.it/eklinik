<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jpelayanan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="jpelayanan";
	protected $primarykey="idjnspelayanan";
	protected $guarded = [];
	public $timestamps = false;
}
