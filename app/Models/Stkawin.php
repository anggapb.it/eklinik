<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stkawin extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="stkawin";
    protected $primaryKey="idstkawin";
    protected $guarded = [];
	public $timestamps = false;
}
