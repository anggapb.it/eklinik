<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Po extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="po";
	protected $primaryKey="nopo";
	public $incrementing=false;
	protected $guarded = [];
	public $timestamps = false;

    public function bagian()
    {
        return $this->hasOne('App\Models\Bagian', 'idbagian', 'idbagian');
	}

    public function supplier()
    {
        return $this->hasOne('App\Models\Supplier', 'kdsupplier', 'kdsupplier');
	}

    public function sypembayaran()
    {
        return $this->hasOne('App\Models\Syaratpembayaran', 'idsypembayaran', 'idsypembayaran');
	}
    public function jnspembayaran()
    {
        return $this->hasOne('App\Models\Jenispembayaran', 'idjnspembayaran', 'idjnspembayaran');
	}

    public function stsetuju()
    {
        return $this->hasOne('App\Models\Stsetuju', 'idstsetuju', 'idstsetuju');
	}

    public function stkontrabon()
    {
        return $this->hasOne('App\Models\Stkontrabon', 'idstkontrabon', 'idstkontrabon');
	}
}
