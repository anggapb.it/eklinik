<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Templateobat extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="templateobat";
	protected $primaryKey="idtempobat";
	protected $guarded = [];
	public $timestamps = false;

	public function jhirarki()
    {
        return $this->hasOne('App\Models\Jenishirarki', 'idjnshirarki', 'idjnshirarki');
	}

	public function status()
    {
        return $this->hasOne('App\Models\Status', 'idstatus', 'idstatus');
	}
	public function templateobat()
    {
        return $this->hasOne('App\Models\Templateobat', 'idtempobat', 'idtempobat');
	}
}
