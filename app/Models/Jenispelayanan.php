<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenispelayanan extends Model
{
    use HasFactory;
    protected $connection = 'klinik_old';
	protected $table="jpelayanan";
	protected $primaryKey="idjnspelayanan";
	protected $guarded = [];
	public $timestamps = false;
}
