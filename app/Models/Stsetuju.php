<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stsetuju extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="stsetuju";
	protected $primaryKey="idstsetuju";
	public $incrementing=false;
	protected $guarded = [];
	public $timestamps = false;
}
