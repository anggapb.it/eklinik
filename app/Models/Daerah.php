<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daerah extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="daerah";
	protected $primaryKey="iddaerah";
	protected $guarded = [];
    public $timestamps = false;
    
    public function lvldaerah()
    {
        return $this->hasOne('App\Models\Lvldaerah', 'idlvldaerah', 'idlvldaerah');
	}
    public function jhirarki()
    {
        return $this->hasOne('App\Models\Jenishirarki', 'idjnshirarki', 'idjnshirarki');
    }

     public function kecamatan()
    {
        return $this->hasOne('App\Models\Daerah', 'iddaerah', 'dae_iddaerah');
    }

}
