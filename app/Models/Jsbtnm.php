<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jsbtnm extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="jsbtnm";
    protected $primaryKey="idsbtnm";
    protected $guarded = [];
	public $timestamps = false;
}
