<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bidangperawatan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
    protected $table="bdgrawat";
    protected $primaryKey="idbdgrawat";
    protected $guarded = [];

	public $timestamps = false;
}
