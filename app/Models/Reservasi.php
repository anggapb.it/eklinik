<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservasi extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="reservasi";
    protected $guarded = [];
	public $timestamps = false;
}
