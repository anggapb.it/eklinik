<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statusposisipasien extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="stposisipasien";
    protected $primaryKey="idstposisipasien";
    protected $guarded = [];
	public $timestamps = false;
}
