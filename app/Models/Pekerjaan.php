<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    use HasFactory;

    protected $connection = 'klinik_old';
	protected $table="pekerjaan";
    protected $primaryKey="idpekerjaan";
    protected $guarded = [];
	public $timestamps = false;
}
