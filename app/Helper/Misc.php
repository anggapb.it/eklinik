<?php /** @noinspection PhpWrongStringConcatenationInspection */

$cache = [];

if (! function_exists('_once')) {
    /**
     * A much superior version of `spatie/once`, in which we stole lots of stuff from.
     *
     * @param \Closure $callback
     *
     * @return mixed
     */
    function _once($callback) {
        global $cache;

        $trace = debug_backtrace(
            DEBUG_BACKTRACE_PROVIDE_OBJECT,
            2
        )[1];

        $objectHash = spl_object_hash($trace['object'] ?? app());

        $normalizedArguments = array_map(function ($argument) {
            return is_object($argument) ? spl_object_hash($argument) : $argument;
        }, $trace['args']);

        $hash = ($objectHash) . ($trace['type'] ?? '--') . $trace['function']
            . '(' . md5(serialize($normalizedArguments)) . ')';

        if (empty($cache[ $hash ])) {
            $result = call_user_func($callback, $trace['args']);

            $cache[ $hash ] = $result;
        }

        return $cache[ $hash ];
    }
}

if (! function_exists('array_has_keys')) {
    /**
     * Checks where an assoc array has keys.
     *
     * @param array $array
     * @param array|string|int $keys
     *
     * @return bool
     */
    function array_has_keys($array, $keys) {
        if (!is_array($keys)) {
            $keys = [ $keys ];
        }

        foreach ($keys as $key) {
            if (empty($array[ $key ])) {
                return false;
            }
        }

        return true;
    }
}

if (! function_exists('cache_assets')) {
    /**
     * Cache multiple assets of the same type (css / js only).
     *
     * @param string[] $paths
     * @param string $ext css / js
     * @param boolean $deferJs if js, will add [defer] attribute to resulting <script>
     * @param boolean $noTurbolinksEval if js, will add [data-turbolinks-eval="false"] attribute to resulting <script>
     * @return void
     */
    function cache_assets ($paths, $ext, $deferJs = false, $noTurbolinksEval = false) {
        if (empty($paths)) {
            return '';
        }

        if (!in_array($ext, ['css', 'js'])) {
            $error = "ERROR: cache_assets function does not recognize {$ext} extension";

            return "<!-- {$error} --><script>console.log('{$error}')</script>";
        }

        $cachedUri = hash('fnv164', implode(',', $paths), false) . ".{$ext}";
        $cachedUrl = url("storage/cached-assets/{$cachedUri}");
        $cachePath = storage_path("app/public/cached-assets/{$cachedUri}");

        if (!file_exists($cachePath)) {
            $cacheContent = '';

            foreach ($paths as $path) {
                if (config('app.debug')) {
                    if ($ext === 'js') {
                        $cacheContent .= PHP_EOL . '// file: ' . $path . PHP_EOL;
                    } else if ($ext === 'css') {
                        $cacheContent .= PHP_EOL . '/* file: ' . $path . ' */' . PHP_EOL;
                    }
                }

                $cacheContent .= preg_replace(
                    '/\/\/# sourceMappingURL=.+/',
                    '',
                    file_get_contents(base_path($path))
                );

                if ($ext === 'js') {
                    $cacheContent .= ';' . PHP_EOL;
                }
            }

            file_put_contents($cachePath, $cacheContent);
        }

        if ($ext === 'js') {
            return "<script src=\"{$cachedUrl}\""
                . ($deferJs ? ' defer' : '')
                . ($noTurbolinksEval ? ' data-turbolinks-eval="false"' : '')
                . '></script>';
        }

        return "<link rel=\"stylesheet\" href=\"{$cachedUrl}\">";
    }
}

if (! function_exists('inline_asset')) {
    /**
     * Usage:
     * - inline_asset('node_modules/somepackage/somefile.js')
     * - inline_asset([
     *  'node_modules/somepackage/somefile.js',
     *  'frontend/css/app.js',
     * ])
     *
     * This function does not care whether all paths are of the same extension.
     *
     * @param string[]|string $paths Paths must reside inside app's base path.
     * @param string[]|string $absolutes URI patterns that will be replace with it's Absolute URL.
     *
     * @return string
     */
    function inline_asset($paths, $absolutes = []) {
        $contents = '';

        $paths = !is_array($paths) ? [$paths] : $paths;

        if (!empty($absolutes)) {
            $absolutes = !is_array($absolutes) ? [$absolutes] : $absolutes;
        }

        foreach ($paths as $path) {
            $rawPath = $path;
            $baseDir = str_replace('public/', '', dirname($path));
            $path = base_path($path);

            if (!file_exists($path)) {
                $contents .= "/* FILE MISSING: {$rawPath} */" . PHP_EOL;

                continue;
            }

            $content = @file_get_contents($path);

            if (!empty($absolutes)) {
                foreach ($absolutes as $oldPattern) {
                    $content = str_replace(
                        "url('{$oldPattern}",
                        "url('" . url("{$baseDir}/{$oldPattern}"),
                        $content
                    );
                }
            }

            if (config('app.debug')) {
                $contents .= "/* {$rawPath} */";
            }

            $contents .= "{$content}";
        }

        return $contents;
    }
}

if ( !function_exists('pegawaiAuth') ) {
    function pegawaiAuth($field = 'nip') {
        $pengguna = Illuminate\Support\Facades\Auth::user();
        $pegawai = $pengguna->pegawai;
        $field_mapping = array(
            'id' => 'pegawai_id',
            'nip' => 'pegawai_nip',
            'peg_id' => 'pegawai_simpeg_id',
        );
        $field = $field_mapping[$field];

        return empty($pegawai) || empty($pegawai->{$field}) ? null : $pegawai->{$field};
    }
}

if ( !function_exists('generate_cache_key') ) {
    function generate_cache_key() {
        $url = request()->url();
        $queryParams = request()->query();

        ksort($queryParams);

        $queryString = http_build_query($queryParams);

        $fullUrl = "{$url}?{$queryString}";

        return $fullUrl;

        return sha1($fullUrl);
    }
}

if ( !function_exists('roleKeyCheck') ) {
    function roleKeyCheck($key = array()) {
        $role = auth()->user()->roles
            ->whereIn('key', $key)
            ->values()
            ->toArray();

        $role = true;

        if ( empty($role) ):
            throw new \Illuminate\Validation\UnauthorizedException;
        endif;
    }
}
