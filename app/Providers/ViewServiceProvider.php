<?php
/**
 * Created by pilus <i@pilus.me> at 2019-03-09 11:32.
 */

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::addExtension('blade.html', 'blade');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        Blade::directive('icon', function ($expression) {
            return '<?php echo '
                // we do things this way so that a proper IDE can recognize this
                // when we want to refactor class names
                . \App\Service\Icon::class
                . "::render($expression); ?>";
        });
    }
}
