<?php

namespace App\Http\Controllers\Kasir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Pasien,
    Stkawin,Agama,Goldarah,Pendidikan,Pekerjaan,Daerah,
    Registrasi,RegistrasiDet,Reservasi,Penjamin,Caradatang,Bagian,Dokter,Jadwalpraktek,Shift,Mutasikrm,
    Barangbagian,Barang,Jsatuan,Nota,Notadet,Tarif,Pelayanan,Kuitansi,Kuitansidet,Carabayar,Bank,Jsbtnm};
use Auth,DB;


class KasirController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $bagian =  Bagian::where('idjnspelayanan','=','1')->get();
        return view('pages/kasir/index',compact(['bagian']));
    }

    public function detail($noreg)
    {
        $reg = Registrasi::select('registrasi.*')
        ->where('noreg','=',$noreg)
        ->with(['get_pasien','get_pasien.jkelamin','get_reg_det',
        'get_reg_det.get_bagian','get_reg_det.get_dokter',
        'get_reg_det.get_shift','get_reg_det.get_cara_datang','get_reg_det.get_reservasi'])->first();

        $transaksi = Nota::selectRaw("(sum(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp))) AS total_tagihan,
        (sum(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) + nota.uangr
        - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - nota.diskon) AS total_tagihan, sum(notadet.dijamin) AS total_dijamin
        , nota.uangr as jasa_racik, nota.diskon as diskonf, (sum(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) + nota.diskon) as total_diskon
        , nota.nonota, nota.nokuitansi
        ")->leftJoin('notadet', 'notadet.nonota', '=', 'nota.nonota')
           ->where('nota.idregdet','=',$reg->get_reg_det->idregdet)
           ->groupby('nota.nonota')->first();

        $kuitansi = Kuitansi::where('nokuitansi','=',$transaksi->nokuitansi)->first();
        $total_bayar = ($kuitansi->pembayaran)?$kuitansi->pembayaran:0;
        $sisa_tagihan = $transaksi->total_tagihan - $total_bayar;

        $carabayar = Carabayar::all();
        $bank = Bank::all();



        return view('pages/kasir/detail',compact(['reg','noreg','transaksi','carabayar','bank','total_bayar','sisa_tagihan']));
    }

    public function get_kuitansi_det(Request $req)
    {
        $nokuitansi = $req->nokuitansi;
        $kuitansidet = Kuitansidet::where('nokuitansi','=',$nokuitansi)
                    ->with(['bank','carabayar'])
                    ->get();

        return datatables()->of($kuitansidet)
        ->addColumn('nmbank', function ($data) {
            if(isset($data->bank->nmbank)){
                return $data->bank->nmbank;
            }else{
                return '';
            }

        })
        ->rawColumns(['nmbank'])
        ->addIndexColumn()
        ->make(true);
    }

    public function insert_pembayaran(Request $req)
    {
        $nonota = $req->nonota;
        if($req->nokuitansi == '' || $req->nokuitansi == null)
		{
			$nr = $this->getNokuitansi();
			$nostart = 'NK';
			$nomid = date('y');
			$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
            $nokuitansi = $nostart.$nomid.$noend;
            $nota = Nota::where('nonota','=',$nonota)->update([
                'nokuitansi' => $nokuitansi
            ]);
		} else{
			$nokuitansi = $req->nokuitansi;
		}
        $idcarabayar = $req->idcarabayar;
        $idbank = $req->idbank;
        $nokartu = $req->nokartu;
        $sisa_tagihan = $req->sisa_tagihan;
        $nominal = $req->nominal;
        $noreg = $req->noreg;
        $norm = $req->norm;
        $atasnama = $req->atasnama;
        $tagihan = $req->tagihan;

        $kui = Kuitansi::where('nokuitansi','=',$nokuitansi);
        $row_kui = $kui->first();
        $count_kui = $kui->count();
        if($count_kui > 0):
            $up_kui = $kui->update([
                            'pembayaran' => $row_kui->pembayaran + $nominal
                        ]);
            $kuitansi_det = new Kuitansidet();
            $kuitansi_det->nokuitansi = $nokuitansi;
            $kuitansi_det->idbank = $idbank;
            $kuitansi_det->idcarabayar = $idcarabayar;
            $kuitansi_det->jumlah = $nominal;
            $kuitansi_det->nokartu = $nokartu;
            $kuitansi_det->save();

            $message = array();
            $message['error'] = '';
            $message['message'] = 'Data berhasil ditambahkan';
            $message['list'] = $kuitansi_det;
        else:
              //getumurpasien
            $row_rd = RegistrasiDet::where('noreg','=',$noreg)->first();
            $umur = $row_rd->umurtahun;
            $row_pasien = Pasien::where('norm','=',$norm)->first();
            //$row_sbtnm = Jsbtnm::where('idjnskelamin','=',$row_pasien->idjnskelamin)
              //      ->whereRaw('dariumur >= "'.$row_rd->umurtahun.'" AND sampaiumur <= "'.$row_rd->umurtahun.'"')->first();
            $kuitansi = new Kuitansi();
            $kuitansi->nokuitansi = $nokuitansi;
            $kuitansi->tglkuitansi = date("Y-m-d");
            $kuitansi->jamkuitansi = date("H:i:s");
            $kuitansi->idstkuitansi = 1; // normal
            //$kuitansi->idsbtnm = $row_sbtnm->idsbtnm;
            $kuitansi->atasnama = $atasnama;
            $kuitansi->idjnskuitansi = 1; // pendapatan rawat jalan
            $kuitansi->pembulatan = 0;
            $kuitansi->total = $tagihan;
            $kuitansi->tgljaminput = date("Y-m-d H:i:s");
            $kuitansi->pembayaran = $nominal;
            $kuitansi->save();

            $kuitansi_det = new Kuitansidet();
            $kuitansi_det->nokuitansi = $nokuitansi;
            $kuitansi_det->idbank = $idbank;
            $kuitansi_det->idcarabayar = $idcarabayar;
            $kuitansi_det->jumlah = $nominal;
            $kuitansi_det->nokartu = $nokartu;
            $kuitansi_det->save();

            $message = array();
            $message['error'] = '';
            $message['message'] = 'Data berhasil ditambahkan';
            $message['list'] = $kuitansi_det;

        endif;


        return response()->json($message);

    }

    function getNokuitansi(){
        $thn = date('y');
        $data = Kuitansi::whereRaw("SUBSTRING(nokuitansi,3,2) = '".$thn."'")->count();
		if($data == 0) $max = 1;
		else $max = $data + 1;
		return $max;
    }

    public function delete_kuidet($id)
    {
        $kuidet = Kuitansidet::where('idkuitansidet','=',$id);
        $row_kuidet = $kuidet->first();
        $kui = Kuitansi::where('nokuitansi','=',$row_kuidet->nokuitansi);
        $row_kui = $kui->first();
        $up_kui = $kui->update([
                'pembayaran' => $row_kui->pembayaran - $row_kuidet->jumlah
            ]);

        $data = array();
        if ($kuidet->delete()) {
            $data['message'] = 'Data Berhasil Dihapus';
            $data['error'] = '';
		}else{
            $data['message'] = '';
            $data['error'] = 'Data Gagal Dihapus';
        }

        return response()->json($data)->setStatusCode(200);
    }
}
