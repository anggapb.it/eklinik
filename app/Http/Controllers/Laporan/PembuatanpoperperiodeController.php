<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pasien;

class PembuatanpoperperiodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
    	return view('pages/laporan/pembuatan_po_per_periode');
    }

    public function get_data()
    {
    	$pasien = Pasien::all();

    	return datatables()->of($pasien)
        ->addIndexColumn()
        ->make(true);
    }
}
