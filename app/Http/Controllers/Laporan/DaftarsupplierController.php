<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;

class DaftarsupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
    	return view('pages/laporan/daftar_supplier');
    }

    public function get_data()
    {
    	$supplier = Supplier::with(['bank,status'])->get();

    	return datatables()->of($supplier)
        >addColumn('nmbank', function ($data) {
            if(isset($data->bank->nmbank)){
                return $data->bank->nmbank;
            } else{
                return '';
            }

        })
        ->addColumn('status', function ($data) {
            if(isset($data->status->nmstatus)){
                return $data->status->nmstatus;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }
}
