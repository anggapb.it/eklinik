<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Registrasi;
use App\Models\RegistrasiDet;

class KunjunganpasienrjperpoliklinikController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
    	return view('pages/laporan/kunjungan_pasien_rj_per_poliklinik');
    }

    public function get_data()
    {
    	$registrasi = Registrasi::with(['get_pasien','get_reg_det.get_dokter','get_reg_det.get_bagian','get_penjamin','get_reg_det.get_cara_datang','get_reg_det'])->get();

    	return datatables()->of($registrasi)
        ->addIndexColumn()
        ->make(true);
    }
}
