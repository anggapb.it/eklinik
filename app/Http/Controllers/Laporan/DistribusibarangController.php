<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Distribusibarang;

class DistribusibarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
    	return view('pages/laporan/distribusi_barang');
    }

    public function get_data()
    {
    	$distribusibarang = Distribusibarang::all();

    	return datatables()->of($distribusibarang)
        ->addIndexColumn()
        ->make(true);
    }
}
