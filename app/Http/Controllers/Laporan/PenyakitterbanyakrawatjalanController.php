<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penyakit;

class PenyakitterbanyakrawatjalanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
    	return view('pages/laporan/penyakit_terbanyak_rawat_jalan');
    }

    public function get_data()
    {
    	$penyakit = Penyakit::all();

    	return datatables()->of($penyakit)
        ->addIndexColumn()
        ->make(true);
    }
}
