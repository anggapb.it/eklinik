<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barang;

class DaftarbarangController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
    	return view('pages/laporan/daftar_barang');
    }

    public function get_data()
    {
        $barang = Barang::with(['jbarang','jsatuan','barangbagian'])->get();

        return datatables()->of($barang)
        ->addColumn('nmjnsbrg', function ($data) {
            if(isset($data->jbarang->nmjnsbrg)){
                return $data->jbarang->nmjnsbrg;
            } else{
                return '';
            }

        })
        ->addColumn('nmsatuanbsr', function ($data) {
            if(isset($data->jsatuan->nmsatuanbsr)){
                return $data->jsatuan->nmsatuanbsr;
            } else{
                return '';
            }


        })
        ->addColumn('nmsatuankcl', function ($data) {
            if(isset($data->jsatuan->nmstkawin)){
                return $data->jsatuan->nmstkawin;
            } else{
                return '';
            }
            

        })
        ->addIndexColumn()
        ->make(true);
    }
}
