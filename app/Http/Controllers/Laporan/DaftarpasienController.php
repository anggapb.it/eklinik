<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pasien;

class DaftarpasienController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
    	return view('pages/laporan/daftar_pasien');
    }

    public function get_data()
    {
    	$pasien = Pasien::with(['jkelamin','stkawin'])->get();

    	return datatables()->of($pasien)
        ->addColumn('nmjnskelamin', function ($data) {
            if(isset($data->jkelamin->nmjnskelamin)){
                return $data->jkelamin->nmjnskelamin;
            } else{
                return '';
            }

        })
        ->addColumn('nmstkawin', function ($data) {
            if(isset($data->stkawin->nmstkawin)){
                return $data->stkawin->nmstkawin;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }
}
