<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Barangbagian,Barang,Jsatuan,Nota,Notadet,Tarif,Pelayanan,Jpesanan,Syaratpembayaran, Jenispembayaran, Statuspesanan,
     Supplier,Po,Podet,PemesananDetail,Pemesanan};
use App\Http\Requests\StorePpRequest;
use DB,Auth;


class ReceiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $jpp = Jpesanan::all();
        $syarat_pembayaran = Syaratpembayaran::all();
        $jpembayaran = Jenispembayaran::all();
        $stpesanan = Statuspesanan::all();
        $supplier = Supplier::all();
        return view('pages/inventory/purchasing/receive/create',compact(['jpp','syarat_pembayaran','jpembayaran','stpesanan','supplier']));
    }

    public function get_pesanan()
    {
        $data = Pemesanan::with(['bagian','stsetuju','supplier'])->get();
        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }
}
