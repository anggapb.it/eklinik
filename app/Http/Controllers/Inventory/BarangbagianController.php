<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barangbagian;

class BarangbagianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $barangbagian = Barangbagian::all();
        return view('pages/inventory/logistik/barangbagian/index',compact(['barangbagian']));
    }

    public function get_barang_bagian()
    {
        $data = Barangbagian::with(['barang','barang.jsatuan','barang.jbarang','barang.jsatuan_besar','bagian'])->get();

    	return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }
    public function update(Request $req)
    {
        $idbagian = $req->idbagian;
        $kdbrg = $req->kdbrg;
        $stoknowbagian = $req->stoknowbagian;

        Barangbagian::where([
                ['idbagian','=',$idbagian],
                ['kdbrg','=',$kdbrg]
        ])->update([ 'stoknowbagian' => $stoknowbagian ]);

         return Redirect()->back()->with(['message' => 'data berhasil di update']);          
    }
}
