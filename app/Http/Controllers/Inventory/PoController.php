<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Pasien,
    Stkawin,Agama,Goldarah,Pendidikan,Pekerjaan,Daerah,
    Registrasi,RegistrasiDet,Reservasi,Penjamin,Caradatang,Bagian,Dokter,Jadwalpraktek,Shift,Mutasikrm,
    Barangbagian,Barang,Jsatuan,Nota,Notadet,Tarif,Pelayanan,Jpesanan,Syaratpembayaran, Jenispembayaran, Statuspesanan, Supplier};
use Auth,DB;

class PoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create_po()
    {
        $jpp = Jpesanan::all();
        $syarat_pembayaran = Syaratpembayaran::all();
        $jpembayaran = Jenispembayaran::all();
        $stpesanan = Statuspesanan::all();
        $supplier = Supplier::all();
        return view('pages/inventory/purchasing/po/create',compact(['jpp','syarat_pembayaran','jpembayaran','stpesanan','supplier']));
    }

    public function get_barang()
    {
        $data = Barang::with(['jsatuan','jsatuan_besar'])->get();
        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }
}
