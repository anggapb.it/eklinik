<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Pemesanan,PemesananDetail,Supplier,Barang};
use App\Http\Requests\StorePpRequest;
use DB,Auth;

class PemesananController extends Controller
{
    public function index()
    {
        return view('pages/inventory/purchasing/pp/index');
    }

    public function get_pp()
    {
        $data = Pemesanan::with(['bagian','stsetuju','supplier','stpo'])->orderByDesc('tglpp')->get();
        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }
    public function get_ppdet(Request $req)
    {
        $nopp = $req->nopp;
        $data = PemesananDetail::with(['barang','barang.jsatuan','barang.jsatuan_besar'])->where('nopp','=',$nopp)->get();
        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function create()
    {
        $supplier = Supplier::all();
        return view('pages/inventory/purchasing/pp/create', compact(['supplier']));
    }

    public function store(Request $req)
    {
        $nopp = $this->getNoPp();
        $pp = Pemesanan::create([
            'nopp' => $nopp,
            'tglpp' => date("Y-m-d", strtotime($req->input('tglkirim'))),
            'idbagian' => 11, // tbl bagian Farmasi
            'idstsetuju' => 2,
            'idstpo' => 1, // Waiting
            'kdsupplier' => $req->supplier,
            'keterangan' => $req->keterangan,
            'userid' => Auth::user()->id,
            'tgljaminput' => date("Y-m-d H:i:s")
        ]);

        foreach($req->get('kdbrg') as $i => $kdbrg){
            $row_brg = Barang::where('kdbrg','=',$kdbrg)->first();

            $qty = $req->get('qty');
            $qty_besar = $req->get('jumlah');
            PemesananDetail::create([
                'nopp' => $pp->nopp,
                'kdbrg' => $kdbrg,
                'idsatuan' => $row_brg->idsatuanbsr, //d
                'qty' => $qty["$i"],
                'catatan' => '',
                'idstpp' => 1,
                'qty_besar' => $qty_besar["$i"],
                'jml_per_box' => ($row_brg->jmlperbox) ? $row_brg->jmlperbox:1 //d
            ]);
        }

        return redirect()->route('inv.pp')->with(['message' => 'data berhasil disimpan']);
    }

    function getNoPp(){
	    $query = DB::connection('klinik_old')->select("SELECT getOtoNopp(now()) as nm");
        $nm = $query[0]->nm;

        return $nm;
	}

    public function destroy($id)
    {
        $pp = Pemesanan::find($id);
        $ppdet = PemesananDetail::where('nopp','=',$id);
        if($ppdet->count() > 0){
            $ppdet->delete();
        }
    	if ($pp->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}
    }

    public function detail($id)
    {
        $pp = Pemesanan::where('nopp','=',$id)->with(['supplier'])->first();
        $ppdet = PemesananDetail::where('nopp','=',$id)->get();

        return view('pages/inventory/purchasing/pp/detail',compact(['pp','ppdet']));
    }

    public function edit($id)
    {
        $pp = Pemesanan::where('nopp','=',$id)->with(['supplier'])->first();
        $ppdet = PemesananDetail::with(['barang','barang.jsatuan','barang.jsatuan_besar'])->where('nopp','=',$id)->get();
        $supplier = Supplier::all();

        return view('pages/inventory/purchasing/pp/edit',compact(['pp','ppdet','supplier']));
    }

    public function update(Request $req)
    {
        $nopp = $req->nopp;
        $pp = Pemesanan::where('nopp','=',$nopp);
        $pp->update([
            'tglpp' => date("Y-m-d", strtotime($req->input('tglkirim'))),
            'idbagian' => 11, // tbl bagian Farmasi
            'idstsetuju' => 2,
            'idstpo' => 1, // Waiting
            'kdsupplier' => $req->supplier,
            'keterangan' => $req->keterangan,
            'userid' => Auth::user()->id,
            'tgljaminput' => date("Y-m-d H:i:s")
        ]);

        $ppdet = PemesananDetail::where('nopp','=',$nopp);
        $ppdet->delete();

        foreach($req->get('kdbrg') as $i => $kdbrg){
            $row_brg = Barang::where('kdbrg','=',$kdbrg)->first();

            $qty = $req->get('qty');
            $qty_besar = $req->get('jumlah');
            PemesananDetail::create([
                'nopp' => $nopp,
                'kdbrg' => $kdbrg,
                'idsatuan' => $row_brg->idsatuanbsr, //d
                'qty' => $qty["$i"],
                'catatan' => '',
                'idstpp' => 1,
                'qty_besar' => $qty_besar["$i"],
                'jml_per_box' => ($row_brg->jmlperbox) ? $row_brg->jmlperbox:1 //d
            ]);
        }

        return redirect()->route('inv.pp')->with(['message' => 'data berhasil diupdate']);


    }

}
