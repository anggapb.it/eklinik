<?php

namespace App\Http\Controllers\Rawatjalan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Pasien,
    Stkawin,Agama,Goldarah,Pendidikan,Pekerjaan,Daerah,
    Registrasi,RegistrasiDet,Reservasi,Penjamin,Caradatang,Bagian,Dokter,Jadwalpraktek,Shift,Mutasikrm,
    Barangbagian,Barang,Jsatuan,Nota,Notadet,Tarif,Pelayanan};
use Auth,DB;

class RegistrasiRjController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $stkawin = Stkawin::all();
        $goldarah = Goldarah::all();
        $agama = Agama::all();
        $pendidikan = Pendidikan::all();
        $pekerjaan = Pekerjaan::all();

        return view('pages/rawatjalan/pasien',compact(['stkawin','goldarah','agama','pendidikan','pekerjaan']));
    }

    public function detail($norm=null)
    {
        if(!empty($norm)){
            $pasien = Pasien::where('norm','=',$norm)->first();
            $nmpasien = $pasien->nmpasien;
            $jnskelamin = $pasien->jkelamin->nmjnskelamin;
        }else{
            $nmpasien = '';
            $jnskelamin = '';
        }
        $penjamin = Penjamin::all();
        $caradatang = Caradatang::all();
        $bagian = Bagian::where('idjnspelayanan','=','1')->get();
        $dokter_praktek = Dokter::all();
        // $dokter_praktek = Jadwalpraktek::select('jadwalpraktek.*','dokter.nmdoktergelar')
        // ->leftJoin('bagian', 'jadwalpraktek.idbagian', '=', 'bagian.idbagian')
        // ->leftJoin('dokter', 'jadwalpraktek.iddokter', '=', 'dokter.iddokter')
        // ->groupBy('dokter.nmdoktergelar')
        // ->orderBy('dokter.nmdoktergelar', 'asc')
        // ->get();
        return view('pages/rawatjalan/registrasi',compact(['penjamin','caradatang','bagian','dokter_praktek','nmpasien','norm','jnskelamin']));
    }

    public function get_daerah()
    {

        $data = Daerah::select('daerah.*')
                ->where([
                    ['daerah.dae_iddaerah','<>',''],
                    ['daerah.idlvldaerah','=','5']
                ])->with(['lvldaerah'])->get();

        return datatables()->of($data)
        ->addColumn('nmlvldaerah', function ($data) {
            return $data->lvldaerah->nmlvldaerah;

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function get_riwayat_pasien(Request $req)
    {
        $riw_pasien = Registrasi::where('norm','=',$req->norm)
                      ->with(['get_penjamin','get_reg_det','get_jpelayanan','get_reg_det.get_bagian','get_reg_det.get_dokter'])
                      ->get();
                      return datatables()->of($riw_pasien)
                      ->addColumn('nmpenjamin', function ($data) {
                          return $data->get_penjamin->nmpenjamin;

                      })
                      ->addColumn('nmjnspelayanan', function ($data) {
                          return $data->get_jpelayanan->nmjnspelayanan;

                      })
                      ->rawColumns(['nmpenjamin','nmjnspelayanan'])
                      ->addIndexColumn()
                      ->make(true);

    }

    public function insert_registrasi(Request $req)
    {
        date_default_timezone_set('Asia/Jakarta');
        $cek_reg = Registrasi::where('noreg','=',$req->input('noreg'))->count();
        $row_bagian = Bagian::where('idbagian','=',$req->input('idbagian'))->first();
        if($cek_reg == 0){
            $alias = $row_bagian->alias;

                $nr = $this->getNoregistrasi($alias);
                $nomid = date('y');
                $noend = str_pad($nr, 6, "0", STR_PAD_LEFT);
                $noregistrasi = $alias.$nomid.$noend;

            //reg
            $reg = new Registrasi();
            $reg->noreg = $noregistrasi;
            $reg->norm = $req->input('norm');
            $reg->idpenjamin = $req->input('idpenjamin');
            $reg->idjnspelayanan = 1;
            $reg->idstpasien = 1;
            $reg->catatan = $req->input('catatan');
            $reg->status = 1; //Blm Diproses
            $reg->save();

            //regdet
            $reg_det = new RegistrasiDet();
            $reg_det->noregdet = 1;
            $reg_det->noreg = $reg->noreg;
            $reg_det->tglreg = date("Y-m-d", strtotime($req->input('tglreg')));
            $reg_det->jamreg = date("H:i:s", strtotime($req->input('jamreg')));
            $reg_det->idshift = $req->input('idshift');
            $reg_det->idbagian = $req->input('idbagian');
            $reg_det->iddokter = $req->input('iddokter');
            $reg_det->idcaradatang = $req->input('idcaradatang');
            $reg_det->tglmasuk = date("Y-m-d");
            $reg_det->jammasuk = date("H:i:s");
            $reg_det->umurtahun = 0;
            $reg_det->umurbulan = 0;
            $reg_det->umurhari = 0;
            $reg_det->idklsrawat = null;
            $reg_det->idklstarif = null;
            $reg_det->idstregistrasi = 1;
            $reg_det->userinput = Auth::user()->id;
            $reg_det->tglinput = date("Y-m-d");
            $reg_det->save();

            //res
            $reservasi = new Reservasi();
            $reservasi->tglreservasi = date("Y-m-d", strtotime($req->input('tglreg')));
            $reservasi->jamreservasi = date("H:i:s", strtotime($req->input('jamreg')));
            $reservasi->idshift = $req->input('idshift');
            $reservasi->norm = $req->input('norm');
            $reservasi->nmpasien = $req->input('nmpasien');
            $reservasi->nohp = 1;
            $reservasi->iddokter = $req->input('iddokter');
            $reservasi->idbagian = $req->input('idbagian');
            $reservasi->idstreservasi = 1;
            $reservasi->idregdet = $reg_det->idregdet;
            $reservasi->noantrian = null;
            $reservasi->userinput = Auth::user()->id;
            $reservasi->tglinput =  date("Y-m-d");
            $reservasi->idstposisipasien =  2;
            $reservasi->stdatang =  1;
            $reservasi->save();

            //ins mutasikrm
            $nostart = 'RJ';
            $nr = $this->getNomutasikrm();
            $nomid = date('y');
            $noend = str_pad($nr, 6, "0", STR_PAD_LEFT);
            $nomutasikrm = $nostart.$nomid.$noend;
            $mutasikrm = new Mutasikrm();
            $mutasikrm->nomutasikrm = $nomutasikrm;
            $mutasikrm->idregdet = $reg_det->idregdet;
            $mutasikrm->idjnsstkrm = 1;
            $mutasikrm->idjnsrefkrm = 1;
            $mutasikrm->tglminta = date("Y-m-d");
            $mutasikrm->jamminta = date("H:i:s");
            $mutasikrm->save();

            $data = array();
            $data['error'] = '';
            $data['message'] = 'Data berhasil ditambahkan';
            $data['reg'] = $reg;
            $data['reg_det'] = $reg_det;
            $data['reservasi'] = $reservasi;
            $data['mutasikrm'] = $mutasikrm;

            return response()->json($data);

        }
    }

    function getNoregistrasi($alias){
        $thn = date('y');
        $data = Registrasi::whereRaw("SUBSTRING(noreg,1,2) = '".$alias."'")
                ->whereRaw("SUBSTRING(noreg,3,2) = '".$thn."'")
                ->count();
		if(empty($data)) $max = 1;
		else $max = $data + 1;
		return $max;
    }

    function getNomutasikrm(){
        $alias = "RJ";
        $thn = date('y');
        $data = Mutasikrm::selectRaw("SUBSTRING(max(nomutasikrm), 7, 4) AS max_np")->whereRaw("SUBSTRING(nomutasikrm,1,2) = '".$alias."'")
                ->whereRaw("SUBSTRING(nomutasikrm,3,2) = '".$thn."'")
                ->first();

		if(empty($data->max_np)) $max = 1;
		else $max = $data->max_np + 1;
		return $max;
    }

    public function transaksi()
    {
        $reg = Registrasi::select('registrasi.*')
        ->with(['get_pasien','get_pasien.jkelamin','get_reg_det',
        'get_reg_det.get_bagian','get_reg_det.get_dokter',
        'get_reg_det.get_shift','get_reg_det.get_cara_datang','get_reg_det.get_reservasi'])->get();

        $bagian = Bagian::where('idjnspelayanan','=','1')->get();
        return view('pages/rawatjalan/transaksi',compact(['bagian','reg']));
    }

    public function get_data(Request $req)
    {
        $tglawal = date("Y-m-d", strtotime($req->tglawal));
        $tglakhir = date("Y-m-d", strtotime($req->tglakhir));
        $bagian = $req->bagian;

        $data = Registrasi::select('registrasi.noreg','registrasi.status','registrasi.norm','pasien.nmpasien','jkelamin.kdjnskelamin'
                ,'bagian.nmbagian','dokter.nmdoktergelar','jkelamin.nmjnskelamin','penjamin.nmpenjamin'
                ,'caradatang.nmcaradatang','registrasi.catatan','registrasidet.tglreg','registrasidet.jamreg'
                ,'shift.nmshift','reservasi.noantrian','nota.nonota')
                ->leftJoin('penjamin', 'penjamin.idpenjamin', '=', 'registrasi.idpenjamin')
                ->leftJoin('pasien', 'pasien.norm', '=', 'registrasi.norm')
                ->leftJoin('jkelamin', 'jkelamin.idjnskelamin', '=', 'pasien.idjnskelamin')
                ->leftJoin('registrasidet', 'registrasi.noreg', '=', 'registrasidet.noreg')
                ->leftJoin('bagian', 'bagian.idbagian', '=', 'registrasidet.idbagian')
                ->leftJoin('caradatang', 'caradatang.idcaradatang', '=', 'registrasidet.idcaradatang')
                ->leftJoin('shift', 'shift.idshift', '=', 'registrasidet.idshift')
                ->leftJoin('reservasi', 'reservasi.idregdet', '=', 'registrasidet.idregdet')
                ->leftJoin('nota', 'nota.idregdet', '=', 'registrasidet.idregdet')
                ->leftJoin('dokter', 'dokter.iddokter', '=', 'registrasidet.iddokter');
        if($req->tglawal != '' && $req->tglakhir != ''){
            $data = $data->whereRaw('registrasidet.tglreg BETWEEN \''.$tglawal.'\'
            AND \''.$tglakhir.'\'');
        }
        if($bagian != ''){
            $data = $data->where('registrasidet.idbagian','=',$bagian);
        }
        $data = $data->where('registrasidet.userbatal','=',null);
        $data = $data->get();
        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }


    public function get_registrasi_proses(Request $req)
    {
        $tglawal = date("Y-m-d", strtotime($req->tglawal));
        $tglakhir = date("Y-m-d", strtotime($req->tglakhir));
        $bagian = $req->bagian;

        $data = Registrasi::select('registrasi.noreg','registrasi.status','registrasi.norm','pasien.nmpasien','jkelamin.kdjnskelamin'
                ,'bagian.nmbagian','dokter.nmdoktergelar','jkelamin.nmjnskelamin','penjamin.nmpenjamin'
                ,'caradatang.nmcaradatang','registrasi.catatan','registrasidet.tglreg','registrasidet.jamreg'
                ,'shift.nmshift','reservasi.noantrian','nota.nonota')
                ->leftJoin('penjamin', 'penjamin.idpenjamin', '=', 'registrasi.idpenjamin')
                ->leftJoin('pasien', 'pasien.norm', '=', 'registrasi.norm')
                ->leftJoin('jkelamin', 'jkelamin.idjnskelamin', '=', 'pasien.idjnskelamin')
                ->leftJoin('registrasidet', 'registrasi.noreg', '=', 'registrasidet.noreg')
                ->leftJoin('bagian', 'bagian.idbagian', '=', 'registrasidet.idbagian')
                ->leftJoin('caradatang', 'caradatang.idcaradatang', '=', 'registrasidet.idcaradatang')
                ->leftJoin('shift', 'shift.idshift', '=', 'registrasidet.idshift')
                ->leftJoin('reservasi', 'reservasi.idregdet', '=', 'registrasidet.idregdet')
                ->leftJoin('nota', 'nota.idregdet', '=', 'registrasidet.idregdet')
                ->leftJoin('dokter', 'dokter.iddokter', '=', 'registrasidet.iddokter');
        if($req->tglawal != '' && $req->tglakhir != ''){
            $data = $data->whereRaw('registrasidet.tglreg BETWEEN \''.$tglawal.'\'
            AND \''.$tglakhir.'\'');
        }
        if($bagian != ''){
            $data = $data->where('registrasidet.idbagian','=',$bagian);
        }
        $data = $data->where('registrasidet.userbatal','=',null);
        $data = $data->where('registrasi.status','=',2); //sudah diinput tindakan atau obat nya
        $data = $data->get();
        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function detail_transaksi($noreg)
    {
        $reg = Registrasi::select('registrasi.*')
               ->where('noreg','=',$noreg)
               ->with(['get_pasien','get_pasien.jkelamin','get_reg_det',
               'get_reg_det.get_bagian','get_reg_det.get_dokter',
               'get_reg_det.get_shift','get_reg_det.get_cara_datang','get_reg_det.get_reservasi'])->first();

        return view('pages/rawatjalan/transaksi-detail',compact(['reg']));
    }

    public function get_tindakan()
    {
        $sql =
<<<EOT
SELECT
	`p`.`kdpelayanan` AS `kditem`,
	`p`.`nmpelayanan` AS `nmitem`,
	`t`.`idklstarif` AS `klstarif`,
	'' AS `idbagian`,
	'' AS `satuankcl`,
	'' AS `satuanbsr`,
IF
	( isnull( `t`.`tarifjs` ), 0, `t`.`tarifjs` ) AS `tarifjs`,
	`t`.`tarifjm` AS `tarifjm`,
	`t`.`tarifjp` AS `tarifjp`,
	`t`.`tarifbhp` AS `tarifbhp`,
	( ( ( `t`.`tarifjs` + `t`.`tarifjm` ) + `t`.`tarifjp` ) + `t`.`tarifbhp` ) AS `ttltarif`,
	`p`.`idstatus` AS `idstatus`,
	'-' AS `stoknowbagian`,
	( ( `t`.`tarifjm` + `t`.`tarifjp` ) + `t`.`tarifbhp` ) AS `hrgbeli`
FROM
	( `pelayanan` `p` JOIN `tarif` `t` )
WHERE
    ( `p`.`kdpelayanan` = `t`.`kdpelayanan` AND left(p.kdpelayanan, 1) <> "B" )
    UNION
SELECT
	`tp`.`idtarifpaket` AS `kditem`,
	`tp`.`nmpaket` AS `nmitem`,
	`tp`.`idklstarif` AS `klstarif`,
	'' AS `idbagian`,
	'' AS `satuankcl`,
	'' AS `satuanbsr`,
	sum( `tpd`.`tarifjs` ) AS `tarifjs`,
	sum( `tpd`.`tarifjm` ) AS `tarifjm`,
	sum( `tpd`.`tarifjp` ) AS `tarifjp`,
	sum( `tpd`.`tarifbhp` ) AS `tarifbhp`,
	(
		( ( sum( `tpd`.`tarifjs` ) + sum( `tpd`.`tarifjm` ) ) + sum( `tpd`.`tarifjp` ) ) + sum( `tpd`.`tarifbhp` )
	) AS `ttltarif`,
	`tp`.`idstatus` AS `idstatus`,
	'-' AS `stoknowbagian`,
	NULL AS `hrgbeli`
FROM
	( `tarifpaket` `tp` JOIN `tarifpaketdet` `tpd` )
WHERE
	( `tp`.`idtarifpaket` = `tpd`.`idtarifpaket` AND left(tp.idtarifpaket, 1) <> "B" )
GROUP BY
	`tp`.`idtarifpaket`
EOT;
    $data = DB::connection('klinik_old')->select($sql);
    // $data = $data->where('idstatus','=','1');
    // $data = $data->whereRaw('left(kditem, 1) <> "B"');
    // $data = $data->get();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }


    public function get_obat()
    {
        $data = Barangbagian::with(['barang','barang.jsatuan'])->get();
            return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function insert_nota_awal(Request $req)
    {
        if(empty($req->nonota) || $req->nonota == '')
		{
            $alias = 'RJ';
			$nr = $this->getNonota();
			$nostart = 'N'.$alias;
			$nomid = date('y');
			$nombln = date('m');
			$noend = str_pad($nr, 5, "0", STR_PAD_LEFT);
			$nonota = $nostart.$nomid.$nombln.$noend;
		} else{
			$nonota = $req->nonota;
		}
        $noreg = $req->noreg;
        $row_reg = Registrasidet::where('noreg','=',$noreg)->first();

        $update_reg = Registrasi::where('noreg','=',$req->noreg)
                     ->update([ 'status' => 2 ]);

        //Insert Nota
        $nota = new Nota();
        $nota->nonota = $nonota;
        $nota->tglnota = date("Y-m-d");
        $nota->jamnota = date("H:i:s");
        $nota->idregdet = $row_reg->idregdet;
        $nota->iddokter = $row_reg->iddokter;
        $nota->idbagian = $row_reg->idbagian;
        $nota->idbagianfar = 11; // Instalasi Farmasi
        $nota->idjnstransaksi = 2; // Nota RJ
        $nota->idsttransaksi = 1; // Nota RJ
        $nota->noorder = 1; // Nota RJ
        $nota->idshift = $row_reg->idshift; //
        $nota->userinput = Auth::user()->id; //
        $nota->tglinput = date("Y-m-d");
        $nota->idstpelayanan = 1;
        $nota->tgljaminput = date("Y-m-d H:i:s");

        $data = array();
        if($nota->save()) {
            $data['nota'] = $nota;
            $data['message'] = 'Silahkan tambahkan Tindakan & Obat / Alkes';
            $data['error'] = '';
          } else {
            $data['message'] = '';
            $data['error'] = 'Gagal';
          }

          return response()->json($data)->setStatusCode(200);
    }

    function getNonota(){
              $alias = 'RJ';
              $thn = date('y');
              $data = Nota::selectRaw("SUBSTRING(max(nonota), 8, 5) AS max_np")
              ->whereRaw("SUBSTRING(nonota,2,2) = '".$alias."'")
              ->whereRaw("SUBSTRING(nonota,4,2) = '".$thn."'")
              ->first();


              if(empty($data->max_np)) $max = 1;
              else $max = $data->max_np + 1;
              return $max;
    }

    public function insert_tindakan(Request $req)
    {
        $tarif = Tarif::where([
            ['kdpelayanan','=',$req->kditem],
            ['idklstarif','=','5']
        ])->first();

        $reg_det = Registrasidet::where('noreg','=',$req->noreg)->first();
        date_default_timezone_set('Asia/Jakarta');

        $tindakan = Notadet::updateOrCreate(
            ['idnotadet' => $req->idnotadet],
            [
                'nonota' => $req->input('nonota'),
                'kditem' => $req->kditem,
                'qty' => $req->qty,
                'tgl_diberikan' => date("Y-m-d H:i:s"),
                'tarifjs' => $tarif->tarifjs,
                'tarifjm' => $tarif->tarifjm,
                'tarifjp' => $tarif->tarifjp,
                'tarifbhp' => $tarif->tarifbhp,
                'diskonjs' => 0,
                'diskonjm' => 0,
                'diskonjp' => 0,
                'diskonbhp' => 0,
                'iddokter' => $reg_det->iddokter
            ]);


        $data = array();

            $data['message'] = 'Berhasil';
            $data['error'] = '';

          return response()->json($data)->setStatusCode(200);
    }


    public function insert_obat(Request $req)
    {
        $brg = Barang::where([
            ['kdbrg','=',$req->kditem]
        ])->with(['jsatuan'])->first();

        $reg_det = Registrasidet::where('noreg','=',$req->noreg)->first();
        date_default_timezone_set('Asia/Jakarta');

        //set koder
        if(isset($req->idnotadet)):
            $row_nd = Notadet::where('idnotadet','=',$req->idnotadet)->first();
            $koder = $row_nd->koder;
        else:
            $count_obat = Notadet::where([
                ['nonota','=',$req->nonota],
                ['koder','!=',null]
                ])->count();
            $koder = $count_obat + 1;
        endif;
        $tindakan = Notadet::updateOrCreate(
            ['idnotadet' => $req->idnotadet],
            [
                'nonota' => $req->nonota,
                'kditem' => $req->kditem,
                'qty' => $req->qty,
                'tgl_diberikan' => date("Y-m-d H:i:s"),
                'tarifjs' => 0,
                'tarifjm' => 0,
                'tarifjp' => 0,
                'tarifbhp' => $brg->hrgjual,
                'diskonjs' => 0,
                'diskonjm' => 0,
                'diskonjp' => 0,
                'diskonbhp' => 0,
                'idsatuan' => $brg->jsatuan->idsatuan,
                'hrgjual' => $brg->hrgjual,
                'hrgbeli' => $brg->hrgbeli,
                'aturanpakai' => $req->aturan_pakai,
                'iddokter' => $reg_det->iddokter,
                'koder' => $koder,
            ]);


        $data = array();

            $data['message'] = 'Berhasil';
            $data['error'] = '';

          return response()->json($data)->setStatusCode(200);
    }

    public function get_tindakan_by_nota(Request $req)
    {
        $nonota = $req->nonota;

        $data = Notadet::selectRaw("notadet.qty AS qty, notadet.idnotadet, notadet.iddokter, notadet.kditem,
        (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) AS tarif,
        (notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS tarif2,
        (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS diskon,
        nota.diskon AS diskonr, nota.uangr AS uangr, notadet.dijamin AS dijaminrp, notadet.dijamin AS dijaminrpf,
        ((notadet.dijamin/((notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp)))*100) AS dijaminprsn,
        ((notadet.dijamin/(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)))*100) AS dijaminprsnf,
        (notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin AS selisih,
        (notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin AS selisihf,
        notadet.stdijamin AS dijamin, nota.stplafond AS stplafond,
        notadet.obat_luar as obat_luar, notadet.nmracik, notadet.tgl_diberikan, dokter.nmdoktergelar, pelayanan.nmpelayanan as nmitem,
        null as diskon")
        ->leftJoin('nota', 'nota.nonota', '=', 'notadet.nonota')
        ->leftJoin('dokter', 'dokter.iddokter', '=', 'notadet.iddokter')
        ->leftJoin('pelayanan', 'pelayanan.kdpelayanan', '=', 'notadet.kditem')
        ->where([
            ['notadet.koder','=',null],
            ['notadet.nonota','=',$nonota],
        ])->get();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);

    }

    public function get_obat_by_nota(Request $req)
    {
        $nonota = $req->nonota;

        $data = Notadet::selectRaw("notadet.qty AS qty, notadet.idnotadet, notadet.iddokter, notadet.kditem,
        (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) AS tarif,
        (notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS tarif2,
        (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS diskon,
        nota.diskon AS diskonr, nota.uangr AS uangr, notadet.dijamin AS dijaminrp, notadet.dijamin AS dijaminrpf,
        ((notadet.dijamin/((notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp)))*100) AS dijaminprsn,
        ((notadet.dijamin/(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)))*100) AS dijaminprsnf,
        (notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin AS selisih,
        (notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin AS selisihf,
        notadet.stdijamin AS dijamin, nota.stplafond AS stplafond,
        notadet.obat_luar as obat_luar, notadet.nmracik, notadet.tgl_diberikan, dokter.nmdoktergelar, barang.nmbrg as nmitem, barang.hrgbeli,
        barang.hrgjual, jsatuan.nmsatuan, notadet.aturanpakai,
        null as diskon")
        ->leftJoin('nota', 'nota.nonota', '=', 'notadet.nonota')
        ->leftJoin('dokter', 'dokter.iddokter', '=', 'notadet.iddokter')
        ->leftJoin('barang', 'barang.kdbrg', '=', 'notadet.kditem')
        ->leftJoin('jsatuan', 'jsatuan.idsatuan', '=', 'barang.idsatuankcl')
        ->where([
            ['notadet.koder','!=',null],
            ['notadet.nonota','=',$nonota],
        ])->get();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);

    }

    public function hapus_notadet($id)
    {
        $notadet = Notadet::find($id);
        $data = array();
        if ($notadet->delete()) {
            $data['message'] = 'Data Berhasil Dihapus';
            $data['error'] = '';
		}else{
            $data['message'] = '';
            $data['error'] = 'Data Gagal Dihapus';
        }

        return response()->json($data)->setStatusCode(200);
    }


}
