<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Pasien,
    Stkawin,Agama,Goldarah,Pendidikan,Pekerjaan,Daerah,
    Registrasi,RegistrasiDet,Reservasi,Penjamin,Caradatang,Bagian,Dokter,Jadwalpraktek,Shift};


class ApiController extends Controller
{
    public function get_shift()
    {
        date_default_timezone_set('Asia/Jakarta');
        $waktu = date("H:i:s");
        $shift = Shift::whereRaw("CURTIME() BETWEEN darijam AND sampaijam")->first();
        
        $arr = array();
        $arr['data'] = $shift;

        return response()->json($arr)->setStatusCode(200);
	}
}