<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jadwalpraktek;
use App\Models\Bagian;
use App\Models\Dokter;
use App\Models\Hari;
use App\Models\Shift;
use App\Models\Jampraktek;

class JadwalpraktekController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $bagian = Bagian::all();
        $dokter = Dokter::all();
        $hari = Hari::all();
        $shift = Shift::all();
        $jampraktek = Jampraktek::all();
    	return view('pages/master/jadwal_praktek', compact(['bagian','dokter','hari','shift','jampraktek']));
    }

    public function get_data()
    {
    	$jadwalpraktek = Jadwalpraktek::with(['bagian','dokter','hari','shift'])->get();

    	return datatables()->of($jadwalpraktek)
       	->addColumn('nmbagian', function ($data) {
            return $data->bagian->nmbagian;

        }) 
        ->addColumn('nmdokter', function ($data) {
            return $data->dokter->nmdokter;

        })
        ->addColumn('nmhari', function ($data) {
            return $data->hari->nmhari;

        })
        ->addColumn('nmshift', function ($data) {
            return $data->shift->nmshift;
            
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Jadwalpraktek::UpdateOrCreate(
            ['idjadwalpraktek' => $req->idjadwalpraktek],
            [
            	'idbagian' => $req->idbagian,
                'iddokter' => $req->iddokter,
                'idhari' => $req->idhari,
                'idshift' => $req->idshift,
                'jampraktek' => $req->jampraktek,
                'keterangan' => $req->keterangan
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$jadwalpraktek = Jadwalpraktek::find($id);
    	if ($jadwalpraktek->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}

    }

}
