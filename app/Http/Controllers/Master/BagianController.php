<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bagian;
use App\Models\Lvlbagian;
use App\Models\Jenishirarki;
use App\Models\Jpelayanan;
use App\Models\Bidangperawatan;

class BagianController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $lvlbagian = Lvlbagian::all();
        $jhirarki = Jenishirarki::all();
        $jpelayanan = Jpelayanan::all();
        $bdgrawat = Bidangperawatan::all();
    	return view('pages/master/bagian', compact(['lvlbagian','jhirarki','jpelayanan','bdgrawat']));
    }

    public function get_data()
    {
    	$bagian = Bagian::with(['lvlbagian','jhirarki','jpelayanan','bdgrawat'])->get();

    	return datatables()->of($bagian)
        ->addColumn('nmlvlbagian', function ($data) {
            return $data->lvlbagian->nmlvlbagian;

        })
        ->addColumn('nmjnshirarki', function ($data) {
            return $data->jhirarki->nmjnshirarki;

        })
        ->addColumn('nmjnspelayanan', function ($data) {
            return $data->jpelayanan->nmjnspelayanan;

        })
        ->addColumn('nmbdgrawat', function ($data) {
            if(isset($data->bdgrawat->nmbdgrawat)){
                return $data->bdgrawat->nmbdgrawat;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Bagian::UpdateOrCreate(
            ['idbagian' => $req->idbagian],
            [
                'kdbagian' => $req->kdbagian,
                'nmbagian' => $req->nmbagian,
                'alias' => $req->alias,
                'idlvlbagian' => $req->idlvlbagian,
                'idjnshirarki' => $req->idjnshirarki,
                'idjnspelayanan' => $req->idjnspelayanan,
                'idbdgrawat' => $req->idbdgrawat
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$bagian = Bagian::find($id);
    	if ($bagian->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}

    }

}
