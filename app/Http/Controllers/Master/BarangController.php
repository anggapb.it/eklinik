<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\Status;
use App\Models\Supplier;
use App\Models\Pabrik;
use App\Models\Jsatuan;
use App\Models\Jenisbarang;
use App\Models\Kelompokbarang;

class BarangController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $barang = Barang::all();
        $status = Status::all();
        $supplier = Supplier::all();
        $pabrik = Pabrik::all();
        $jsatuan = Jsatuan::all();
        $jbarang = Jenisbarang::all();
        $klpbarang = Kelompokbarang::all();
    	return view('pages/master/barang', compact(['barang','status','supplier','pabrik','jsatuan' ,'jbarang','klpbarang']));
    }

    public function get_data()
    {
    	$barang = Barang::with(['pabrik','jsatuan_besar','jbarang','jsatuan','klpbrg'])->get();

    	return datatables()->of($barang)
        ->addColumn('nmklpbrg', function ($data) {
            return $data->klpbrg->nmklpbarang;

        }) 
        ->addColumn('nmsatuankecil', function ($data) {
            return $data->jsatuan->nmsatuan;

        })
        ->addColumn('nmjnsbrg', function ($data) {
            return $data->jbarang->nmjnsbrg;

        })
        ->addColumn('nmsatuanbesar', function ($data) {
            return $data->jsatuan_besar->nmsatuan;

        })
        ->addColumn('nmpabrik', function ($data) {
            if(isset($data->pabrik->nmpabrik)){
                return $data->pabrik->nmpabrik;
            } else{
                return '';
            }

        }) 
        ->addColumn('nmsupplier', function ($data) {
            if(isset($data->supplier->nmsupplier)){
                return $data->supplier->nmsupplier;
            } else{
                return '';
            }

        }) 
        ->addColumn('nmstatus', function ($data) {
            return $data->status->nmstatus;

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Barang::UpdateOrCreate(
            ['kdbrg' => $req->kdbrg],
            [
                'nmbrg' => $req->nmbrg,
                'idklpbrg' => $req->idklpbrg,
                'idjnsbrg' => $req->idjnsbrg,
                'idsatuankcl' => $req->idsatuankcl,
                'idsatuanbsr' => $req->idsatuanbsr,
                'jmlperbox' => $req->jmlperbox,
                'hrgbeli' => $req->hrgbeli,
                'hrgjual' => $req->hrgjual,
                'idpabrik' => $req->idpabrik,
                'kdsupplier' => $req->kdsupplier,
                'idstatus' => $req->idstatus,
                'keterangan' => $req->keterangan
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$barang = Barang::where('kdbrg','=',$id);
    	if ($barang->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}

    }

}
