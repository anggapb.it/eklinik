<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Statuspasien;
use App\Models\Jpelayanan;

class StatuspasienController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jpelayanan = Jpelayanan::all();
        return view('pages/master/status_pasien',compact(['jpelayanan']));
    }

    public function get_data()
    {
        $data = Statuspasien::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Statuspasien::updateOrCreate(
            ['idstpasien' => $req->idstpasien],
            [
            	'idjnspelayanan' => $req->idjnspelayanan,
                'kdstpasien' => $req->kdstpasien,
                'nmstpasien' => $req->nmstpasien
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Statuspasien::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
