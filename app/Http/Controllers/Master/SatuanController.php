<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Satuan;

class SatuanController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/satuan');
    }

    public function get_data()
    {
        $data = Satuan::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Satuan::updateOrCreate(
            ['idsatuan' => $req->idsatuan],
            [
                'kdsatuan' => $req->kdsatuan,
                'nmsatuan' => $req->nmsatuan
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Satuan::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
