<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penyakit;
use App\Models\Jenishirarki;

class PenyakitController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jhirarki = Jenishirarki::all();
        $penyakit = Penyakit::all();
    	return view('pages/master/penyakit', compact(['jhirarki','penyakit']));
    }

    public function get_data()
    {
    	$penyakit = Penyakit::with(['jhirarki'])->get();

    	return datatables()->of($penyakit)
        ->addColumn('nmjnshirarki', function ($data) {
            if(isset($data->jhirarki->nmjnshirarki)){
                return $data->jhirarki->nmjnshirarki;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {
    	$tindakan = Penyakit::UpdateOrCreate(
    	['idpenyakit' => $req->idpenyakit],
    	[	
    		'kdpenyakit' => $req->kdpenyakit,
    		'nmpenyakit' => $req->nmpenyakit,
    		'nmpenyakiteng' => $req->nmpenyakiteng,
    		'idjnshirarki' => $req->idjnshirarki,
    		'pen_idpenyakit' => $req->pen_idpenyakit,
    		'kategori' => $req->kategori,
    		'dtd' => $req->dtd
    	]);

    	return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$penyakit = Penyakit::where('idpenyakit', $id); 
    	if ($penyakit->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'daata gagal dihapus']);
    	}
    }
}
