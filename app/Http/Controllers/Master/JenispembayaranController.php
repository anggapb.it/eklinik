<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jenispembayaran;

class JenispembayaranController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/jenis_pembayaran');
    }

    public function get_data()
    {
        $data = Jenispembayaran::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Jenispembayaran::updateOrCreate(
            ['idjnspembayaran' => $req->idjnspembayaran],
            [
                'kdjnspembayaran' => $req->kdjnspembayaran,
                'nmjnspembayaran' => $req->nmjnspembayaran
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Jenispembayaran::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
