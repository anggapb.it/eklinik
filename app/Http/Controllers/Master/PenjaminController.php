<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penjamin;
use App\Models\Jenispenjamin;
use App\Models\Status;

class PenjaminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $penjamin = Penjamin::all();
        $jpenjamin = Jenispenjamin::all();
        $status = Status::all();
    	return view('pages/master/penjamin', compact(['penjamin','jpenjamin','status']));
    }

    public function get_data()
    {
    	$penjamin = Penjamin::with(['jpenjamin','status'])->get();

    	return datatables()->of($penjamin)
        ->addColumn('nmjnspenjamin', function ($data) {
            if(isset($data->jpenjamin->nmjnspenjamin)){
                return $data->jpenjamin->nmjnspenjamin;
            } else{
                return '';
            }

        })
        ->addColumn('nmstatus', function ($data) {
            if(isset($data->status->nmstatus)){
                return $data->status->nmstatus;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Penjamin::UpdateOrCreate(
            ['idpenjamin' => $req->idpenjamin],
            [
                'kdpenjamin' => $req->kdpenjamin,
                'nmpenjamin' => $req->nmpenjamin,
                'idjnspenjamin' => $req->idjnspenjamin,
                'alamat' => $req->alamat,
                'notelp' => $req->notelp,
                'nofax' => $req->nofax,
                'email' => $req->email,
                'website' => $req->website,
                'nmcp' => $req->nmcp,
                'nohp' => $req->nohp,
                'tglawal' => date("Y-m-d", strtotime($req->tglawal)),
                'tglakhir' => date("Y-m-d", strtotime($req->tglakhir)),
                'idstatus' => $req->idstatus,
                'infoumum' => $req->infoumum,
                'inforj' => $req->inforj,
                'infori' => $req->infori                

            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $penjamin = Penjamin::where('idpenjamin','=',$id); 
        if ($penjamin->delete()){
            return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
        }else{
            return Redirect()->back()->with(['message' => 'data gagal dihapus']);
        }

    }

} 