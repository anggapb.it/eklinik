<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jenistenagamedis;

class JenistenagamedisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/jenis_tenaga_medis');
    }

    public function get_data()
    {
        $data = Jenistenagamedis::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Jenistenagamedis::updateOrCreate(
            ['idjnstenagamedis' => $req->idjnstenagamedis],
            [
                'kdjnstenagamedis' => $req->kdjnstenagamedis,
                'nmjnstenagamedis' => $req->nmjnstenagamedis
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Jenistenagamedis::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
