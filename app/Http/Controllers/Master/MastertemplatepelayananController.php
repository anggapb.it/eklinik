<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Templatepelayanan;
use App\Models\Jenishirarki;
use App\Models\Status;


class MastertemplatepelayananController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $jhirarki = Jenishirarki::all();
        $templatepelayanan = Templatepelayanan::all();
        $status = Status::all();
    	return view('pages/master/master_template_pelayanan', compact(['templatepelayanan','jhirarki','status']));
    }

    public function get_data()
    {
    	$templatepelayanan = Templatepelayanan::with(['jhirarki','status'])->get();

    	return datatables()->of($templatepelayanan)
       ->addColumn('nmjnshirarki', function ($data) {
            if(isset($data->jhirarki->nmjnshirarki)){
                return $data->jhirarki->nmjnshirarki;
            } else{
                return '';
            }

        })

       ->addColumn('nmstatus', function ($data) {
            if(isset($data->status->nmstatus)){
                return $data->status->nmstatus;
            } else{
                return '';
            }

        })
       ->addColumn('nmtemplayanan', function ($data) {
            if(isset($data->templatepelayanan->nmtemplayanan)){
                return $data->templatepelayanan->nmtemplayanan;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Templatepelayanan::UpdateOrCreate(
            ['idtemplayanan' => $req->idtemplayanan],
            [
                'idjnshirarki' => $req->idjnshirarki,
				'tem_idtemplayanan' => $req->tem_idtemplayanan,
                'idstatus' => $req->idstatus,
                'nmtemplayanan' => $req->nmtemplayanan,
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$templatepelayanan = Templatepelayanan::where('idtemplayanan','=',$id);
    	if ($templatepelayanan->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}
    }
}