<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bidangperawatan;

class BidangperawatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/bidang_perawatan');
    }

    public function get_data()
    {
        $data = Bidangperawatan::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Bidangperawatan::updateOrCreate(
            ['idbdgrawat' => $req->idbdgrawat],
            [
                'kdbdgrawat' => $req->kdbdgrawat,
                'nmbdgrawat' => $req->nmbdgrawat
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Bidangperawatan::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
