<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jenisbarang;

class JenisbarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/jenis_barang');
    }

    public function get_data()
    {
        $data = Jenisbarang::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Jenisbarang::updateOrCreate(
            ['idjnsbrg' => $req->idjnsbrg],
            [
                'kdjnsbrg' => $req->kdjnsbrg,
                'nmjnsbrg' => $req->nmjnsbrg ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Jenisbarang::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
