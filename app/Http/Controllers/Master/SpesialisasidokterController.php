<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Spesialisasidokter;

class SpesialisasidokterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/spesialisasi_dokter');
    }

    public function get_data()
    {
        $data = Spesialisasidokter::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Spesialisasidokter::updateOrCreate(
            ['idspesialisasi' => $req->idspesialisasi],
            [
                'kdspesialisasi' => $req->kdspesialisasi,
                'nmspesialisasi' => $req->nmspesialisasi
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Spesialisasidokter::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
