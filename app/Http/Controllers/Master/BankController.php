<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bank;

class BankController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('pages/master/bank');
    }

    public function get_data()
    {
    	$bank = Bank::all();

    	return datatables()->of($bank)
    	->addIndexColumn()
    	->make(true);
    }

    public function store(Request $req)
    {
    	$tindakan = Bank::UpdateOrCreate(
    	['idbank' => $req->idbank],
    	[	
    		'kdbank' => $req->kdbank,
    		'nmbank' => $req->nmbank
    	]);

    	return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$bank = Bank::find($id);
    	if ($bank->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'daata gagal dihapus']);
    	}
    }
}
