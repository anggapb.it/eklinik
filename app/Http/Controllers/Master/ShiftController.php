<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shift;

class ShiftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/shift');
    }

    public function get_data()
    {
        $data = Shift::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Shift::updateOrCreate(
             ['idshift' => $req->idshift],
            [
                'kdshift' => $req->kdshift,
                'nmshift' => $req->nmshift,
                'darijam' => $req->darijam,
                'sampaijam' => $req->sampaijam
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Shift::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
