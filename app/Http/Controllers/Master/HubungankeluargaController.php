<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\hubungankeluarga;

class hubungankeluargaController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/hubungan_keluarga');
    }

    public function get_data()
    {
        $data = hubungankeluarga::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = hubungankeluarga::updateOrCreate(
            ['idhubkeluarga' => $req->idhubkeluarga],
            [
                'kdhubkeluarga' => $req->kdhubkeluarga,
                'nmhubkeluarga' => $req->nmhubkeluarga
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = hubungankeluarga::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
