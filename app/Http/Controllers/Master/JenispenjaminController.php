<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jenispenjamin;

class JenispenjaminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/jenis_penjamin');
    }

    public function get_data()
    {
        $data = Jenispenjamin::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Jenispenjamin::updateOrCreate(
            ['idjnspenjamin' => $req->idjnspenjamin],
            [
                'kdjnspenjamin' => $req->kdjnspenjamin,
                'nmjnspenjamin' => $req->nmjnspenjamin
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Jenispenjamin::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
