<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Statusposisipasien;

class StatusposisipasienController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/status_posisi_pasien');
    }

    public function get_data()
    {
        $data = Statusposisipasien::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Statusposisipasien::updateOrCreate(
            ['idstposisipasien' => $req->idstposisipasien],
            [
                'kdstposisipasien' => $req->kdstposisipasien,
                'nmstposisipasien' => $req->nmstposisipasien
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Statusposisipasien::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
