<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jkelamin;

class JeniskelaminController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/jenis_kelamin');
    }

    public function get_data()
    {
        $data = Jkelamin::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Jkelamin::updateOrCreate(
            ['idjnskelamin' => $req->idjnskelamin],
            [
                'kdjnskelamin' => $req->kdjnskelamin,
                'nmjnskelamin' => $req->nmjnskelamin
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Jkelamin::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
