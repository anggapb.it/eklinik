<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carakeluar;

class CarakeluarController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/cara_keluar');
    }

    public function get_data()
    {
        $data = Carakeluar::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Carakeluar::updateOrCreate(
            ['idcarakeluar' => $req->idcarakeluar],
            [
                'kdcarakeluar' => $req->kdcarakeluar,
                'nmcarakeluar' => $req->nmcarakeluar
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Carakeluar::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
