<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kamar;
use App\Models\Bagian;

class KamarController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$bagian = Bagian::all();
        return view('pages/master/kamar', compact(['bagian']));
    }

    public function get_data()
    {
        $data = Kamar::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Kamar::updateOrCreate(
            ['idkamar' => $req->idkamar],
            [
                'idbagian' => $req->idbagian,                
                'kdkamar' => $req->kdkamar,
                'nmkamar' => $req->nmkamar,
                'fasilitas' => $req->fasilitas,
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Kamar::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
