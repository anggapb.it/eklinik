<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pelayanan;
use App\Models\JenisPelayanan;
use App\Models\Jenishirarki;
use App\Models\Status;

class PelayananController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $pelayanan = Pelayanan::all();
        $jpelayanan = JenisPelayanan::all();
        $jhirarki = Jenishirarki::all();
        $status = Status::all();
    	return view('pages/master/pelayanan', compact(['pelayanan','jpelayanan','jhirarki','status']));
    }

    public function get_data()
    {
    	$pelayanan = Pelayanan::with(['jpelayanan','jhirarki','status'])->get();

    	return datatables()->of($pelayanan)
        ->addColumn('nmjnspelayanan', function ($data) {
            return $data->jPelayanan->nmjnspelayanan;

        }) 
        ->addColumn('nmjnshirarki', function ($data) {
            return $data->jhirarki->nmjnshirarki;

        })
        ->addColumn('nmstatus', function ($data) {
            return $data->status->nmstatus;

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Pelayanan::UpdateOrCreate(
            ['kdpelayanan' => $req->kdpelayanan],
            [
                'nourut' => $req->nourut,
                'nmpelayanan' => $req->nmpelayanan,
                'idjnspelayanan' => $req->idjnspelayanan,
                'idjnshirarki' => $req->idjnshirarki,
                'idstatus' => $req->idstatus,
                'pel_kdpelayanan' => $req->pel_kdpelayanan
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$pelayanan = Pelayanan::where('kdpelayanan','=',$id);
    	if ($pelayanan->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}

    }

}
