<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Syaratpembayaran;

class SyaratpembayaranController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/syarat_pembayaran');
    }

    public function get_data()
    {
        $data = Syaratpembayaran::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Syaratpembayaran::updateOrCreate(
            ['idsypembayaran' => $req->idsypembayaran],
            [
                'kdsypembayaran' => $req->kdsypembayaran,
                'nmsypembayaran' => $req->nmsypembayaran
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Syaratpembayaran::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
