<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Templateobat;
use App\Models\Jenishirarki;
use App\Models\Status;


class MastertemplateobatController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $jhirarki = Jenishirarki::all();
        $templateobat = Templateobat::all();
        $status = Status::all();
    	return view('pages/master/master_template_obat', compact(['templateobat','jhirarki','status']));
    }

    public function get_data()
    {
    	$templateobat = Templateobat::with(['jhirarki','status'])->get();

    	return datatables()->of($templateobat)
       ->addColumn('nmjnshirarki', function ($data) {
            if(isset($data->jhirarki->nmjnshirarki)){
                return $data->jhirarki->nmjnshirarki;
            } else{
                return '';
            }

        })

       ->addColumn('nmstatus', function ($data) {
            if(isset($data->status->nmstatus)){
                return $data->status->nmstatus;
            } else{
                return '';
            }

        })
       ->addColumn('nmtempobat', function ($data) {
            if(isset($data->templateobat->nmtempobat)){
                return $data->templateobat->nmtempobat;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Templateobat::UpdateOrCreate(
            ['idtempobat' => $req->idtempobat],
            [
                'idjnshirarki' => $req->idjnshirarki,
				'tem_idtempobat' => $req->tem_idtempobat,
                'idstatus' => $req->idstatus,
                'nmtempobat' => $req->nmtempobat,
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$templateobat = Templateobat::where('idtempobat','=',$id);
    	if ($templateobat->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}
    }
}