<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dokter;
use App\Models\Jenistenagamedis;
use App\Models\Jkelamin;
use App\Models\Spesialisasidokter;
use App\Models\Status;
use App\Models\Statusdokter;
use App\Models\Bagian;

class DokterController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jtenagamedis = Jenistenagamedis::all();
        $jkelamin = Jkelamin::all();
        $spesialisasi = Spesialisasidokter::all();
        $status = Status::all();
        $stdokter = Statusdokter::all();
        $bagian = Bagian::all();
    	return view('pages/master/dokter', compact(['jtenagamedis','jkelamin','spesialisasi','status','stdokter','bagian']));
    }

    public function get_data()
    {
    	$dokter = Dokter::with(['jtenagamedis','jkelamin','spesialisasi','status','stdokter'])->get();

    	return datatables()->of($dokter)
        ->addColumn('nmjnstenagamedis', function ($data) {
            if(isset($data->jtenagamedis->nmjnstenagamedis)){
                return $data->jtenagamedis->nmjnstenagamedis;
            } else{
                return '';
            }

        })
        ->addColumn('nmjnskelamin', function ($data) {
            if(isset($data->jkelamin->nmjnskelamin)){
                return $data->jkelamin->nmjnskelamin;
            } else{
                return '';
            }

        })
        ->addColumn('nmspesialisasi', function ($data) {
            if(isset($data->spesialisasi->nmspesialisasi)){
                return $data->spesialisasi->nmspesialisasi;
            } else{
                return '';
            }

        })
        ->addColumn('nmstatus', function ($data) {
            if(isset($data->status->nmstatus)){
                return $data->status->nmstatus;
            } else{
                return '';
            }

        })
        ->addColumn('nmstdokter', function ($data) {
            if(isset($data->stdokter->nmstdokter)){
                return $data->stdokter->nmstdokter;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {
    	$tindakan = Dokter::UpdateOrCreate(
    	['iddokter' => $req->iddokter],
    	[	
    		'kddokter' => $req->kddokter,
    		'idjnstenagamedis' => $req->idjnstenagamedis,
    		'nmdokter' => $req->nmdokter,
    		'nmdoktergelar' => $req->nmdoktergelar,
    		'idjnskelamin' => $req->idjnskelamin,
    		'tptlahir' => $req->tptlahir,
    		'tgllahir' => date("Y-m-d", strtotime($req->tgllahir)),
    		'alamat' => $req->alamat,
    		'notelp' => $req->notelp,
    		'nohp' => $req->nohp,
    		'idspesialisasi' => $req->idspesialisasi,
    		'idstatus' => $req->idstatus,
    		'idstdokter' => $req->idstdokter,
    		'catatan' => $req->catatan,
    		'keterangan' => $req->keterangan

    	]);

    	return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$dokter = Dokter::where('iddokter', $id); 
    	if ($dokter->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'daata gagal dihapus']);
    	}
    }
}
