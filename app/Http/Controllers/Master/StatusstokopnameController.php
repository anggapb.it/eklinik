<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Statusstokopname;

class StatusstokopnameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/status_stok_opname');
    }

    public function get_data()
    {
        $data = Statusstokopname::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Statusstokopname::updateOrCreate(
            ['idstso' => $req->idstso],
            [
                'kdstso' => $req->kdstso,
                'nmstso' => $req->nmstso
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Statusstokopname::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
