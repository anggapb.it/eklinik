<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pabrik;

class PabrikController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/pabrik');
    }

    public function get_data()
    {
        $data = Pabrik::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Pabrik::updateOrCreate(
            ['idpabrik' => $req->idpabrik],
            [
                'kdpabrik' => $req->kdpabrik,
                'nmpabrik' => $req->nmpabrik,
                'alamat' => $req->alamat,
                'keterangan' => $req->keterangan
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Pabrik::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
