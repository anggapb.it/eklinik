<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Daerah;
use App\Models\Jenishirarki;
use App\Models\Lvldaerah;


class DaerahController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $lvldaerah = Lvldaerah::all();
        $jhirarki = Jenishirarki::all();
        $daerah = Daerah::all();
    	return view('pages/master/daerah', compact(['daerah','lvldaerah','jhirarki']));
    }

    public function get_data()
    {
    	$daerah = Daerah::with(['lvldaerah'])->get();

    	return datatables()->of($daerah)
       ->addColumn('nmlvldaerah', function ($data) {
            if(isset($data->lvldaerah->nmlvldaerah)){
                return $data->lvldaerah->nmlvldaerah;
            } else{
                return '';
            }

        }) 
       ->addColumn('nmjnshirarki', function ($data) {
            if(isset($data->jhirarki->nmjnshirarki)){
                return $data->jhirarki->nmjnshirarki;
            } else{
                return '';
            }

        }) 
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Daerah::UpdateOrCreate(
            ['iddaerah' => $req->iddaerah],
            [
                'kddaerah' => $req->kddaerah,
                'nmdaerah' => $req->nmdaerah,
                'idjnshirarki' => $req->idjnshirarki,
                'idlvldaerah' => $req->idlvldaerah,
                'nmdaerah' => $req->nmdaerah,
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$daerah = daerah::find($id);
    	if ($daerah->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}

    }

}
