<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Statuspelayanan;

class StatuspelayananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/status_pelayanan');
    }

    public function get_data()
    {
        $data = Statuspelayanan::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Statuspelayanan::updateOrCreate(
            ['idstpelayanan' => $req->idstpelayanan],
            [
                'kdstpelayanan' => $req->kdstpelayanan,
                'nmstpelayanan' => $req->nmstpelayanan
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Statuspelayanan::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
