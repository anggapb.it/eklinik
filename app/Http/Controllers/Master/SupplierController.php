<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Status;
use App\Models\Bank;
use Auth;

class SupplierController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        $supplier = Supplier::all();
        $status = Status::all();
        $bank = Bank::all();

    	return view('pages/master/supplier', compact(['supplier','status','bank']));
    }

    public function get_data()
     {
    	$supplier = Supplier::with(['bank','status'])->get();

    	return datatables()->of($supplier)
        ->addColumn('nmbank', function ($data) {
            if(isset($data->bank->nmbank)){
                return $data->bank->nmbank;
            } else{
                return '';
            }

        })
        ->addColumn('nmstatus', function ($data) {
            if(isset($data->status->nmstatus)){
                return $data->status->nmstatus;
            } else{
                return '';
            }

        })
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Supplier::UpdateOrCreate(
            ['kdsupplier' => $req->kdsupplier],
            [
                'tgldaftar' => date("Y-m-d", strtotime($req->tgldaftar)),
                'nmsupplier' => $req->nmsupplier,
                'alamat' => $req->alamat,
                'notelp' => $req->notelp,
                'nofax' => $req->nofax,
                'email' => $req->email,
                'website' => $req->website,
                'kontakperson' => $req->kontakperson,
                'nohp' => $req->nohp,
                'npwp' => $req->npwp,
                'idbank' => $req->idbank,
                'norek' => $req->norek,
                'atasnama' => $req->atasnama,
                'keterangan' => $req->keterangan,
                'idstatus' => $req->idstatus,
                'userid' => Auth::user()->id,
                'tglinput' => date("Y-m-d"),
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
    	$supplier = Supplier::where('kdsupplier','=',$id);
    	if ($supplier->delete()){
    		return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
    	}else{
    		return Redirect()->back()->with(['message' => 'data gagal dihapus']);
    	}

    }

}
