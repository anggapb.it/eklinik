<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jenisidentitas;

class JenisidentitasController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/master/jenis_identitas');
    }

    public function get_data()
    {
        $data = Jenisidentitas::all();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $req)
    {

        $tindakan = Jenisidentitas::updateOrCreate(
            ['idjnsidentitas' => $req->idjnsidentitas],
            [
                'kdjnsidentitas' => $req->kdjnsidentitas,
                'nmjnsidentitas' => $req->nmjnsidentitas
            ]);

            return Redirect()->back()->with(['message' => 'data berhasil disimpan']);
    }

    public function destroy($id)
    {
        $data = Jenisidentitas::find($id);
        if ($data->delete()) {
			return Redirect()->back()->with(['message' => 'data berhasil dihapus']);
		}else{
			return Redirect()->back()->with(['error' => 'data gagal dihapus']);
		}
    }
}
