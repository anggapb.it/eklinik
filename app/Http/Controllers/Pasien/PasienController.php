<?php

namespace App\Http\Controllers\Pasien;

use App\Http\Controllers\Controller;
use App\Models\Pasien;
use Illuminate\Http\Request;

class PasienController extends Controller
{
    public function store(Request $req)
    {
        if(is_null($req->norm) || $req->norm == '') $np = $this->getnorm();
		else $np = $req->norm;
		$norm = str_pad($np, 10, "0", STR_PAD_LEFT);
        $cek_pasien = Pasien::where('norm','=',$norm)->count();
        if($cek_pasien > 0):
            $pasien = Pasien::where('norm','=',$norm)
                      ->update([
                        'nmpasien'=> $req->nmpasien,
                        'idjnskelamin'=> $req->jkelamin,
                        'idstkawin'=> $req->idstkawin,
                        'alamat'=> $req->alamat,
                        'idwn'=> $req->idwn,
                        'iddaerah'=> ($req->iddaerah) ? $req->iddaerah:null,
                        'nohp'=> $req->nohp,
                        'tptlahir'=> $req->tptlahir,
                        'tgllahir'=> date_format(date_create($req->tgllahir), 'Y-m-d'),
                        'tgllahirortu'=> date_format(date_create($req->tgllahir_ortu), 'Y-m-d'),
                        'tgllahirpasangan'=> date_format(date_create($req->tgllahir_pasangan), 'Y-m-d'),
                        'nmibu'=> $req->nmortu,
                        'nmpasangan'=> $req->nmpasangan,
                        'idpekerjaan'=> $req->idpekerjaan,
                        'idagama'=> $req->idagama,
                        'noidentitas'=> $req->noidentitas,
                        'idgoldarah'=> $req->idgoldarah,
                        'idpendidikan'=> $req->idpendidikan,
                        'negara'=> $req->negara,
                        'alergi'=> $req->alergi,
                      ]);

                        $message = array();
                        $message['error'] = '';
                        $message['message'] = 'Data berhasil diupdate';
                        $message['list'] = $pasien;
        else:
            $pasien = Pasien::create([
                'norm'=> $norm,
                'nmpasien'=> $req->nmpasien,
                'idjnskelamin'=> $req->jkelamin,
                'idstkawin'=> $req->idstkawin,
                'alamat'=> $req->alamat,
                'idwn'=> $req->idwn,
                'iddaerah'=> ($req->iddaerah) ? $req->iddaerah:null,
                'nohp'=> $req->nohp,
                'tptlahir'=> $req->tptlahir,
                'tgllahir'=> date_format(date_create($req->tgllahir), 'Y-m-d'),
                'tgllahirortu'=> date_format(date_create($req->tgllahir_ortu), 'Y-m-d'),
                'tgllahirpasangan'=> date_format(date_create($req->tgllahir_pasangan), 'Y-m-d'),
                'nmibu'=> $req->nmortu,
                'nmpasangan'=> $req->nmpasangan,
                'idpekerjaan'=> $req->idpekerjaan,
                'idagama'=> $req->idagama,
                'noidentitas'=> $req->noidentitas,
                'idgoldarah'=> $req->idgoldarah,
                'idpendidikan'=> $req->idpendidikan,
                'negara'=> $req->negara,
                'catatan'=> $req->catatan,
                'alergi'=> $req->alergi,
                'tgldaftar'=> date('Y-m-d')
            ]);

            $message = array();
            $message['error'] = '';
            $message['message'] = 'Data berhasil ditambahkan';
            $message['list'] = $pasien;
        endif;



        return response()->json($message);
    }

    public function getnorm(){
        $data = Pasien::selectRaw('max(norm) AS max_np')->first();
		if(is_null($data->max_np)) $max = 1;
		else $max = $data->max_np + 1;
		return $max;
	}

    public function get_data()
    {

        $data = Pasien::select('pasien.*')->with(['jkelamin','get_daerah'])->get();

        return datatables()->of($data)
        ->addColumn('kdjnskelamin', function ($data) {
            return $data->jkelamin->kdjnskelamin;

        })
        ->addColumn('nmdaerah', function ($data) {
            if($data->get_daerah()->exists()) {
                return $data->get_daerah->nmdaerah;
            } else{
                return '';
            }


        })
        ->addIndexColumn()
        ->make(true);
    }
}
