<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\DashboardController::class, 'index']);


Route::middleware('auth')->group(function() {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::prefix('rj')->name('rj.')->group(function() {
        Route::get('registrasi-pasien', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'index'])->name('registrasi-pasien');
        Route::get('detail-registrasi/{id?}', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'detail']);
        Route::get('get-daerah', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_daerah'])->name('get-daerah');
        Route::get('get-riwayat-pasien', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_riwayat_pasien'])->name('get-riwayat-pasien');
        Route::post('store-registrasi', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'insert_registrasi'])->name('store');
        Route::get('transaksi-pasien', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'transaksi'])->name('transaksi-pasien');
        Route::get('get-list-registrasi', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_data'])->name('list-registrasi');
        Route::get('get-list-registrasi-proses', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_registrasi_proses'])->name('list-registrasi-proses');
        Route::get('transaksi-rj/detail/{noreg}', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'detail_transaksi'])->name('detail-transaksi-pasien');
        Route::get('get-tindakan', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_tindakan'])->name('get-tindakan');
        Route::get('get-obat', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_obat'])->name('get-obat');
        Route::post('insert-draft-nota', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'insert_nota_awal'])->name('insert-draft-nota-rj');
        Route::post('insert-tindakan', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'insert_tindakan'])->name('insert-tindakan');
        Route::get('get-tindakan-bynota', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_tindakan_by_nota'])->name('get-tindakan-bynota');
        Route::get('transaksi/hapus-notadet/{id}', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'hapus_notadet']);
        Route::post('insert-obat', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'insert_obat'])->name('insert-obat');
        Route::get('get-obat-bynota', [App\Http\Controllers\Rawatjalan\RegistrasiRjController::class, 'get_obat_by_nota'])->name('get-obat-bynota');
    });

     Route::prefix('ri')->name('ri.')->group(function() {
        Route::get('registrasi-pasien-ri', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'index'])->name('registrasi-pasien-ri');
        Route::get('detail-registrasi/{id?}', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'detail']);
        Route::get('get-daerah', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'get_daerah'])->name('get-daerah');
        Route::get('get-riwayat-pasien-ri', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'get_riwayat_pasien-ri'])->name('get-riwayat-pasien-ri');
        Route::post('store-registrasi_ri', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'insert_registrasi_ri'])->name('store');
         Route::get('input-tindakan-pasien', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'input-tindakan'])->name('input-tindakan-pasien');
        Route::get('get-list-registrasi', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'get_data'])->name('list-registrasi');
        Route::get('get-list-registrasi-proses', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'get_registrasi_proses'])->name('list-registrasi-proses');
        Route::get('input-tindakan-ri/detail/{noreg}', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'detail_input-tindakan'])->name('detail-input-tindakan-pasien');
        Route::get('get-tindakan', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'get_tindakan'])->name('get-tindakan');
        Route::get('get-obat', [App\Http\Controllers\Rawatinap\RegisRtrasiRiController::class, 'get_obat'])->name('get-obat');
        Route::post('insert-draft-nota', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'insert_nota_awal'])->name('insert-draft-nota-ri');
        Route::post('insert-tindakan', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'insert_tindakan'])->name('insert-tindakan');
        Route::get('get-tindakan-bynota', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'get_tindakan_by_nota'])->name('get-tindakan-bynota');
        Route::get('input-tindakan/hapus-notadet/{id}', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'hapus_notadet']);
        Route::post('insert-obat', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'insert_obat'])->name('insert-obat');
        Route::get('get-obat-bynota', [App\Http\Controllers\Rawatinap\RegistrasiRiController::class, 'get_obat_by_nota'])->name('get-obat-bynota');
    });

    Route::prefix('kasir')->name('kasir.')->group(function() {
        Route::get('index', [App\Http\Controllers\Kasir\KasirController::class, 'index'])->name('index');
        Route::get('detail/{noreg}', [App\Http\Controllers\Kasir\KasirController::class, 'detail'])->name('detail');
        Route::get('list-kuitansidet', [App\Http\Controllers\Kasir\KasirController::class, 'get_kuitansi_det'])->name('list-kuitansidet');
        Route::post('insert-bayar', [App\Http\Controllers\Kasir\KasirController::class, 'insert_pembayaran'])->name('insert-bayar');
        Route::get('hapus-kuidet/{id}', [App\Http\Controllers\Kasir\KasirController::class, 'delete_kuidet']);


    });


    Route::prefix('inv')->name('inv.')->group(function() {
        Route::get('po', [App\Http\Controllers\Inventory\PoController::class, 'create_po'])->name('create-po');
        Route::get('list-po', [App\Http\Controllers\Inventory\PoController::class, 'get_po'])->name('po');
        Route::get('get-barang', [App\Http\Controllers\Inventory\PoController::class, 'get_barang'])->name('get-barang');

        Route::get('barang-bagian', [App\Http\Controllers\Inventory\BarangbagianController::class, 'index'])->name('barang-bagian');
        Route::get('get-barang-bagian', [App\Http\Controllers\Inventory\BarangbagianController::class, 'get_barang_bagian'])->name('get-barang-bagian');
        Route::post('barang-bagian/update', [App\Http\Controllers\Inventory\BarangbagianController::class, 'update']);

        Route::get('pp', [App\Http\Controllers\Inventory\PemesananController::class, 'index'])->name('pp');
        Route::get('list-pp', [App\Http\Controllers\Inventory\PemesananController::class, 'get_pp'])->name('list-pp');
        Route::get('list-ppdet', [App\Http\Controllers\Inventory\PemesananController::class, 'get_ppdet'])->name('list-ppdet');
        Route::get('create-pp', [App\Http\Controllers\Inventory\PemesananController::class, 'create'])->name('create-pp');
        Route::post('insert-pp', [App\Http\Controllers\Inventory\PemesananController::class, 'store'])->name('insert-pp');
        Route::get('deletepp/{id}', [App\Http\Controllers\Inventory\PemesananController::class, 'destroy'])->name('deletepp');
        Route::get('detailpp/{id}', [App\Http\Controllers\Inventory\PemesananController::class, 'detail'])->name('detailpp');
        Route::get('editpp/{id}', [App\Http\Controllers\Inventory\PemesananController::class, 'edit'])->name('editpp');
        Route::post('update-pp', [App\Http\Controllers\Inventory\PemesananController::class, 'update'])->name('update-pp');

        Route::get('receive', [App\Http\Controllers\Inventory\ReceiveController::class, 'create'])->name('receive');
        Route::get('receive-getpesanan', [App\Http\Controllers\Inventory\ReceiveController::class, 'get_pesanan'])->name('receive-getpesanan');
     


    });

    Route::prefix('pasien')->name('pasien.')->group(function() {
        Route::post('store', [App\Http\Controllers\Pasien\PasienController::class, 'store'])->name('store');
        Route::get('get-data', [App\Http\Controllers\Pasien\PasienController::class, 'get_data'])->name('get-data');
    });

    Route::prefix('master')->name('master.')->group(function() {
        // bidang perawatan
        Route::get('bidang-perawatan', [App\Http\Controllers\Master\BidangperawatanController::class, 'index'])->name('bidang-perawatan');
        Route::get('bidang-perawatan/get-data', [App\Http\Controllers\Master\BidangperawatanController::class, 'get_data']);
        Route::post('bidang-perawatan/store', [App\Http\Controllers\Master\BidangperawatanController::class, 'store']);
        Route::get('bidang-perawatan/hapus/{id}', [App\Http\Controllers\Master\BidangperawatanController::class, 'destroy']);
        // cara bayar
        Route::get('cara-bayar', [App\Http\Controllers\Master\CarabayarController::class, 'index'])->name('cara-bayar');
        Route::get('cara-bayar/get-data', [App\Http\Controllers\Master\CarabayarController::class, 'get_data']);
        Route::post('cara-bayar/store', [App\Http\Controllers\Master\CarabayarController::class, 'store']);
        Route::get('cara-bayar/hapus/{id}', [App\Http\Controllers\Master\CarabayarController::class, 'destroy']);
        // shift
        Route::get('shift', [App\Http\Controllers\Master\ShiftController::class, 'index'])->name('shift');
        Route::get('shift/get-data', [App\Http\Controllers\Master\ShiftController::class, 'get_data']);
        Route::post('shift/store', [App\Http\Controllers\Master\ShiftController::class, 'store']);
        Route::get('shift/hapus/{id}', [App\Http\Controllers\Master\ShiftController::class, 'destroy']);
        // cara datang
        Route::get('cara-datang', [App\Http\Controllers\Master\CaradatangController::class, 'index'])->name('cara-datang');
        Route::get('cara-datang/get-data', [App\Http\Controllers\Master\CaradatangController::class, 'get_data']);
        Route::post('cara-datang/store', [App\Http\Controllers\Master\CaradatangController::class, 'store']);
        Route::get('cara-datang/hapus/{id}', [App\Http\Controllers\Master\CaradatangController::class, 'destroy']);
        // cara keluar
        Route::get('cara-keluar', [App\Http\Controllers\Master\CarakeluarController::class, 'index'])->name('cara-keluar');
        Route::get('cara-keluar/get-data', [App\Http\Controllers\Master\CarakeluarController::class, 'get_data']);
        Route::post('cara-keluar/store', [App\Http\Controllers\Master\CarakeluarController::class, 'store']);
        Route::get('cara-keluar/hapus/{id}', [App\Http\Controllers\Master\CarakeluarController::class, 'destroy']);
        // golongan darah
        Route::get('golongan-darah', [App\Http\Controllers\Master\GolongandarahController::class, 'index'])->name('golongan-darah');
        Route::get('golongan-darah/get-data', [App\Http\Controllers\Master\GolongandarahController::class, 'get_data']);
        Route::post('golongan-darah/store', [App\Http\Controllers\Master\GolongandarahController::class, 'store']);
        Route::get('golongan-darah/hapus/{id}', [App\Http\Controllers\Master\GolongandarahController::class, 'destroy']);
        // hubungan keluarga
        Route::get('hubungan-keluarga', [App\Http\Controllers\Master\HubungankeluargaController::class, 'index'])->name('hubungan-keluarga');
        Route::get('hubungan-keluarga/get-data', [App\Http\Controllers\Master\HubungankeluargaController::class, 'get_data']);
        Route::post('hubungan-keluarga/store', [App\Http\Controllers\Master\HubungankeluargaController::class, 'store']);
        Route::get('hubungan-keluarga/hapus/{id}', [App\Http\Controllers\Master\HubungankeluargaController::class, 'destroy']);
         // jam praktek
        Route::get('jam-praktek', [App\Http\Controllers\Master\JampraktekController::class, 'index'])->name('jam-praktek');
        Route::get('jam-praktek/get-data', [App\Http\Controllers\Master\JampraktekController::class, 'get_data']);
        Route::post('jam-praktek/store', [App\Http\Controllers\Master\JampraktekController::class, 'store']);
        Route::get('jam-praktek/hapus/{id}', [App\Http\Controllers\Master\JampraktekController::class, 'destroy']);
        // jenis barang
        Route::get('jenis-barang', [App\Http\Controllers\Master\JenisbarangController::class, 'index'])->name('jenis-barang');
        Route::get('jenis-barang/get-data', [App\Http\Controllers\Master\JenisbarangController::class, 'get_data']);
        Route::post('jenis-barang/store', [App\Http\Controllers\Master\JenisbarangController::class, 'store']);
        Route::get('jenis-barang/hapus/{id}', [App\Http\Controllers\Master\JenisbarangController::class, 'destroy']);
        // jenis identitas
        Route::get('jenis-identitas', [App\Http\Controllers\Master\JenisidentitasController::class, 'index'])->name('jenis-identitas');
        Route::get('jenis-identitas/get-data', [App\Http\Controllers\Master\JenisidentitasController::class, 'get_data']);
        Route::post('jenis-identitas/store', [App\Http\Controllers\Master\JenisidentitasController::class, 'store']);
        Route::get('jenis-identitas/hapus/{id}', [App\Http\Controllers\Master\JenisidentitasController::class, 'destroy']);
        // jenis kelamin
        Route::get('jenis-kelamin', [App\Http\Controllers\Master\JeniskelaminController::class, 'index'])->name('jenis-kelamin');
        Route::get('jenis-kelamin/get-data', [App\Http\Controllers\Master\JeniskelaminController::class, 'get_data']);
        Route::post('jenis-kelamin/store', [App\Http\Controllers\Master\JeniskelaminController::class, 'store']);
        Route::get('jenis-kelamin/hapus/{id}', [App\Http\Controllers\Master\JeniskelaminController::class, 'destroy']);
        // jenis pembayaran
        Route::get('jenis-pembayaran', [App\Http\Controllers\Master\JenispembayaranController::class, 'index'])->name('jenis-pembayaran');
        Route::get('jenis-pembayaran/get-data', [App\Http\Controllers\Master\JenispembayaranController::class, 'get_data']);
        Route::post('jenis-pembayaran/store', [App\Http\Controllers\Master\JenispembayaranController::class, 'store']);
        Route::get('jenis-pembayaran/hapus/{id}', [App\Http\Controllers\Master\JenispembayaranController::class, 'destroy']);
        // jenis penjamin
        Route::get('jenis-penjamin', [App\Http\Controllers\Master\JenispenjaminController::class, 'index'])->name('jenis-penjamin');
        Route::get('jenis-penjamin/get-data', [App\Http\Controllers\Master\JenispenjaminController::class, 'get_data']);
        Route::post('jenis-penjamin/store', [App\Http\Controllers\Master\JenispenjaminController::class, 'store']);
        Route::get('jenis-penjamin/hapus/{id}', [App\Http\Controllers\Master\JenispenjaminController::class, 'destroy']);
         // jenis tenaga medis
        Route::get('jenis-tenaga-medis', [App\Http\Controllers\Master\JenistenagamedisController::class, 'index'])->name('jenis-tenaga-medis');
        Route::get('jenis-tenaga-medis/get-data', [App\Http\Controllers\Master\JenistenagamedisController::class, 'get_data']);
        Route::post('jenis-tenaga-medis/store', [App\Http\Controllers\Master\JenistenagamedisController::class, 'store']);
        Route::get('jenis-tenaga-medis/hapus/{id}', [App\Http\Controllers\Master\JenistenagamedisController::class, 'destroy']);
         // kelas tarif
        Route::get('kelas-tarif', [App\Http\Controllers\Master\KelastarifController::class, 'index'])->name('kelas-tarif');
        Route::get('kelas-tarif/get-data', [App\Http\Controllers\Master\KelastarifController::class, 'get_data']);
        Route::post('kelas-tarif/store', [App\Http\Controllers\Master\KelastarifController::class, 'store']);
        Route::get('kelas-tarif/hapus/{id}', [App\Http\Controllers\Master\KelastarifController::class, 'destroy']);
        // kelompok barang
        Route::get('kelompok-barang', [App\Http\Controllers\Master\KelompokbarangController::class, 'index'])->name('kelompok-barang');
        Route::get('kelompok-barang/get-data', [App\Http\Controllers\Master\KelompokbarangController::class, 'get_data']);
        Route::post('kelompok-barang/store', [App\Http\Controllers\Master\KelompokbarangController::class, 'store']);
        Route::get('kelompok-barang/hapus/{id}', [App\Http\Controllers\Master\KelompokbarangController::class, 'destroy']);
        // pekerjaan
        Route::get('pekerjaan', [App\Http\Controllers\Master\PekerjaanController::class, 'index'])->name('pekerjaan');
        Route::get('pekerjaan/get-data', [App\Http\Controllers\Master\PekerjaanController::class, 'get_data']);
        Route::post('pekerjaan/store', [App\Http\Controllers\Master\PekerjaanController::class, 'store']);
        Route::get('pekerjaan/hapus/{id}', [App\Http\Controllers\Master\PekerjaanController::class, 'destroy']);
        // pendidikan
        Route::get('pendidikan', [App\Http\Controllers\Master\PendidikanController::class, 'index'])->name('pendidikan');
        Route::get('pendidikan/get-data', [App\Http\Controllers\Master\PendidikanController::class, 'get_data']);
        Route::post('pendidikan/store', [App\Http\Controllers\Master\PendidikanController::class, 'store']);
        Route::get('pendidikan/hapus/{id}', [App\Http\Controllers\Master\PendidikanController::class, 'destroy']);
        // satuan
        Route::get('satuan', [App\Http\Controllers\Master\SatuanController::class, 'index'])->name('satuan');
        Route::get('satuan/get-data', [App\Http\Controllers\Master\SatuanController::class, 'get_data']);
        Route::post('satuan/store', [App\Http\Controllers\Master\SatuanController::class, 'store']);
        Route::get('satuan/hapus/{id}', [App\Http\Controllers\Master\SatuanController::class, 'destroy']);
         // spesialisasi dokter
        Route::get('spesialisasi-dokter', [App\Http\Controllers\Master\SpesialisasidokterController::class, 'index'])->name('spesialisasi-dokter');
        Route::get('spesialisasi-dokter/get-data', [App\Http\Controllers\Master\SpesialisasidokterController::class, 'get_data']);
        Route::post('spesialisasi-dokter/store', [App\Http\Controllers\Master\SpesialisasidokterController::class, 'store']);
        Route::get('spesialisasi-dokter/hapus/{id}', [App\Http\Controllers\Master\SpesialisasidokterController::class, 'destroy']);
        // status dokter
        Route::get('status-dokter', [App\Http\Controllers\Master\StatusdokterController::class, 'index'])->name('status-dokter');
        Route::get('status-dokter/get-data', [App\Http\Controllers\Master\StatusdokterController::class, 'get_data']);
        Route::post('status-dokter/store', [App\Http\Controllers\Master\StatusdokterController::class, 'store']);
        Route::get('status-dokter/hapus/{id}', [App\Http\Controllers\Master\StatusdokterController::class, 'destroy']);
         // status keluar
        Route::get('status-keluar', [App\Http\Controllers\Master\StatuskeluarController::class, 'index'])->name('status-keluar');
        Route::get('status-keluar/get-data', [App\Http\Controllers\Master\StatuskeluarController::class, 'get_data']);
        Route::post('status-keluar/store', [App\Http\Controllers\Master\StatuskeluarController::class, 'store']);
        Route::get('status-keluar/hapus/{id}', [App\Http\Controllers\Master\StatuskeluarController::class, 'destroy']);
        // status pasien
        Route::get('status-pasien', [App\Http\Controllers\Master\StatuspasienController::class, 'index'])->name('status-pasien');
        Route::get('status-pasien/get-data', [App\Http\Controllers\Master\StatuspasienController::class, 'get_data']);
        Route::post('status-pasien/store', [App\Http\Controllers\Master\StatuspasienController::class, 'store']);
        Route::get('status-pasien/hapus/{id}', [App\Http\Controllers\Master\StatuspasienController::class, 'destroy']);
        // status pelayanan
        Route::get('status-pelayanan', [App\Http\Controllers\Master\StatuspelayananController::class, 'index'])->name('status-pelayanan');
        Route::get('status-pelayanan/get-data', [App\Http\Controllers\Master\StatuspelayananController::class, 'get_data']);
        Route::post('status-pelayanan/store', [App\Http\Controllers\Master\StatuspelayananController::class, 'store']);
        Route::get('status-pelayanan/hapus/{id}', [App\Http\Controllers\Master\StatuspelayananController::class, 'destroy']);
        // status perkawinan
        Route::get('status-perkawinan', [App\Http\Controllers\Master\StatusperkawinanController::class, 'index'])->name('status-perkawinan');
        Route::get('status-perkawinan/get-data', [App\Http\Controllers\Master\StatusperkawinanController::class, 'get_data']);
        Route::post('status-perkawinan/store', [App\Http\Controllers\Master\StatusperkawinanController::class, 'store']);
        Route::get('status-perkawinan/hapus/{id}', [App\Http\Controllers\Master\StatusperkawinanController::class, 'destroy']);
        // status pesanan
        Route::get('status-pesanan', [App\Http\Controllers\Master\StatuspesananController::class, 'index'])->name('status-pesanan');
        Route::get('status-pesanan/get-data', [App\Http\Controllers\Master\StatuspesananController::class, 'get_data']);
        Route::post('status-pesanan/store', [App\Http\Controllers\Master\StatuspesananController::class, 'store']);
        Route::get('status-pesanan/hapus/{id}', [App\Http\Controllers\Master\StatuspesananController::class, 'destroy']);
         // status posisi pasien
        Route::get('status-posisi-pasien', [App\Http\Controllers\Master\StatusposisipasienController::class, 'index'])->name('status-posisi-pasien');
        Route::get('status-posisi-pasien/get-data', [App\Http\Controllers\Master\StatusposisipasienController::class, 'get_data']);
        Route::post('status-posisi-pasien/store', [App\Http\Controllers\Master\StatusposisipasienController::class, 'store']);
        Route::get('status-posisi-pasien/hapus/{id}', [App\Http\Controllers\Master\StatusposisipasienController::class, 'destroy']);
        // status stok opname
        Route::get('status-stok-opname', [App\Http\Controllers\Master\StatusstokopnameController::class, 'index'])->name('status-stok-opname');
        Route::get('status-stok-opname/get-data', [App\Http\Controllers\Master\StatusstokopnameController::class, 'get_data']);
        Route::post('status-stok-opname/store', [App\Http\Controllers\Master\StatusstokopnameController::class, 'store']);
        Route::get('status-stok-opname/hapus/{id}', [App\Http\Controllers\Master\StatusstokopnameController::class, 'destroy']);
        // syarat pembayaran
        Route::get('syarat-pembayaran', [App\Http\Controllers\Master\SyaratpembayaranController::class, 'index'])->name('syarat-pembayaran');
        Route::get('syarat-pembayaran/get-data', [App\Http\Controllers\Master\SyaratpembayaranController::class, 'get_data']);
        Route::post('syarat-pembayaran/store', [App\Http\Controllers\Master\SyaratpembayaranController::class, 'store']);
        Route::get('syarat-pembayaran/hapus/{id}', [App\Http\Controllers\Master\SyaratpembayaranController::class, 'destroy']);

        // bagian
        Route::get('bagian', [App\Http\Controllers\Master\BagianController::class, 'index'])->name('bagian');
        Route::get('bagian/get-data', [App\Http\Controllers\Master\BagianController::class, 'get_data']);
        Route::post('bagian/store', [App\Http\Controllers\Master\BagianController::class, 'store']);
        Route::get('bagian/hapus/{id}', [App\Http\Controllers\Master\BagianController::class, 'destroy']);
        // bank
        Route::get('bank', [App\Http\Controllers\Master\BankController::class, 'index'])->name('bank');
        Route::get('bank/get-data', [App\Http\Controllers\Master\BankController::class, 'get_data']);
        Route::post('bank/store', [App\Http\Controllers\Master\BankController::class, 'store']);
        Route::get('bank/hapus/{id}', [App\Http\Controllers\Master\BankController::class, 'destroy']);
        // barang
        Route::get('barang', [App\Http\Controllers\Master\BarangController::class, 'index'])->name('barang');
        Route::get('barang/get-data', [App\Http\Controllers\Master\BarangController::class, 'get_data']);
        Route::post('barang/store', [App\Http\Controllers\Master\BarangController::class, 'store']);
        Route::get('barang/hapus/{id}', [App\Http\Controllers\Master\BarangController::class, 'destroy']);
        // daerah
        Route::get('daerah', [App\Http\Controllers\Master\DaerahController::class, 'index'])->name('daerah');
        Route::get('daerah/get-data', [App\Http\Controllers\Master\DaerahController::class, 'get_data']);
        Route::post('daerah/store', [App\Http\Controllers\Master\DaerahController::class, 'store']);
        Route::get('daerah/hapus/{id}', [App\Http\Controllers\Master\DaerahController::class, 'destroy']);
        // pabrik
        Route::get('pabrik', [App\Http\Controllers\Master\PabrikController::class, 'index'])->name('pabrik');
        Route::get('pabrik/get-data', [App\Http\Controllers\Master\PabrikController::class, 'get_data']);
        Route::post('pabrik/store', [App\Http\Controllers\Master\PabrikController::class, 'store']);
        Route::get('pabrik/hapus/{id}', [App\Http\Controllers\Master\PabrikController::class, 'destroy']);
        // kamar
        Route::get('kamar', [App\Http\Controllers\Master\KamarController::class, 'index'])->name('kamar');
        Route::get('kamar/get-data', [App\Http\Controllers\Master\KamarController::class, 'get_data']);
        Route::post('kamar/store', [App\Http\Controllers\Master\KamarController::class, 'store']);
        Route::get('kamar/hapus/{id}', [App\Http\Controllers\Master\KamarController::class, 'destroy']);
        // master template obat
        Route::get('master-template-obat', [App\Http\Controllers\Master\MastertemplateobatController::class, 'index'])->name('master-template-obat');
        Route::get('master-template-obat/get-data', [App\Http\Controllers\Master\MastertemplateobatController::class, 'get_data']);
        Route::post('master-template-obat/store', [App\Http\Controllers\Master\MastertemplateobatController::class, 'store']);
        Route::get('master-template-obat/hapus/{id}', [App\Http\Controllers\Master\MastertemplateobatController::class, 'destroy']);
        // master template pelayanan
        Route::get('master-template-pelayanan', [App\Http\Controllers\Master\MastertemplatepelayananController::class, 'index'])->name('master-template-pelayanan');
        Route::get('master-template-pelayanan/get-data', [App\Http\Controllers\Master\MastertemplatepelayananController::class, 'get_data']);
        Route::post('master-template-pelayanan/store', [App\Http\Controllers\Master\MastertemplatepelayananController::class, 'store']);
        Route::get('master-template-pelayanan/hapus/{id}', [App\Http\Controllers\Master\MastertemplatepelayananController::class, 'destroy']);
        // pelayanan
        Route::get('pelayanan', [App\Http\Controllers\Master\PelayananController::class, 'index'])->name('pelayanan');
        Route::get('pelayanan/get-data', [App\Http\Controllers\Master\PelayananController::class, 'get_data']);
        Route::post('pelayanan/store', [App\Http\Controllers\Master\PelayananController::class, 'store']);
        Route::get('pelayanan/hapus/{id}', [App\Http\Controllers\Master\PelayananController::class, 'destroy']);
        // penjamin
        Route::get('penjamin', [App\Http\Controllers\Master\PenjaminController::class, 'index'])->name('penjamin');
        Route::get('penjamin/get-data', [App\Http\Controllers\Master\PenjaminController::class, 'get_data']);
        Route::post('penjamin/store', [App\Http\Controllers\Master\PenjaminController::class, 'store']);
        Route::get('penjamin/hapus/{id}', [App\Http\Controllers\Master\PenjaminController::class, 'destroy']);
        // penyakit
        Route::get('penyakit', [App\Http\Controllers\Master\PenyakitController::class, 'index'])->name('penyakit');
        Route::get('penyakit/get-data', [App\Http\Controllers\Master\PenyakitController::class, 'get_data']);
        Route::post('penyakit/store', [App\Http\Controllers\Master\PenyakitController::class, 'store']);
        Route::get('penyakit/hapus/{id}', [App\Http\Controllers\Master\PenyakitController::class, 'destroy']);
        // supplier
        Route::get('supplier', [App\Http\Controllers\Master\SupplierController::class, 'index'])->name('supplier');
        Route::get('supplier/get-data', [App\Http\Controllers\Master\SupplierController::class, 'get_data']);
        Route::post('supplier/store', [App\Http\Controllers\Master\SupplierController::class, 'store']);
        Route::get('supplier/hapus/{id}', [App\Http\Controllers\Master\SupplierController::class, 'destroy']);
        // dokter
        Route::get('dokter', [App\Http\Controllers\Master\DokterController::class, 'index'])->name('dokter');
        Route::get('dokter/get-data', [App\Http\Controllers\Master\DokterController::class, 'get_data']);
        Route::post('dokter/store', [App\Http\Controllers\Master\DokterController::class, 'store']);
        Route::get('dokter/hapus/{id}', [App\Http\Controllers\Master\DokterController::class, 'destroy']);

        // jadwal praktek
        Route::get('jadwal-praktek', [App\Http\Controllers\Master\JadwalpraktekController::class, 'index'])->name('jadwal-praktek');
        Route::get('jadwal-praktek/get-data', [App\Http\Controllers\Master\JadwalpraktekController::class, 'get_data']);
        Route::post('jadwal-praktek/store', [App\Http\Controllers\Master\JadwalpraktekController::class, 'store']);
        Route::get('jadwal-praktek/hapus/{id}', [App\Http\Controllers\Master\JadwalpraktekController::class, 'destroy']);



    });

    Route::prefix('api')->name('api.')->group(function() {
        Route::get('get-shift', [App\Http\Controllers\Api\ApiController::class, 'get_shift'])->name('get-shift');
    });

    Route::prefix('laporan')->name('laporan.')->group(function() {
        // daftar pasien
        Route::get('daftar-pasien', [App\Http\Controllers\Laporan\DaftarpasienController::class, 'index'])->name('daftar-pasien');
        Route::get('daftar-pasien/get-data', [App\Http\Controllers\Laporan\DaftarpasienController::class, 'get_data']);
        // kunjungan rj per polklinik
        Route::get('kunjungan-pasien-rj-per-poliklinik', [App\Http\Controllers\Laporan\KunjunganpasienrjperpoliklinikController::class, 'index'])->name('kunjungan-pasien-rj-per-poliklinik');
        Route::get('kunjungan-pasien-rj-per-poliklinik/get-data', [App\Http\Controllers\Laporan\KunjunganpasienrjperpoliklinikController::class, 'get_data']);
        // penyakit terbanyak rawat jalan
        Route::get('penyakit-terbanyak-rawat-jalan', [App\Http\Controllers\Laporan\PenyakitterbanyakrawatjalanController::class, 'index'])->name('penyakit-terbanyak-rawat-jalan');
        Route::get('penyakit-terbanyak-rawat-jalan/get-data', [App\Http\Controllers\Laporan\PenyakitterbanyakrawatjalanController::class, 'get_data']);
        // daftar barang
        Route::get('daftar-barang', [App\Http\Controllers\Laporan\DaftarbarangController::class, 'index'])->name('daftar-barang');
        Route::get('daftar-barang/get-data', [App\Http\Controllers\Laporan\DaftarbarangController::class, 'get_data']);
        // daftar supplier
        Route::get('daftar-supplier', [App\Http\Controllers\Laporan\DaftarsupplierController::class, 'index'])->name('daftar-supplier');
        Route::get('daftar-supplier/get-data', [App\Http\Controllers\Laporan\DaftarbarangController::class, 'get_data']);
        // distribusi barang
        Route::get('distribusi-barang', [App\Http\Controllers\Laporan\DistribusibarangController::class, 'index'])->name('distribusi-barang');
        Route::get('distribusi-barang/get-data', [App\Http\Controllers\Laporan\DistribusibarangController::class, 'get_data']);
        // pembuatan po per periode
        Route::get('pembuatan-po-per-periode', [App\Http\Controllers\Laporan\PembuatanpoperperiodeController::class, 'index'])->name('pembuatan-po-per-periode');
        Route::get('pembuatan-po-per-periode/get-data', [App\Http\Controllers\Laporan\PembuatanpoperperiodeController::class, 'get_data']);
        // daftar supplier
        Route::get('obat-dokter', [App\Http\Controllers\Laporan\ObatdokterController::class, 'index'])->name('obat-dokter');
        Route::get('obat-dokter/get-data', [App\Http\Controllers\Laporan\ObatdokterController::class, 'get_data']);
        // penerimaan setoran kasir
        Route::get('penerimaan-setoran-kasir', [App\Http\Controllers\Laporan\PenerimaansetorankasirController::class, 'index'])->name('penerimaan-setoran-kasir');
        Route::get('penerimaan-setoran-kasir/get-data', [App\Http\Controllers\Laporan\PenerimaansetorankasirController::class, 'get_data']);
    });
});



require __DIR__.'/auth.php';
