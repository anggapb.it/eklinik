@extends('layouts.main')

@section('page.title', 'Distribusi Barang')
@section('page.heading', 'Distribusi Barang')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

              <div class="table-responsive">
                <table  id="tbl_distribusibarang" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No. Pengeluaran</th>
                      <th class="align-middle" scope="col">Tgl. Pengeluaran</th>
                      <th class="align-middle" scope="col">Jam</th>
                      <th class="align-middle" scope="col">Bagian Minta</th>
                      <th class="align-middle" scope="col">Penerima</th>
                      <th class="align-middle" scope="col">Status Persetujuan</th>
                      <th class="align-middle" scope="col">Status Transaksi</th>
                      <th class="align-middle" scope="col">Kode Barang</th>
                      <th class="align-middle" scope="col">Nama Barang</th>
                      <th class="align-middle" scope="col">Jenis Barang</th>
                      <th class="align-middle" scope="col">Satuan</th>
                      <th class="align-middle" scope="col">Qty</th>
                    </tr>
                  </thead>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('laporan/distribusi-barang/get-data') }}';

        var table = $('#tbl_distribusibarang').DataTable({
            dom: 'B<br>lfrtip',
            buttons: [
            'print','pdf','excel'
        ],
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
            
              {data: 'nokeluarbrg', name: 'nokeluarbrg'},
              {data: 'tglkeluar', name: 'tglkeluar'},
              {data: 'jamkeluar', name: 'jamkeluar'},
              {data: 'idbagianuntuk', name: 'idbagianuntuk'},
              {data: 'penerima', name: 'penerima'},
              {data: 'idstsetuju', name: 'idstsetuju'},
              {data: 'idsttransaksi', name: 'idsttransaksi'},
              {data: 'kdbrg', name: 'kdbrg'},
              {data: 'nmbrg', name: 'nmbrg'},
              {data: 'idjnsbrg', name: 'idjnsbrg'},
              {data: 'satuan', name: 'satuan'},
              {data: 'qty', name: 'qty'},
              
            ],
            responsive: true,
          });

    }
</script>
@endpush
