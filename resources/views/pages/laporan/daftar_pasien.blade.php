@extends('layouts.main')

@section('page.title', 'Laporan Daftar Pasien')
@section('page.heading', 'Laporan Daftar Pasien')

@section('page.content')
<div class="col-12">
    <div class="card">

          <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

              <div class="table-responsive">
                <table  id="tbl_daftarpasien" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">Tgl. Daftar</th>
                      <th class="align-middle" scope="col">No. RM</th>
                      <th class="align-middle" scope="col">Nama Pasien</th>
                      <th class="align-middle" scope="col">Nama Orangtua/Pasangan</th>
                      <th class="align-middle" scope="col">Jenis Kelamin</th>
                      <th class="align-middle" scope="col">Status Kawin</th>
                      <th class="align-middle" scope="col">Tempat lahir</th>
                      <th class="align-middle" scope="col">Tanggal lahir</th>
                      <th class="align-middle" scope="col">Alamat</th>
                      <th class="align-middle" scope="col">Kode Pos</th>
                      <th class="align-middle" scope="col">Kelurahan</th>
                      <th class="align-middle" scope="col">Kecamatan</th>
                      <th class="align-middle" scope="col">Kota/Kabupaten</th>
                      <th class="align-middle" scope="col">Provinsi</th>
                      <th class="align-middle" scope="col">No. Handphone</th>
                      <th class="align-middle" scope="col">No. Telepon</th>
                      <th class="align-middle" scope="col">Catatan</th>
                      <th class="align-middle" scope="col">Agama</th>
                      <th class="align-middle" scope="col">Gol. Darah</th>
                      <th class="align-middle" scope="col">Pendidikan</th>
                      <th class="align-middle" scope="col">Pekerjaan</th>
                      <th class="align-middle" scope="col">Suku Bangsa</th>
                      <th class="align-middle" scope="col">No. KTP/Passpor</th>
                      <th class="align-middle" scope="col">Kebangsaan</th>
                      <th class="align-middle" scope="col">Negara</th>
                      <th class="align-middle" scope="col">Alergi Obat</th>                    
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('laporan/daftar-pasien/get-data') }}';

        var table = $('#tbl_daftarpasien').DataTable({
            dom: 'B<br>lfrtip',
            buttons: [
            'print','pdf','excel'
        ],
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
            
              {data: 'tgldaftar', name: 'tgldaftar'},
              {data: 'norm', name: 'norm'},
              {data: 'nmpasien', name: 'nmpasien'},
              {data: 'nmpasangan', name: 'nmpasangan'},
              {data: 'nmjnskelamin', name: 'nmjnskelamin'},
              {data: 'nmstkawin', name: 'nmstkawin'},
              {data: 'tptlahir', name: 'tptlahir'},
              {data: 'tgllahir', name: 'tgllahir'},
              {data: 'alamat', name: 'noalamatrm'},
              {data: 'idlokasi', name: 'idlokasi'},
              {data: 'iddaerah', name: 'iddaerah'},
              {data: 'iddaerah', name: 'iddaerah'},
              {data: 'iddaerah', name: 'iddaerah'},
              {data: 'iddaerah', name: 'iddaerah'},
              {data: 'nohp', name: 'nohp'},
              {data: 'notelp', name: 'notelp'},
              {data: 'catatan', name: 'catatan'},
              {data: 'idagama', name: 'idagama'},
              {data: 'idgoldarah', name: 'idgoldarah'},
              {data: 'idpendidikan', name: 'idpendidikan'},
              {data: 'idpekerjaan', name: 'idpekerjaan'},
              {data: 'idsukubangsa', name: 'idsukubangsa'},
              {data: 'noidentitas', name: 'noidentitas'},
              {data: 'idsukubangsa', name: 'idsukubangsa'},
              {data: 'negara', name: 'negara'},
              {data: 'alergi', name: 'alergi'},

            ],
            responsive: true,
          });

    }
</script>
@endpush
