@extends('layouts.main')

@section('page.title', 'Dashboard')
@section('page.heading', 'Dashboard')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

              <div class="table-responsive">
                <table  id="tbl_daftarpasien" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No. Pembelian</th>
                      <th class="align-middle" scope="col">Tgl. Pembelian</th>
                      <th class="align-middle" scope="col">Nama Barang</th>
                      <th class="align-middle" scope="col">Satuan</th>
                      <th class="align-middle" scope="col">Qty</th>
                      <th class="align-middle" scope="col">Qty Bonus</th>
                      <th class="align-middle" scope="col">Harga Beli</th>
                      <th class="align-middle" scope="col">Supplier</th>
                    </tr>
                  </thead>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">

</script>
@endpush
