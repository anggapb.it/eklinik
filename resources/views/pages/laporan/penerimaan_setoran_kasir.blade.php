@extends('layouts.main')

@section('page.title', 'Dashboard')
@section('page.heading', 'Dashboard')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

              <div class="table-responsive">
                <table  id="tbl_daftarpasien" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No. Reg</th>
                      <th class="align-middle" scope="col">Tgl. Reg</th>
                      <th class="align-middle" scope="col">No. RM</th>
                      <th class="align-middle" scope="col">Nama Pasien</th>
                      <th class="align-middle" scope="col">Diskon</th>
                      <th class="align-middle" scope="col">Tunai</th>
                      <th class="align-middle" scope="col">Kartu Debit</th>
                      <th class="align-middle" scope="col">Kartu Kredit</th>
                      <th class="align-middle" scope="col">Total</th>
                      <th class="align-middle" scope="col">Shift</th>
                      <th class="align-middle" scope="col">Poli</th>
                      <th class="align-middle" scope="col">Dokter</th>
                      <th class="align-middle" scope="col">User Input</th>
                    </tr>
                  </thead>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">

</script>
@endpush
