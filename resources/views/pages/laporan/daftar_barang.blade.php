@extends('layouts.main')

@section('page.title', 'Daftar Barang')
@section('page.heading', 'Daftar Barang')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

              <div class="table-responsive">
                <table  id="tbl_daftarbarang" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">Kode Barang</th>
                      <th class="align-middle" scope="col">Nama Barang</th>
                      <th class="align-middle" scope="col">Jenis Barang</th>
                      <th class="align-middle" scope="col">Satuan Besar</th>
                      <th class="align-middle" scope="col">Satuan Kecil</th>
                      <th class="align-middle" scope="col">Harga Jual</th>
                      <th class="align-middle" scope="col">Stok Sekarang </th>
                      <th class="align-middle" scope="col">Stok Minimal</th>
                      <th class="align-middle" scope="col">Stok Maksimal</th>
                    </tr>
                  </thead>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('laporan/daftar-barang/get-data') }}';

        var table = $('#tbl_daftarbarang').DataTable({
            dom: 'B<br>lfrtip',
            buttons: [
            'print','pdf','excel'
        ],
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
            
              {data: 'kdbrg', name: 'kdbrg'},
              {data: 'nmbrg', name: 'nmbrg'},
              {data: 'nmjnsbrg', name: 'nmjnsbrg'},
              {data: 'nmsatuanbsr', name: 'nmsatuanbsr'},
              {data: 'nmsatuankcl', name: 'nmsatuankcl'},
              {data: 'hrgjual', name: 'hrgjual'},
              {data: 'barangbagian.stoknowbagian', name: 'barangbagian.stoknowbagian'},
              {data: 'stokmin', name: 'stokmin'},
              {data: 'stokmax', name: 'stokmax'},
              
            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
        'pdf'
    ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }
</script>
@endpush
