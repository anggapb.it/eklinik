@extends('layouts.main')

@section('page.title', 'Daftar Supplier')
@section('page.heading', 'Daftar Supplier')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

              <div class="table-responsive">
                <table  id="tbl_daftarsupplier" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Tanggal Daftar</th>
                      <th class="align-middle" scope="col">Nama</th>
                      <th class="align-middle" scope="col">Alamat</th>
                      <th class="align-middle" scope="col">No. Telepon</th>
                      <th class="align-middle" scope="col">No. Fax</th>
                      <th class="align-middle" scope="col">Email</th>
                      <th class="align-middle" scope="col">Web Site</th>
                      <th class="align-middle" scope="col">Contact Person</th>
                      <th class="align-middle" scope="col">No. HP</th>
                      <th class="align-middle" scope="col">NpWp</th>
                      <th class="align-middle" scope="col">Bank</th>
                      <th class="align-middle" scope="col">No. Rek</th>
                      <th class="align-middle" scope="col">Atas Nama</th>
                      <th class="align-middle" scope="col">Keterangan</th>
                      <th class="align-middle" scope="col">Status</th>
                    </tr>
                  </thead>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/supplier/get-data') }}';

        var table = $('#tbl_daftarsupplier').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
            
              {data: 'kdsupplier', name: 'kdsupplier'},
              {data: 'tgldaftar', name: 'tgldaftar'},
              {data: 'nmsupplier', name: 'nmsupplier'},
              {data: 'alamat', name: 'alamat'},
              {data: 'notelp', name: 'notelp'},
              {data: 'nofax', name: 'nofax'},
              {data: 'email', name: 'email'},
              {data: 'website', name: 'website'},
              {data: 'kontakperson', name: 'kontakperson'},
              {data: 'nohp', name: 'nohp'},
              {data: 'npwp', name: 'npwp'},
              {data: 'nmbank', name: 'nmbank'},
              {data: 'norek', name: 'norek'},
              {data: 'atasnama', name: 'atasnama'},
              {data: 'keterangan', name: 'keterangan'},
              {data: 'nmstatus', name: 'nmstatus'},
           
              
            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
        'pdf'
    ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }
</script>
@endpush
