@extends('layouts.main')

@section('page.title', 'kunjungan pasien per poliklinik')
@section('page.heading', 'kunjungan pasien per poliklinik')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

              <div class="table-responsive">
                <table  id="tbl_registrasi" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">Tgl. Registrasi</th>
                      <th class="align-middle" scope="col">No. Reg</th>
                      <th class="align-middle" scope="col">No. RM</th>
                      <th class="align-middle" scope="col">Nama Pasien</th>
                      <th class="align-middle" scope="col">Bagian</th>
                      <th class="align-middle" scope="col">Alamat</th>
                      <th class="align-middle" scope="col">Penanggung Biaya</th>
                      <th class="align-middle" scope="col">Dokter Rawat</th>
           
                    </tr>
                  </thead>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('laporan/kunjungan-pasien-rj-per-poliklinik/get-data') }}';

        var table = $('#tbl_registrasi').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [    
              {data: 'tgljaminput', name: 'tgljaminput'},
              {data: 'noreg', name: 'noreg'},
              {data: 'norm', name: 'norm'},
              {data: 'get_pasien.nmpasien', name: 'get_pasien.nmpasien'},
              {data: 'get_reg_det.get_bagian.nmbagian', name: 'get_reg_det.get_bagian.nmbagian'},
              {data: 'get_pasien.alamat', name: 'get_pasien.alamat'},
              {data: 'get_penjamin.nmpenjamin', name: 'get_penjamin.nmpenjamin'},
              {data: 'get_reg_det.get_dokter.nmdokter', name: 'get_dokter.nmdokter'},
            
            ],
            responsive: true,
            pageLength: 10,
            buttons: [
              'pdf'
          ],
            
        });

    }
</script>
@endpush
