@extends('layouts.main')

@section('page.title', 'Dokter')
@section('page.heading', 'Dokter')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_dokter">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_dokter" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Tenaga Medis</th>
                      <th class="align-middle" scope="col">Nama Tenaga Medis Tanpa Gelar</th>
                      <th class="align-middle" scope="col">Nama Tenaga Medis Dengan Gelar</th>
                      <th class="align-middle" scope="col">Jenis Kelamin</th>
                      <th class="align-middle" scope="col">Tempat Lahir</th>
                      <th class="align-middle" scope="col">Tanggal Lahir</th>
                      <th class="align-middle" scope="col">Alamat</th>
                      <th class="align-middle" scope="col">No. telp</th>
                      <th class="align-middle" scope="col">No. HP</th>
                      <th class="align-middle" scope="col">Spesialisasi</th>
                      <th class="align-middle" scope="col">Status</th> 
                      <th class="align-middle" scope="col">Status Dokter</th>
                      <th class="align-middle" scope="col">Catatan</th>
                      <th class="align-middle" scope="col">Keterangan</th>                     
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_dokter" tabindex="-1" aria-labelledby="modal_label_dokter" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_dokter"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/dokter/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="iddokter" name="iddokter" required>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kddokter" name="kddokter" required>
              </div>
             </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tenaga Medis</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjnstenagamedis" id="idjnstenagamedis">
                        <option value="">Pilih</option>
                            @foreach ($jtenagamedis as $row)
                                <option value="{{ $row->idjnstenagamedis }}">{{ $row->nmjnstenagamedis }}</option>
                            @endforeach
                      </select>
              </div>
            </div> 
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Tenaga Medis Tanpa Gelar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmdokter" name="nmdokter" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Tenaga Medis Dengan Gelar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmdoktergelar" name="nmdoktergelar" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjnskelamin" id="idjnskelamin">
                        <option value="">Pilih</option>
                            @foreach ($jkelamin as $row)
                                <option value="{{ $row->idjnskelamin }}">{{ $row->nmjnskelamin }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tempat Lahir</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tptlahir" name="tptlahir" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tgllahir" name="tgllahir" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Alamat</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="alamat" name="alamat" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. Telp</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="notelp" name="notelp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. HP</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nohp" name="nohp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Spesialisasi</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idspesialisasi" id="idspesialisasi">
                        <option value="">Pilih</option>
                            @foreach ($spesialisasi as $row)
                                <option value="{{ $row->idspesialisasi }}">{{ $row->nmspesialisasi }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idstatus" id="idstatus">
                        <option value="">Pilih</option>
                            @foreach ($status as $row)
                                <option value="{{ $row->idstatus }}">{{ $row->nmstatus }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status Dokter</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idstdokter" id="idstdokter">
                        <option value="">Pilih</option>
                            @foreach ($stdokter as $row)
                                <option value="{{ $row->idstdokter }}">{{ $row->nmstdokter }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tenaga Medis Bagian</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idbagian" id="idbagian">
                        <option value="">Pilih</option>
                            @foreach ($bagian as $row)
                                <option value="{{ $row->idbagian }}">{{ $row->nmbagian }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Catatan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="catatan" name="catatan" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Keterangan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="keterangan" name="keterangan" required>
              </div>
            </div>

        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
        $('#tgllahir').datepicker({
            format: 'dd-mm-yyyy',
            //startDate: '1d',
            autoclose: true
        });
        $('#tgllahir').datepicker('update', new Date());
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/dokter/get-data') }}';

        var table = $('#tbl_dokter').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kddokter', name: 'kddokter'},
              {data: 'nmjnstenagamedis', name: 'nmjnstenagamedis'},
              {data: 'nmdokter', name: 'nmdokter'},
              {data: 'nmdoktergelar', name: 'nmdoktergelar'},
              {data: 'nmjnskelamin', name: 'nmjnskelamin'},
              {data: 'tptlahir', name: 'tptlahir'},
              {data: 'tgllahir', name: 'tgllahir'},
              {data: 'alamat', name: 'alamat'},
              {data: 'notelp', name: 'notelp'},
              {data: 'nohp', name: 'nohp'},
              {data: 'nmspesialisasi', name: 'nmspesialisasi'},
              {data: 'nmstatus', name: 'nmstatus'},
              {data: 'nmstdokter', name: 'nmstdokter'},
              {data: 'catatan', name: 'catatan'},
              {data: 'keterangan', name: 'keterangan'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_dokter"\
                                  data-iddokter="'+row.iddokter+'" \
                                  data-kddokter="'+row.kddokter+'" \
                                  data-idjnstenagamedis="'+row.idjnstenagamedis+'" \
                                  data-nmdokter="'+row.nmdokter+'" \
                                  data-nmdoktergelar="'+row.nmdoktergelar+'" \
                                  data-idjnskelamin="'+row.idjnskelamin+'" \
                                  data-tptlahir="'+row.tptlahir+'" \
                                  data-tgllahir="'+row.tgllahir+'" \
                                  data-alamat="'+row.alamat+'" \
                                  data-notelp="'+row.notelp+'" \
                                  data-nohp="'+row.nohp+'" \
                                  data-idspesialisasi="'+row.idspesialisasi+'" \
                                  data-idstatus="'+row.idstatus+'" \
                                  data-idstdokter="'+row.idstdokter+'" \
                                  data-catatan="'+row.catatan+'" \
                                  data-keterangan="'+row.keterangan+'" \
                                  onclick="edit_dokter(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/dokter/hapus').'/' }}'+row.iddokter+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_dokter(e) {
      $("#iddokter").val($(e).data('iddokter'));
      $("#kddokter").val($(e).data('kddokter'));
      $("#idjnstenagamedis").val($(e).data('idjnstenagamedis'));
      $("#nmdokter").val($(e).data('nmdokter'));
      $("#nmdoktergelar").val($(e).data('nmdoktergelar'));
      $("#idjnskelamin").val($(e).data('idjnskelamin'));
      $("#tptlahir").val($(e).data('tptlahir'));
      $("#tgllahir").val($(e).data('tgllahir'));
      $("#alamat").val($(e).data('alamat'));
      $("#notelp").val($(e).data('notelp'));
      $("#nohp").val($(e).data('nohp'));
      $("#idspesialisasi").val($(e).data('idspesialisasi'));
      $("#idstatus").val($(e).data('idstatus'));
      $("#idstdokter").val($(e).data('idstdokter'));
      $("#catatan").val($(e).data('catatan'));
      $("#keterangan").val($(e).data('keterangan'));


    }


  </script>
@endpush
