@extends('layouts.main')

@section('page.title', 'Satuan')
@section('page.heading', 'Satuan')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_satuan">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_satuan" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_satuan" tabindex="-1" aria-labelledby="modal_label_satuan" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_satuan"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/satuan/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idsatuan" name="idsatuan" required>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdsatuan" name="kdsatuan" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmsatuan" name="nmsatuan" required>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/satuan/get-data') }}';

        var table = $('#tbl_satuan').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdsatuan', name: 'kdsatuan'},
              {data: 'nmsatuan', name: 'nmsatuan'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_satuan"\
                                  data-idsatuan="'+row.idsatuan+'" \
                                  data-kdsatuan="'+row.kdsatuan+'" \
                                  data-nmsatuan="'+row.nmsatuan+'" \
                                  onclick="edit_satuan(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/satuan/hapus').'/' }}'+row.idsatuan+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_satuan(e) {
      $("#idsatuan").val($(e).data('idsatuan'));
      $("#kdsatuan").val($(e).data('kdsatuan'));
      $("#nmsatuan").val($(e).data('nmsatuan'));

    }


  </script>
@endpush
