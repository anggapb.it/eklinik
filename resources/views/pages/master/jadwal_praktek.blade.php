@extends('layouts.main')

@section('page.title', 'Jadwal Praktek')
@section('page.heading', 'Jadwal Praktek')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_jadwalpraktek">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_jadwalpraktek" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Bagian</th>
                      <th class="align-middle" scope="col">Dokter</th>
                      <th class="align-middle" scope="col">Hari</th>
                      <th class="align-middle" scope="col">Shift</th>
                      <th class="align-middle" scope="col">Jam Praktek</th>
                      <th class="align-middle" scope="col">Keterangan</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_jadwalpraktek" tabindex="-1" aria-labelledby="modal_label_jadwalpraktek" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_jadwalpraktek"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/jadwal-praktek/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idjadwalpraktek" name="idjadwalpraktek" required>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Bagian</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idbagian" id="idbagian">
                        <option value="">Pilih</option>
                            @foreach ($bagian as $row)
                                <option value="{{ $row->idbagian }}">{{ $row->nmbagian }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Dokter</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="iddokter" id="iddokter">
                        <option value="">Pilih</option>
                            @foreach ($dokter as $row)
                                <option value="{{ $row->iddokter }}">{{ $row->nmdoktergelar }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Hari</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idhari" id="idhari">
                        <option value="">Pilih</option>
                            @foreach ($hari as $row)
                                <option value="{{ $row->idhari }}">{{ $row->nmhari }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Shift</label>
              <div class="col-sm-8">
                  <select class="form-control form-control-sm" name="idshift" id="idshift">
                        <option value="">Pilih</option>
                            @foreach ($shift as $row)
                                <option value="{{ $row->idshift }}">{{ $row->nmshift }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jam Praktek</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjampraktek" id="idjampraktek">
                        <option value="">Pilih</option>
                            @foreach ($jampraktek as $row)
                                <option value="{{ $row->idjampraktek }}">{{ $row->nmjampraktek }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Keterangan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="keterangan" name="keterangan" required>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/jadwal-praktek/get-data') }}';

        var table = $('#tbl_jadwalpraktek').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'nmbagian', name: 'nmbagian'},
              {data: 'nmdokter', name: 'nmdokter'},
              {data: 'nmhari', name: 'nmhari'},
              {data: 'nmshift', name: 'nmshift'},
              {data: 'jampraktek', name: 'jampraktek'},
              {data: 'keterangan', name: 'keterangan'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_jadwalpraktek"\
                                  data-idjadwalpraktek="'+row.idjadwalpraktek+'" \
                                  data-idbagian="'+row.idbagian+'" \
                                  data-iddokter="'+row.iddokter+'" \
                                  data-idhari="'+row.idhari+'" \
                                  data-idshift="'+row.idshift+'" \
                                  data-jampraktek="'+row.jampraktek+'" \
                                  data-keterangan="'+row.keterangan+'" \
                                  onclick="edit_jadwalpraktek(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/jadwal-praktek/hapus').'/' }}'+row.idjadwalpraktek+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_jadwalpraktek(e) {
      $("#idjadwalpraktek").val($(e).data('idjadwalpraktek'));
      $("#idbagian").val($(e).data('idbagian'));
      $("#iddokter").val($(e).data('iddokter'));
      $("#idhari").val($(e).data('idhari'));
      $("#idshift").val($(e).data('idshift'));
      $("#jampraktek").val($(e).data('jampraktek'));
      $("#keterangan").val($(e).data('keterangan'));





    }


  </script>
@endpush
