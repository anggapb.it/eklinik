@extends('layouts.main')

@section('page.title', 'Bagian')
@section('page.heading', 'Bagian')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_bagian">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_bagian" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama Bagian</th>
                      <th class="align-middle" scope="col">Alias</th>
                      <th class="align-middle" scope="col">Level Bagian</th>
                      <th class="align-middle" scope="col">Jenis Hirarki</th>
                      <th class="align-middle" scope="col">Jenis Pelayanan</th>
                      <th class="align-middle" scope="col">Bidang Perawatan</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_bagian" tabindex="-1" aria-labelledby="modal_label_bagian" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_bagian"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/bagian/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idbagian" name="idbagian" required>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdbagian" name="kdbagian" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Bagian</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmbagian" name="nmbagian" required>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Alias</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="alias" name="alias" required>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Level Bagian</label>
              <div class="col-sm-8">
                  <select class="form-control form-control-sm" name="idlvlbagian" id="idlvlbagian">
                        <option value="">Pilih</option>
                            @foreach ($lvlbagian as $row)
                                <option value="{{ $row->idlvlbagian }}">{{ $row->nmlvlbagian }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Hirarki</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjnshirarki" id="idjnshirarki">
                        <option value="">Pilih</option>
                            @foreach ($jhirarki as $row)
                                <option value="{{ $row->idjnshirarki }}">{{ $row->nmjnshirarki }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Pelayanan</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjnspelayanan" id="idjnspelayanan">
                        <option value="">Pilih</option>
                            @foreach ($jpelayanan as $row)
                                <option value="{{ $row->idjnspelayanan }}">{{ $row->nmjnspelayanan }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Bidang Perawatan</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idbdgrawat" id="idbdgrawat">
                        <option value="">Pilih</option>
                            @foreach ($bdgrawat as $row)
                                <option value="{{ $row->idbdgrawat }}">{{ $row->nmbdgrawat }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/bagian/get-data') }}';

        var table = $('#tbl_bagian').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdbagian', name: 'kdbagian'},
              {data: 'nmbagian', name: 'nmbagian'},
              {data: 'alias', name: 'alias'},
              {data: 'nmlvlbagian', name: 'nmlvlbagian'},
              {data: 'nmjnshirarki', name: 'nmjnshirarki'},
              {data: 'nmjnspelayanan', name: 'nmjnspelayanan'},
              {data: 'nmbdgrawat', name: 'nmbdgrawat'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_bagian"\
                                  data-idbagian="'+row.idbagian+'" \
                                  data-kdbagian="'+row.kdbagian+'" \
                                  data-nmbagian="'+row.nmbagian+'" \
                                  data-alias="'+row.alias+'" \
                                  data-idlvlbagian="'+row.idlvlbagian+'" \
                                  data-idjnshirarki="'+idjnspelayanan+'" \
                                  data-idbdgrawat="'+idbdgrawat+'" \
                                  onclick="edit_bagian(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/bagian/hapus').'/' }}'+row.idbagian+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_bagian(e) {
      $("#idbagian").val($(e).data('idbagian'));
      $("#kdbagian").val($(e).data('kdbagian'));
      $("#nmbagian").val($(e).data('nmbagian'));
      $("#alias").val($(e).data('alias'));
      $("#idlvlbagian").val($(e).data('idlvlbagian'));
      $("#idjnspelayanan").val($(e).data('idjnspelayanan'));
      $("#idbdgrawat").val($(e).data('idbdgrawat'));





    }


  </script>
@endpush
