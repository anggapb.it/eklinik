@extends('layouts.main')

@section('page.title', 'Pelayanan')
@section('page.heading', 'Pelayanan')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_pelayanan">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_pelayanan" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama pelayanan</th>
                      <th class="align-middle" scope="col">Jenis pelayanan</th>
                      <th class="align-middle" scope="col">Jenis Hirarki</th>
                      <th class="align-middle" scope="col">Status</th>
                      <th class="align-middle" scope="col">Parent</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_pelayanan" tabindex="-1" aria-labelledby="modal_label_pelayanan" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_pelayanan"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/pelayanan/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No Urut</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nourut" name="nourut" required>
              </div>
             </div> 
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdpelayanan" name="kdpelayanan" required>
              </div>
             </div> 
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama pelayanan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmpelayanan" name="nmpelayanan" required>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis pelayanan</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idjnspelayanan" id="idjnspelayanan">
                        <option value="">Pilih</option>
                            @foreach ($jpelayanan as $row)
                                <option value="{{ $row->idjnspelayanan }}">{{ $row->nmjnspelayanan }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Hirarki</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idjnshirarki" id="idjnshirarki">
                        <option value="">Pilih</option>
                            @foreach ($jhirarki as $row)
                                <option value="{{ $row->idjnshirarki }}">{{ $row->nmjnshirarki }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idstatus" id="idstatus">
                        <option value="">Pilih</option>
                            @foreach ($status as $row)
                                <option value="{{ $row->idstatus }}">{{ $row->nmstatus }}</option>
                            @endforeach
                      </select>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Parent</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="pel_kdpelayanan" id="pel_kdpelayanan">
                        <option value="">Pilih</option>
                            @foreach ($pelayanan as $row)
                                <option value="{{ $row->pel_kdpelayanan }}">{{ $row->pel_kdpelayanan }}</option>
                            @endforeach
                      </select>
              </div>
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/pelayanan/get-data') }}';

        var table = $('#tbl_pelayanan').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdpelayanan', name: 'kdpelayanan'},
              {data: 'nmpelayanan', name: 'nmpelayanan'},
              {data: 'nmjnspelayanan', name: 'nmjnspelayanan'},
              {data: 'nmjnshirarki', name: 'nmjnshirarki'},
              {data: 'nmstatus', name: 'nmstatus'},
              {data: 'pel_kdpelayanan', name: 'pel_kdpelayanan'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_pelayanan"\
                                  data-nourut="'+row.nourut+'" \
                                  data-kdpelayanan="'+row.kdpelayanan+'" \
                                  data-nmpelayanan="'+row.nmpelayanan+'" \
                                  data-idjnspelayanan="'+row.idjnspelayanan+'" \
                                  data-idjnshirarki="'+row.idjnshirarki+'" \
                                  data-idstatus="'+row.idstatus+'" \
                                  data-pel_kdpelayanan="'+row.pel_kdpelayanan+'" \
                                  onclick="edit_pelayanan(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/pelayanan/hapus').'/' }}'+row.kdpelayanan+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_pelayanan(e) {
      $("#kdpelayanan").attr("disable",true);
      $("#nourut").val($(e).data('nourut'));
      $("#kdpelayanan").val($(e).data('kdpelayanan'));
      $("#nmpelayanan").val($(e).data('nmpelayanan'));
      $("#idjnspelayanan").val($(e).data('idjnspelayanan'));
      $("#idjnshirarki").val($(e).data('idjnshirarki'));
      $("#idstatus").val($(e).data('idstatus'));
      $("#pel_kdpelayanan").val($(e).data('pel_kdpelayanan'));


    }


  </script>
@endpush
