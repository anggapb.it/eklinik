@extends('layouts.main')

@section('page.title', 'Barang')
@section('page.heading', 'Barang')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_barang">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_barang" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama Barang</th>
                      <th class="align-middle" scope="col">Kelompok Barang</th>
                      <th class="align-middle" scope="col">Jenis Barang</th>
                      <th class="align-middle" scope="col">Satuan Kecil</th>
                      <th class="align-middle" scope="col">Satuan Besar</th>
                      <th class="align-middle" scope="col">Jumlah Per Box</th>
                      <th class="align-middle" scope="col">Harga Beli</th>
                      <th class="align-middle" scope="col">Harga Jual</th>
                      <th class="align-middle" scope="col">Nama Pabrik</th>
                      <th class="align-middle" scope="col">Nama Supplier</th>
                      <th class="align-middle" scope="col">Status</th>
                      <th class="align-middle" scope="col">Keterangan</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_barang" tabindex="-1" aria-labelledby="modal_label_barang" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_barang"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/barang/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdbrg" name="kdbrg" required>
              </div>
             </div> 
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Barang</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmbrg" name="nmbrg" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kelompok Barang</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idklpbrg" id="idklpbrg">
                        <option value="">Pilih</option>
                            @foreach ($klpbarang as $row)
                                <option value="{{ $row->idklpbrg }}">{{ $row->nmklpbarang }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Barang</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idjnsbrg" id="idjnsbrg">
                        <option value="">Pilih</option>
                            @foreach ($jbarang as $row)
                                <option value="{{ $row->idjnsbrg }}">{{ $row->nmjnsbrg }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Satuan Kecil</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idsatuankcl" id="idsatuankcl">
                        <option value="">Pilih</option>
                            @foreach ($jsatuan as $row)
                                <option value="{{ $row->idsatuan }}">{{ $row->nmsatuan }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Satuan Besar</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idsatuanbsr" id="idsatuanbsr">
                        <option value="">Pilih</option>
                            @foreach ($jsatuan as $row)
                                <option value="{{ $row->idsatuan }}">{{ $row->nmsatuan }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jumlah Per box</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="jmlperbox" name="jmlperbox" required>
              </div>
            </div>
              <div class="form-group row">
              <label class="col-sm-3 col-form-label">Harga Beli</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="hrgbeli" name="hrgbeli" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Harga Jual</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="hrgjual" name="hrgjual" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Pabrik</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idpabrik" id="idpabrik">
                        <option value="">Pilih</option>
                            @foreach ($pabrik as $row)
                                <option value="{{ $row->idpabrik }}">{{ $row->nmpabrik }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Supplier</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="kdsupplier" id="kdsupplier">
                        <option value="">Pilih</option>
                            @foreach ($supplier as $row)
                                <option value="{{ $row->kdsupplier }}">{{ $row->nmsupplier }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idstatus" id="idstatus">
                        <option value="">Pilih</option>
                            @foreach ($status as $row)
                                <option value="{{ $row->idstatus }}">{{ $row->nmstatus }}</option>
                            @endforeach
                      </select>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Keterangan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="keterangan" name="keterangan" required>
              </div>
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/barang/get-data') }}';

        var table = $('#tbl_barang').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdbrg', name: 'kdbrg'},
              {data: 'nmbrg', name: 'nmbrg'},
              {data: 'nmklpbrg', name: 'nmklpbrg'},
              {data: 'nmjnsbrg', name: 'nmjnsbrg'},
              {data: 'nmsatuankecil', name: 'nmsatuankecil'},
              {data: 'nmsatuanbesar', name: 'nmsatuanbesar'},
              {data: 'jmlperbox', name: 'jmlperbox'},
              {data: 'hrgbeli', name: 'hrgbeli'},
              {data: 'hrgjual', name: 'hrgjual'},
              {data: 'nmpabrik', name: 'nmpabrik'},
              {data: 'nmsupplier', name: 'nmsupplier'},
              {data: 'nmstatus', name: 'nmstatus'},
              {data: 'keterangan', name: 'keterangan'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_barang"\
                                  data-kdbrg="'+row.kdbrg+'" \
                                  data-nmbrg="'+row.nmbrg+'" \
                                  data-idklpbrg="'+row.idklpbrg+'" \
                                  data-idjnsbrg="'+row.idjnsbrg+'" \
                                  data-idsatuankcl="'+row.idsatuankcl+'" \
                                  data-idsatuanbsr="'+row.idsatuanbsr+'" \
                                  data-jmlperbox="'+row.jmlperbox+'" \
                                  data-hrgbeli="'+row.hrgbeli+'" \
                                  data-hrgjual="'+row.hrgjual+'" \
                                  data-idpabrik="'+row.idpabrik+'" \
                                  data-kdsupplier="'+row.kdsupplier+'" \
                                  data-idstatus="'+row.idstatus+'" \
                                  data-keterangan="'+row.keterangan+'" \
                                  onclick="edit_barang(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/barang/hapus').'/' }}'+row.kdbrg+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_barang(e) {
      $("#kdbrg").val($(e).data('kdbrg'));
      $("#nmbrg").val($(e).data('nmbrg'));
      $("#idklpbrg").val($(e).data('idklpbrg'));
      $("#idjnsbrg").val($(e).data('idjnsbrg'));
      $("#idsatuankcl").val($(e).data('idsatuankcl'));
      $("#idsatuanbsr").val($(e).data('idsatuanbsr'));
      $("#jmlperbox").val($(e).data('jmlperbox'));
      $("#hrgbeli").val($(e).data('hrgbeli'));
      $("#hrgjual").val($(e).data('hrgjual'));
      $("#idpabrik").val($(e).data('idpabrik'));
      $("#kdsupplier").val($(e).data('kdsupplier'));
      $("#idstatus").val($(e).data('idstatus'));
      $("#keterangan").val($(e).data('keterangan'));


    }


  </script>
@endpush
