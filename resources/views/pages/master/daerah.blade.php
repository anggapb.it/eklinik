@extends('layouts.main')

@section('page.title', 'Daerah')
@section('page.heading', 'Daerah')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_daerah">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_daerah" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama</th>
                      <th class="align-middle" scope="col">Level Daerah</th>
                      <th class="align-middle" scope="col">Jenis Hirarki</th>
                      <th class="align-middle" scope="col">Parent</th>                      
                      <th class="align-middle" scope="col">Opsi</th>                      
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_daerah" tabindex="-1" aria-labelledby="modal_label_daerah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_daerah"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/daerah/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="iddaerah" name="iddaerah" required>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kddaerah" name="kddaerah" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmdaerah" name="nmdaerah" required>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Hirarki</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjnshirarki" id="idjnshirarki">
                        <option value="">Pilih</option>
                            @foreach ($jhirarki as $row)
                                <option value="{{ $row->idjnshirarki }}">{{ $row->nmjnshirarki }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Level daerah</label>
              <div class="col-sm-8">
                  <select class="form-control form-control-sm" name="idlvldaerah" id="idlvldaerah">
                        <option value="">Pilih</option>
                            @foreach ($lvldaerah as $row)
                                <option value="{{ $row->idlvldaerah }}">{{ $row->nmlvldaerah }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Parent</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="iddaerah" id="iddaerah">
                        <option value="">Pilih</option>
                            @foreach ($daerah as $row)
                                <option value="{{ $row->iddaerah }}">{{ $row->nmdaerah }}</option>
                            @endforeach
                      </select>
              </div>
            </div>

              </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/daerah/get-data') }}';

        var table = $('#tbl_daerah').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kddaerah', name: 'kddaerah'},
              {data: 'nmdaerah', name: 'nmdaerah'},
              {data: 'nmlvldaerah', name: 'nmlvldaerah'},
              {data: 'nmjnshirarki', name: 'nmjnshirarki'},
              {data: 'iddaerah', name: 'iddaerah'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_daerah"\
                                  data-iddaerah="'+row.iddaerah+'" \
                                  data-kddaerah="'+row.kddaerah+'" \
                                  data-nmdaerah="'+row.nmdaerah+'" \
                                  data-idjnshirarki="'+row.idjnshirarki+'" \
                                  data-idlvldaerah="'+row.idlvldaerah+'" \
                                  data-iddaerah="'+row.iddaerah+'" \
                                  onclick="edit_daerah(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/daerah/hapus').'/' }}'+row.iddaerah+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_daerah(e) {
      $("#iddaerah").val($(e).data('iddaerah'));
      $("#kddaerah").val($(e).data('kddaerah'));
      $("#nmdaerah").val($(e).data('nmdaerah'));
      $("#idjnshirarki").val($(e).data('idjnshirarki'));
      $("#idlvldaerah").val($(e).data('idlvldaerah'));
      $("#iddaerah").val($(e).data('iddaerah'));





    }


  </script>
@endpush
