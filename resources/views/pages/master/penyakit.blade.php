@extends('layouts.main')

@section('page.title', 'Penyakit')
@section('page.heading', 'Penyakit')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_penyakit">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_penyakit" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama penyakit(Indonesia)</th>
                      <th class="align-middle" scope="col">Nama Penyakit(English)</th>
                      <th class="align-middle" scope="col">Kategori</th>
                      <th class="align-middle" scope="col">Jenis Hirarki</th>
                      <th class="align-middle" scope="col">Parent</th>
                      <th class="align-middle" scope="col">DTD</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_penyakit" tabindex="-1" aria-labelledby="modal_label_penyakit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_penyakit"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/penyakit/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idpenyakit" name="idpenyakit" required>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdpenyakit" name="kdpenyakit" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Nama Penyakit(Indonesia)</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmpenyakit" name="nmpenyakit" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Nama Penyakit(English)</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmpenyakit" name="nmpenyakiteng" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Kategori</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kategori" name="kategori" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Hirarki</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjnshirarki" id="idjnshirarki">
                        <option value="">Pilih</option>
                            @foreach ($jhirarki as $row)
                                <option value="{{ $row->idjnshirarki }}">{{ $row->nmjnshirarki }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Parent</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="pen_idpenyakit" id="pen_idpenyakit">
                        <option value="">Pilih</option>
                            @foreach ($penyakit as $row)
                                <option value="{{ $row->pen_idpenyakit }}">{{ $row->nmpenyakiteng }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">DTD</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="dtd" name="dtd" required>
              </div>
            </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/penyakit/get-data') }}';

        var table = $('#tbl_penyakit').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdpenyakit', name: 'kdpenyakit'},
              {data: 'nmpenyakit', name: 'nmpenyakit'},
              {data: 'nmpenyakiteng', name: 'nmpenyakiteng'},
              {data: 'kategori', name: 'kategori'},
              {data: 'nmjnshirarki', name: 'nmjnshirarki'},
              {data: 'pen_idpenyakit', name: 'pen_idpenyakit'},
              {data: 'dtd', name: 'dtd'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_penyakit"\
                                  data-idpenyakit="'+row.idpenyakit+'" \
                                  data-kdpenyakit="'+row.kdpenyakit+'" \
                                  data-nmpenyakit="'+row.nmpenyakit+'" \
                                  data-nmpenyakiteng="'+row.nmpenyakiteng+'" \
                                  data-kategori="'+row.kategori+'" \
                                  data-idjnshirarki="'+row.idjnshirarki+'" \
                                  data-pen_idpenyakit="'+row.pen_idpenyakit+'" \
                                  data-dtd="'+row.dtd+'" \
                                  onclick="edit_penyakit(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/penyakit/hapus').'/' }}'+row.idpenyakit+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_penyakit(e) {
      $("#idpenyakit").val($(e).data('idpenyakit'));
      $("#kdpenyakit").val($(e).data('kdpenyakit'));
      $("#nmpenyakit").val($(e).data('nmpenyakit'));
      $("#nmpenyakiteng").val($(e).data('nmpenyakiteng'));
      $("#kategori").val($(e).data('kategori'));
      $("#idjnshirarki").val($(e).data('idjnshirarki'));
      $("#pen_idpenyakit").val($(e).data('pen_idpenyakit'));
      $("#dtd").val($(e).data('dtd'));

    }


  </script>
@endpush
