@extends('layouts.main')

@section('page.title', 'Master Template Pelayanan')
@section('page.heading', 'Master Template Pelayanan')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_mastertemplatepelayanan">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_mastertemplatepelayanan" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Nama Template pelayanan</th>
                      <th class="align-middle" scope="col">Jenis Hirarki</th>
                      <th class="align-middle" scope="col">Parent</th>
                      <th class="align-middle" scope="col">Status</th>
                      <th class="align-middle" scope="col">Opsi</th>                      
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_mastertemplatepelayanan" tabindex="-1" aria-labelledby="modal_label_mastertemplatepelayanan" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_mastertemplatepelayanan"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/master-template-pelayanan/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idtemplayanan" name="idtemplayanan" required>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Template pelayanan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmtemplayanan" name="nmtemplayanan" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Hirarki</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idjnshirarki" id="idjnshirarki">
                        <option value="">Pilih</option>
                            @foreach ($jhirarki as $row)
                                <option value="{{ $row->idjnshirarki }}">{{ $row->nmjnshirarki }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Parent</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="tem_idtemplayanan" id="tem_idtemplayanan">
                        <option value="">Pilih</option>
                            @foreach ($templatepelayanan as $row)
                                <option value="{{ $row->tem_idtemplayanan }}">{{ $row->nmtemplayanan }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idstatus" id="idstatus">
                        <option value="">Pilih</option>
                            @foreach ($status as $row)
                                <option value="{{ $row->idstatus }}">{{ $row->nmstatus }}</option>
                            @endforeach
                      </select>
              </div>
            </div>

              </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/master-template-pelayanan/get-data') }}';

        var table = $('#tbl_mastertemplatepelayanan').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'nmtemplayanan', name: 'nmtemplayanan'},
              {data: 'nmjnshirarki', name: 'nmjnshirarki'},
              {data: 'nmtemplayanan', name: 'nmtemplayanan'},
              {data: 'nmstatus', name: 'nmstatus'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_mastertemplatepelayanan"\
                                  data-idtemplayanan="'+row.idtemplayanan+'" \
                                  data-nmtemplayanan="'+row.nmtemplayanan+'" \
                                  data-idjnshirarki="'+row.idjnshirarki+'" \
                                  data-tem_idtemplayanan="'+row.tem_idtemplayanan+'" \
                                  data-idstatus="'+row.idstatus+'" \
                                  onclick="edit_mastertemplatepelayanan(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/master-template-pelayanan/hapus').'/' }}'+row.idtemplayanan+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_mastertemplatepelayanan(e) {
      $("#idtemplayanan").val($(e).data('idtemplayanan'));
      $("#nmtemplayanan").val($(e).data('nmtemplayanan'));
      $("#idjnshirarki").val($(e).data('idjnshirarki'));
      $("#tem_idtemplayanan").val($(e).data('tem_idtemplayanan'));
      $("#idstatus").val($(e).data('idstatus'));



    }


  </script>
@endpush
