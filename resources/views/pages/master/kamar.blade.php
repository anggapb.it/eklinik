@extends('layouts.main')

@section('page.title', 'Kamar')
@section('page.heading', 'Kamar')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_kamar">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_kamar" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Ruang Perawatan</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama kamar</th>
                      <th class="align-middle" scope="col">Fasilitas</th>
                      <th class="align-middle" scope="col">Opsi</th>                      
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_kamar" tabindex="-1" aria-labelledby="modal_label_kamar" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_kamar"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/kamar/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idkamar" name="idkamar" required>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Ruang Perawatan</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idbagian" id="idbagian">
                        <option value="">Pilih</option>
                            @foreach ($bagian as $row)
                                <option value="{{ $row->idbagian }}">{{ $row->nmbagian }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode Kamar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdkamar" name="kdkamar" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama kamar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmkamar" name="nmkamar" required>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Fasilitas</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="fasilitas" name="fasilitas" required>
              </div>
            </div>

              </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/kamar/get-data') }}';

        var table = $('#tbl_kamar').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'idbagian', name: 'idbagian'},
              {data: 'kdkamar', name: 'kdkamar'},
              {data: 'nmkamar', name: 'nmkamar'},
              {data: 'fasilitas', name: 'fasilitas'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_kamar"\
                                  data-idkamar="'+row.idkamar+'" \
                                  data-idbagian="'+row.idbagian+'" \
                                  data-kdkamar="'+row.kdkamar+'" \
                                  data-nmkamar="'+row.nmkamar+'" \
                                  data-fasilitas="'+row.fasilitas+'" \
                                  onclick="edit_kamar(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/kamar/hapus').'/' }}'+row.idkamar+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_kamar(e) {
      $("#idkamar").val($(e).data('idkamar'));
      $("#idbagian").val($(e).data('idbagian'));
      $("#kdkamar").val($(e).data('kdkamar'));
      $("#nmkamar").val($(e).data('nmkamar'));
      $("#fasilitas").val($(e).data('fasilitas'));





    }


  </script>
@endpush
