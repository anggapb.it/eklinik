@extends('layouts.main')

@section('page.title', 'Shift')
@section('page.heading', 'Shift')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_shift">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_shift" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama</th>
                      <th class="align-middle" scope="col">Dari Jam</th>
                      <th class="align-middle" scope="col">Sampai Jam</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_shift" tabindex="-1" aria-labelledby="modal_label_shift" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_shift"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/shift/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idshift" name="idshift" required>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdshift" name="kdshift" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Nama</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmshift" name="nmshift" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Dari Jam</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="darijam" name="darijam" required>
              </div>
            </div><div class="form-group row">
              <label for="satuan_organisasi_id" class="col-sm-3 col-form-label">Sampai Jam</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="sampaijam" name="sampaijam" required>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/shift/get-data') }}';

        var table = $('#tbl_shift').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdshift', name: 'kdshift'},
              {data: 'nmshift', name: 'nmshift'},
              {data: 'darijam', name: 'darijam'},
              {data: 'sampaijam', name: 'sampaijam'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_shift"\
                                  data-idshift="'+row.idshift+'" \
                                  data-kdshift="'+row.kdshift+'" \
                                  data-nmshift="'+row.nmshift+'" \
                                  data-darijam="'+row.darijam+'" \
                                  data-sampaijam="'+row.sampaijam+'" \
                                  onclick="edit_shift(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/shift/hapus').'/' }}'+row.idshift+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_shift(e) {
      $("#idshift").val($(e).data('idshift'));
      $("#kdshift").val($(e).data('kdshift'));
      $("#nmshift").val($(e).data('nmshift'));
      $("#darijam").val($(e).data('darijam'));
      $("#sampaijam").val($(e).data('sampaijam'));

    }


  </script>
@endpush
