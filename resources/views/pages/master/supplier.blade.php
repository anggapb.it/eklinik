@extends('layouts.main')

@section('page.title', 'Supplier')
@section('page.heading', 'Supplier')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_supplier">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_supplier" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Tanggal Daftar</th>
                      <th class="align-middle" scope="col">Nama supplier</th>
                      <th class="align-middle" scope="col">Alamat</th>
                      <th class="align-middle" scope="col">No. telp</th>
                      <th class="align-middle" scope="col">No. fax</th>
                      <th class="align-middle" scope="col">email</th>
                      <th class="align-middle" scope="col">website</th>
                      <th class="align-middle" scope="col">kontak person</th>
                      <th class="align-middle" scope="col">No. HP</th>
                      <th class="align-middle" scope="col">NPWP</th>
                      <th class="align-middle" scope="col">Bank</th>
                      <th class="align-middle" scope="col">No. Rek</th>
                      <th class="align-middle" scope="col">Atas Nama</th>
                      <th class="align-middle" scope="col">Keterangan</th>
                      <th class="align-middle" scope="col">Status</th>                      
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_supplier" tabindex="-1" aria-labelledby="modal_label_supplier" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_supplier"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/supplier/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdsupplier" name="kdsupplier" required>
              </div>
             </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tanggal Daftar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tgldaftar" name="tgldaftar" required>
              </div>
            </div> 
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama supplier</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmsupplier" name="nmsupplier" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Alamat</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="alamat" name="alamat" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. Telp</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="notelp" name="notelp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. Fax</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nofax" name="nofax" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Email</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="email" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Website</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="website" name="website" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kontak Person</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kontakperson" name="kontakperson" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. HP</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nohp" name="nohp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">NPWP</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="npwp" name="npwp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Bank</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idbank" id="idbank">
                        <option value="">Pilih</option>
                            @foreach ($bank as $row)
                                <option value="{{ $row->idbank }}">{{ $row->nmbank }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. Rek</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="norek" name="norek" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Atas Nama</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="atasnama" name="atasnama" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Keterangan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="keterangan" name="keterangan" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idstatus" id="idstatus">
                        <option value="">Pilih</option>
                            @foreach ($status as $row)
                                <option value="{{ $row->idstatus }}">{{ $row->nmstatus }}</option>
                            @endforeach
                      </select>
              </div>
            </div>

        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
        $('#tgldaftar').datepicker({
            format: 'dd-mm-yyyy',
            // startDate: '1d',
            autoclose: true
        });
        $('#tgldaftar').datepicker('update', new Date());
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/supplier/get-data') }}';

        var table = $('#tbl_supplier').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdsupplier', name: 'kdsupplier'},
              {data: 'tgldaftar', name: 'tgldaftar'},
              {data: 'nmsupplier', name: 'nmsupplier'},
              {data: 'alamat', name: 'alamat'},
              {data: 'notelp', name: 'notelp'},
              {data: 'nofax', name: 'nofax'},
              {data: 'email', name: 'email'},
              {data: 'website', name: 'website'},
              {data: 'kontakperson', name: 'kontakperson'},
              {data: 'nohp', name: 'nohp'},
              {data: 'npwp', name: 'npwp'},
              {data: 'nmbank', name: 'nmbank'},
              {data: 'norek', name: 'norek'},
              {data: 'atasnama', name: 'atasnama'},
              {data: 'keterangan', name: 'keterangan'},
              {data: 'nmstatus', name: 'nmstatus'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_supplier"\
                                  data-kdsupplier="'+row.kdsupplier+'" \
                                  data-tgldaftar="'+row.tgldaftar+'" \
                                  data-nmsupplier="'+row.nmsupplier+'" \
                                  data-alamat="'+row.alamat+'" \
                                  data-notelp="'+row.notelp+'" \
                                  data-nofax="'+row.nofax+'" \
                                  data-email="'+row.email+'" \
                                  data-website="'+row.website+'" \
                                  data-kontakperson="'+row.kontakperson+'" \
                                  data-nohp="'+row.nohp+'" \
                                  data-npwp="'+row.npwp+'" \
                                  data-idbank="'+row.idbank+'" \
                                  data-norek="'+row.norek+'" \
                                  data-atasnama="'+row.atasnama+'" \
                                  data-keterangan="'+row.keterangan+'" \
                                  data-idstatus="'+row.idstatus+'" \
                                  onclick="edit_supplier(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/supplier/hapus').'/' }}'+row.kdsupplier+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_supplier(e) {
      $("#kdsupplier").val($(e).data('kdsupplier'));
      $("#tgldaftar").val($(e).data('tgldaftar'));
      $("#nmsupplier").val($(e).data('nmsupplier'));
      $("#alamat").val($(e).data('alamat'));
      $("#notelp").val($(e).data('notelp'));
      $("#nofax").val($(e).data('nofax'));
      $("#email").val($(e).data('email'));
      $("#website").val($(e).data('website'));
      $("#kontakperson").val($(e).data('kontakperson'));
      $("#nohp").val($(e).data('nohp'));
      $("#npwp").val($(e).data('npwp'));
      $("#idbank").val($(e).data('idbank'));
      $("#norek").val($(e).data('norek'));
      $("#atasnama").val($(e).data('atasnama'));
      $("#keterangan").val($(e).data('keterangan'));
      $("#idstatus").val($(e).data('idstatus'));


    }


  </script>
@endpush
