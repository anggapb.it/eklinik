@extends('layouts.main')

@section('page.title', 'Penjamin')
@section('page.heading', 'Penjamin')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="row">
                <button type="button" class="btn btn-primary mt-4 ml-4" data-toggle="modal" data-target="#modal_penjamin">Tambah</button>
              </div>
              <div class="table-responsive">
                <table  id="tbl_penjamin" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Nama Penjamin</th>
                      <th class="align-middle" scope="col">Jenis Penjamin</th>
                      <th class="align-middle" scope="col">Alamat</th>
                      <th class="align-middle" scope="col">No. Telepon</th>
                      <th class="align-middle" scope="col">No. fax</th>
                      <th class="align-middle" scope="col">EmaiL</th>
                      <th class="align-middle" scope="col">Website</th>
                      <th class="align-middle" scope="col">Contact Person</th>
                      <th class="align-middle" scope="col">No. Handphone</th>
                      <th class="align-middle" scope="col">Tanggal Awal</th>
                      <th class="align-middle" scope="col">Tanggal Akhir</th>
                      <th class="align-middle" scope="col">Status</th>
                      <th class="align-middle" scope="col">Info Umum</th>
                      <th class="align-middle" scope="col">Info RJ</th>
                      <th class="align-middle" scope="col">Info RI</th>
                      <th class="align-middle" scope="col">Opsi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="modal_penjamin" tabindex="-1" aria-labelledby="modal_label_penjamin" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_penjamin"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('master/penjamin/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id" name="id">
          <div class="modal-body">
            <input type="hidden" class="form-control" id="idpenjamin" name="idpenjamin" required>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdpenjamin" name="kdpenjamin" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Penjamin</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmpenjamin" name="nmpenjamin" required>
              </div>
            </div>
             <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis penjamin</label>
              <div class="col-sm-8">
                 <select class="form-control form-control-sm" name="idjnspenjamin" id="idjnspenjamin">
                        <option value="">Pilih</option>
                            @foreach ($jpenjamin as $row)
                                <option value="{{ $row->idjnspenjamin }}">{{ $row->nmjnspenjamin }}</option>
                            @endforeach
                      </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Alamat</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="alamat" name="alamat" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. Telepon</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="notelp" name="notelp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. Fax</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nofax" name="nofax" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Email</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="email" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Website</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="website" name="website" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Contact Person</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmcp" name="nmcp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">No. Handphone</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nohp" name="nohp" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tanggal Awal</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tglawal" name="tglawal" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tanggal Akhir</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tglakhir" name="tglakhir" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idstatus" id="idstatus">
                        <option value="">Pilih</option>
                            @foreach ($status as $row)
                                <option value="{{ $row->idstatus }}">{{ $row->nmstatus }}</option>
                            @endforeach
                      </select>
              </div>
          </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Info Umum</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="infoumum" name="infoumum" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Info RJ</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inforj" name="inforj" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Info RI</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="infori" name="infori" required>
              </div>
            </div>
          
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
        $('#tglawal').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d',
            autoclose: true
        });
        $('#tglawal').datepicker('update', new Date());
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
        $('#tglakhir').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d',
            autoclose: true
        });
        $('#tglakhir').datepicker('update', new Date());
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('master/penjamin/get-data') }}';

        var table = $('#tbl_penjamin').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {data: 'kdpenjamin', name: 'kdpenjamin'},
              {data: 'nmpenjamin', name: 'nmpenjamin'},
              {data: 'nmjnspenjamin', name: 'nmjnspenjamin'},
              {data: 'alamat', name: 'alamat'},
              {data: 'notelp', name: 'notelp'},
              {data: 'nofax', name: 'nofax'},
              {data: 'email', name: 'email'},
              {data: 'website', name: 'website'},
              {data: 'nmcp', name: 'nmcp'},
              {data: 'nohp', name: 'nohp'},
              {data: 'tglawal', name: 'tglawal'},
              {data: 'tglakhir', name: 'tglakhir'},
              {data: 'nmstatus', name: 'nmstatus'},
              {data: 'infoumum', name: 'infoumum'},
              {data: 'inforj', name: 'inforj'},
              {data: 'infori', name: 'infori'},
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_penjamin"\
                                  data-idpenjamin="'+row.idpenjamin+'" \
                                  data-kdpenjamin="'+row.kdpenjamin+'" \
                                  data-nmpenjamin="'+row.nmpenjamin+'" \
                                  data-idjnspenjamin="'+row.idjnspenjamin+'" \
                                  data-alamat="'+row.alamat+'" \
                                  data-notelp="'+row.notelp+'" \
                                  data-nofax="'+row.nofax+'" \
                                  data-email="'+row.email+'" \
                                  data-website="'+row.website+'" \
                                  data-nmcp="'+row.nmcp+'" \
                                  data-nohp="'+row.nohp+'" \
                                  data-tglawal="'+row.tglawal+'" \
                                  data-tglakhir="'+row.tglakhir+'" \
                                  data-idstatus="'+row.idstatus+'" \
                                  data-infoumum="'+row.infoumum+'" \
                                  data-inforj="'+row.inforj+'" \
                                  data-infori="'+row.infori+'" \
                                  onclick="edit_penjamin(this)">Ubah</button>';
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-danger" href="{{ url('master/penjamin/hapus').'/' }}'+row.idpenjamin+'">Hapus</a>';

                    return result;
                  }
              }

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }



    function edit_penjamin(e) {
      $("#idpenjamin").val($(e).data('idpenjamin'));
      $("#kdpenjamin").val($(e).data('kdpenjamin'));
      $("#nmpenjamin").val($(e).data('nmpenjamin'));
      $("#idjnspenjamin").val($(e).data('idjnspenjamin'));
      $("#alamat").val($(e).data('alamat'));
      $("#notelp").val($(e).data('notelp'));
      $("#nofax").val($(e).data('nofax'));
      $("#email").val($(e).data('email'));
      $("#website").val($(e).data('website'));
      $("#nmcp").val($(e).data('nmcp'));
      $("#nohp").val($(e).data('nohp'));
      $("#tglawal").val($(e).data('tglawal'));
      $("#tglakhir").val($(e).data('tglakhir'));
      $("#idstatus").val($(e).data('idstatus'));
      $("#infoumum").val($(e).data('infoumum'));
      $("#inforj").val($(e).data('inforj'));
      $("#infori").val($(e).data('infori'));


    }


  </script>
@endpush