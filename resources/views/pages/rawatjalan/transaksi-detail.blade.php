@extends('layouts.main')

@section('page.title', 'Transaksi Rawat Jalan')
@section('page.heading', 'Transaksi Rawat Jalan')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Detail Registrasi Pasien</h4>
        </div>
        <div class="card-body">
            <form method="POST" id="frm_reg_rj" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="button-group">
                    <a href="{{ route('rj.transaksi-pasien') }}" class="btn waves-effect waves-light btn-secondary btn-sm">Kembali</a>
                    <button type="button" class="btn waves-effect waves-light btn-info btn-sm" onclick="submitRegistrasi()">Simpan</button>
                    <button type="button" class="btn waves-effect waves-light btn-warning btn-sm btn-list-registrasi">List Registrasi</button>
                    <button type="button" class="btn waves-effect waves-light btn-warning btn-sm">Input Diagnosa</button>
                    <button type="button" class="btn waves-effect waves-light btn-primary btn-sm">Cetak Nota </button>
                    <button type="button" class="btn waves-effect waves-light btn-primary btn-sm">Cetak Label</button>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">No RM</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" value="{{ $reg->norm }}" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">No. Registrasi</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="noreg" name="noreg" value="{{ $reg->noreg }}" readOnly>
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Nama Pasien</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien" value="{{ $reg->get_pasien->nmpasien }}" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">Tgl/Jam/Shift<span class="text-danger">*</span></label>
                    <div class="col-md-2">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglreg" name="tglreg" value="{{ $reg->get_reg_det->tglreg}}" readOnly>
                   </div>
                    <label class="control-label text-right col-xs-1">/</label>
                    <div class="col-md-1">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jamreg" name="jamreg" value="{{ $reg->get_reg_det->jamreg}}" readOnly>
                   </div>
                    <label class="control-label text-right col-xs-1">/</label>
                    <div class="col-md-1">
                        <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idshift" name="idshift" value="{{ $reg->get_reg_det->get_shift->idshift}}">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="shift" name="shift" value="{{ $reg->get_reg_det->get_shift->nmshift}}" readOnly>
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Jenis Kelamin</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jkelamin" name="jkelamin" value="{{ $reg->get_pasien->jkelamin->nmjnskelamin }}" readOnly>
                    </div>
                    <label class="col-md-2 text-right">Unit Pelayanan<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian" value="{{ $reg->get_reg_det->get_bagian->nmbagian }}" readOnly>
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Penjamin<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian" value="{{ $reg->get_penjamin->nmpenjamin }}" readOnly>
                    </div>
                    <label class="col-md-2 text-right">Dokter<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="dokter" name="dokter" value="{{ $reg->get_reg_det->get_dokter->nmdoktergelar }}" readOnly>

                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Cara Datang<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="caradatang" name="caradatang" value="{{ $reg->get_reg_det->get_cara_datang->nmcaradatang }}" readOnly>

                    </div>
                    <label class="col-md-2 text-right">No. Antrian</label>
                    <div class="col-md-1">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="noantrian" name="noantrian" value="{{ $reg->get_reg_det->get_reservasi->noantrian }}" readOnly>
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Catatan</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="catatan_regis" name="catatan_regis" value="{{ $reg->catatan }}">
                    </div>

                </div>
                </form>

        </div>
    </div>
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Tindakan</h4>
        </div>
        <div class="card-body">
            <div class="button-group">
                <button type="button" class="btn waves-effect waves-light btn-info btn-sm" >Tambah Tindakan</button>
            </div>
            <div class="table-responsive m-t-60">
                <table id="tbl_tindakan" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>Item Tindakan</th>
                            <th>Dokter</th>
                            <th>Tarif</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Diskon</th>
                            <th>Total Diskon</th>
                            <th>Subtotal</th>
                            <th>Dijamin (%)</th>
                            <th>Dijamin (Rp)</th>
                            <th>Selisih</th>
                            <th>Dijamin</th>
                            <th>Tgl. Diberikan</th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>

    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Obat / Alat Kesehatan</h4>
        </div>
        <div class="card-body">
            <div class="button-group">
                <button type="button" class="btn waves-effect waves-light btn-info btn-sm" >Tambah Obat / Alkes</button>
                <button type="button" class="btn waves-effect waves-light btn-primary btn-sm" >Tambah Racikan</button>
            </div>
            <div class="table-responsive m-t-60">
                <table id="tbl_obat" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>R/</th>
                            <th>Item Obat / Alkes</th>
                            <th>Aturan Pakai</th>
                            <th>Nama Racikan</th>
                            <th>Tarif</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Subtotal</th>
                            <th>Dijamin (%)</th>
                            <th>Dijamin (Rp)</th>
                            <th>Selisih</th>
                            <th>Dijamin</th>
                            <th>Tgl. Diberikan</th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalRegistrasi" tabindex="-1" role="dialog" aria-labelledby="modalRegLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Registrasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                        <h5 class="box-title m-t-30">Periode</h5>

                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" autocomplete="off" class="form-control" id="tglawal" />
                            <div class="input-group-append">
                                <span class="input-group-text bg-info b-0 text-white">s/d</span>
                            </div>
                            <input type="text" autocomplete="off" class="form-control" id="tglakhir" />
                        </div>
                </div>
                <div class="col-md-4">
                    <div class="example">
                        <h5 class="box-title m-t-30">Unit Pelayanan</h5>
                        <select id="filter_bagian" class="select2 form-control custom-select" style="width: 100%; height:30px;">
                            <option value="">Pilih</option>
                            @foreach($bagian as $item)
                                <option value="{{ $item->idbagian }}">{{ $item->nmbagian }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-mt-1">
                        <h5 class="box-title m-t-30">&nbsp;</h5>
                        <button type="button" id="btn_cari" class="btn btn-primary">Cari</button>

                </div>
                <div class="col-md-1">
                        <h5 class="box-title m-t-30">&nbsp;</h5>
                        <button type="button" id="btn_refresh" class="btn btn-info">Refresh</button>
                </div>
            </div>

            <div class="table-responsive m-t-40">
                <table id="tbl_list_registrasi" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>No Reg</th>
                            <th>Pasien</th>
                            <th>L/P</th>
                            <th>Unit Pelayanan</th>
                            <th>Dokter</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>


@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){

     $(".select2").select2();


});

$('body').on('click', '.btn-list-registrasi', function () {
       $('#modalRegistrasi').modal('show');

    });


function loadRegistrasi(){
    const page_url = "{{ route('rj.list-registrasi') }}";

    var table = $('#tbl_list_registrasi').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                        tglawal :  $("#tglawal").val(),
                        tglakhir :  $("#tglakhir").val(),
                        bagian :  $("#filter_bagian").val()
                }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                            var result = '<a class="btn btn-info btn-proses" href="#"\
                                data-noreg="'+row.noreg+'"\
                                >Detail</a>';
                                if(row.status == 1){
                                    result += '&nbsp; <button type="button"\
                                    class="btn waves-effect waves-light btn-danger btn-batal">Batalkan</button>';
                                }

                                return result;
                         }
                    },
            {data: 'noreg', name: 'noreg', orderable: true,searchable: true},
            {data: 'nmpasien', name: 'nmpasien', orderable: true,searchable: true},
            {data: 'kdjnskelamin', name: 'kdjnskelamin', orderable: true,searchable: true},
            {data: 'nmbagian', name: 'nmbagian', orderable: true,searchable: true},
            {data: 'nmdoktergelar', name: 'nmdoktergelar', orderable: true,searchable: true},
            {
                        data: 'status', name: 'status', searchable: true,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if(row.status == 1){
                                return 'Belum Diproses';
                            } else{
                                return 'Sudah Diproses';
                            }

                        }
                },
          ],
          responsive: true,
          columnDefs: [
                {
                    targets: [0],
                    className: "no-wrap",
                    width: "200px",
                }
          ],

      });
}

</script>
@endpush
