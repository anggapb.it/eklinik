@extends('layouts.main')

@section('page.title', 'Transaksi')
@section('page.heading', 'Transaksi')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Detail Registrasi Pasien</h4>
        </div>
        <div class="card-body">
            <form method="POST" id="frm_reg_rj" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="button-group">
                    {{-- <a href="{{ route('rj.transaksi-pasien') }}" class="btn waves-effect waves-light btn-secondary btn-sm">Kembali</a> --}}
                    <button type="button" class="btn waves-effect waves-light btn-primary btn-sm btn-list-registrasi">List Registrasi</button>
                    <button type="button" class="btn waves-effect waves-light btn-info btn-sm btn-draft-trx" onclick="saveDraftNota()">Lanjutkan Input Tindakan & Obat / Alkes</button>
                    <button type="button" class="btn waves-effect waves-light btn-info btn-sm btn-finish-trx" onclick="finishNota()">Simpan Perubahan</button>

                    <button type="button" class="btn waves-effect waves-light btn-warning btn-sm">Input Diagnosa</button>
                    <button type="button" class="btn waves-effect waves-light btn-primary btn-sm">Cetak Nota </button>
                    <button type="button" class="btn waves-effect waves-light btn-primary btn-sm">Cetak Label</button>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">No RM</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">No. Nota</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="nonota" name="nonota" readOnly>
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Nama Pasien</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">No. Resep</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="noresep" name="noresep" readOnly>
                   </div>

                </div>
                <div class="form-row mt-1">
                    <label class="control-label text-right col-md-2">No. Registrasi</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="noreg" name="noreg" readOnly>
                   </div>
                    <label class="control-label text-right col-md-2">Tgl/Jam/Shift<span class="text-danger">*</span></label>
                    <div class="col-md-2">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglreg" name="tglreg" readOnly>
                   </div>
                    <label class="control-label text-right col-xs-1">/</label>
                    <div class="col-md-1">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jamreg" name="jamreg" readOnly>
                   </div>
                    <label class="control-label text-right col-xs-1">/</label>
                    <div class="col-md-1">
                        <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idshift" name="idshift">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="shift" name="shift" readOnly>
                   </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Penjamin<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="penjamin" name="penjamin" readOnly>
                    </div>
                    <label class="col-md-2 text-right">Unit Pelayanan<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian"  readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Cara Datang<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="caradatang" name="caradatang" readOnly>

                    </div>
                    <label class="col-md-2 text-right">Dokter<span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="dokter" name="dokter" readOnly>

                    </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Catatan</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="catatan_regis" name="catatan_regis">
                    </div>
                    <label class="col-md-2 text-right">No. Antrian</label>
                    <div class="col-md-1">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="noantrian" name="noantrian" readOnly>
                    </div>

                </div>
                </form>

        </div>
    </div>
    <div class="card card-outline-info panel_tindakan">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Tindakan</h4>
        </div>
        <div class="card-body">
            <div class="button-group">
                <button type="button" class="btn waves-effect waves-light btn-info btn-sm btn-add-item" >Tambah Tindakan</button>
            </div>

            <div class="table-responsive m-t-60">
                <table id="tbl_tindakan_bynota" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>Item Tindakan</th>
                            <th>Dokter</th>
                            <th>Tarif</th>
                            <th>Qty</th>
                            <th>Diskon</th>
                            <th>Total Diskon</th>
                            <th>Subtotal</th>
                            <th>Dijamin (%)</th>
                            <th>Dijamin (Rp)</th>
                            <th>Selisih</th>
                            <th>Dijamin</th>
                            <th>Tgl. Diberikan</th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>

    <div class="card card-outline-info panel_obat">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Obat / Alat Kesehatan</h4>
        </div>
        <div class="card-body">
            <div class="button-group">
                <button type="button" class="btn waves-effect waves-light btn-info btn-sm btn-add-obat" >Tambah Obat / Alkes</button>
                <button type="button" class="btn waves-effect waves-light btn-primary btn-sm" >Tambah Racikan</button>
            </div>
            <div class="table-responsive m-t-60">
                <table id="tbl_obat_bynota" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>R/</th>
                            <th>Item Obat / Alkes</th>
                            <th>Aturan Pakai</th>
                            <th>Nama Racikan</th>
                            <th>Tarif</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Subtotal</th>
                            <th>Dijamin (%)</th>
                            <th>Dijamin (Rp)</th>
                            <th>Selisih</th>
                            <th>Dijamin</th>
                            <th>Tgl. Diberikan</th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalRegistrasi" tabindex="-1" role="dialog" aria-labelledby="modalRegLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Registrasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-5">
                        <h5 class="box-title m-t-30">Periode</h5>

                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" autocomplete="off" class="form-control" id="tglawal" />
                            <div class="input-group-append">
                                <span class="input-group-text bg-info b-0 text-white">s/d</span>
                            </div>
                            <input type="text" autocomplete="off" class="form-control" id="tglakhir" />
                        </div>
                </div>
                <div class="col-md-4">
                    <div class="example">
                        <h5 class="box-title m-t-30">Unit Pelayanan</h5>
                        <select id="filter_bagian" class="select2 form-control custom-select" style="width: 100%; height:30px;">
                            <option value="">Pilih</option>
                            @foreach($bagian as $item)
                                <option value="{{ $item->idbagian }}">{{ $item->nmbagian }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-mt-1">
                        <h5 class="box-title m-t-30">&nbsp;</h5>
                        <button type="button" id="btn_cari" class="btn btn-primary">Cari</button>

                </div>
                {{-- <div class="col-md-1">
                        <h5 class="box-title m-t-30">&nbsp;</h5>
                        <button type="button" id="btn_refresh" class="btn btn-info">Refresh</button>
                </div> --}}
            </div>

            <div class="table-responsive m-t-40">
                <table id="tbl_list_registrasi" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>No Reg</th>
                            <th>Pasien</th>
                            <th>L/P</th>
                            <th>Unit Pelayanan</th>
                            <th>Dokter</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>


<div class="modal fade" id="modalTindakan" tabindex="-1" role="dialog" aria-labelledby="modalTindakanLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalTindakanLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Tindakan</label>
                <div class="col-md-6">
                  <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idnotadet_item" name="idnotadet_item" readOnly>
                  <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="kditem" name="kditem" readOnly>
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmitem" name="nmitem" readOnly>
                </div>
                <div class="col-md-2">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pilih-tindakan">...</button>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Tarif</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="tarif" name="tarif" readOnly>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Qty</label>
                <div class="col-md-1">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="qty_item" name="qty_item">
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Subtotal</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="subtotal_item" name="subtotal_item" readOnly>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Dijamin (%)</label>
                <div class="col-md-1">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="dijamin_persen_item" name="dijamin_persen_item">
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Dijamin (Rp.)</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="dijamin_rp_item" name="dijamin_rp_item">
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Selisih</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="selisih_item" name="selisih_item" readOnly>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Dijamin Semua?</label>
                <div class="col-md-1">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="dijamin_all_item" name="dijamin_all_item">
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-3 text-right">Dokter</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="dokter_item" name="dokter_item">
                </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveTindakan()">Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalListTindakan" tabindex="-1" role="dialog" aria-labelledby="modalListTindakanLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Tindakan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_tindakan' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Item Tindakan</th>
                    <th class="text-center">Harga</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalObat" tabindex="-1" role="dialog" aria-labelledby="modalObatLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalObatLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Obat /  Alkes</label>
                <div class="col-md-6">
                  <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idnotadet_brg" name="idnotadet_brg" readOnly>
                  <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="kdbrg" name="kdbrg" readOnly>
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmbrg" name="nmbrg" readOnly>
                </div>
                <div class="col-md-2">
                <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pilih-obat">...</button>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Tarif</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="hrgjual" name="hrgjual" readOnly>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Qty</label>
                <div class="col-md-1">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="qty_brg" name="qty_brg">
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Subtotal</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="subtotal_brg" name="subtotal_brg" readOnly>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Dijamin (%)</label>
                <div class="col-md-1">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="dijamin_persen_brg" name="dijamin_persen_brg">
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Dijamin (Rp.)</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="dijamin_rp_brg" name="dijamin_rp_brg">
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Selisih</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="selisih_brg" name="selisih_brg" readOnly>
                </div>
            </div>
            <div class="form-row mt-1">
                <label for="tindakan" class="col-md-4 text-right">Dijamin Semua?</label>
                <div class="col-md-1">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="dijamin_all_brg" name="dijamin_all_brg">
                </div>
            </div>
            <div class="form-row mt-1">
                <label class="col-md-4 text-right">Aturan Pakai</label>
                <div class="col-md-6">
                  <input type="text" class="form-control form-control-sm" autocomplete="off" id="aturan_pakai" name="aturan_pakai">
                </div>
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveObat()">Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalListObat" tabindex="-1" role="dialog" aria-labelledby="modalListObatLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Obat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_obat' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Obat / Alkes</th>
                    <th class="text-center">Harga</th>
                    <th class="text-center">Stok</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>


@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
     $(".select2").select2();
     $('#date-range').datepicker({
        toggleActive: true
    });

    $('#tglawal').datepicker('update', new Date());
    $('#tglakhir').datepicker('update', new Date());
     loadRegistrasi();

     $('.panel_tindakan').hide();
                    $('.panel_obat').hide();
     $('.btn-finish-trx').hide();

});

$('body').on('click', '.btn-list-registrasi', function () {
       $('#modalRegistrasi').modal('show');

    });


function loadRegistrasi(){
    const page_url = "{{ route('rj.list-registrasi') }}";

    var table = $('#tbl_list_registrasi').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                        tglawal :  $("#tglawal").val(),
                        tglakhir :  $("#tglakhir").val(),
                        bagian :  $("#filter_bagian").val()
                }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                            var result = '<a class="btn btn-info btn-pilih-reg btn-sm" href="#"\
                                data-norm="'+row.norm+'"\
                                data-nmpasien="'+row.nmpasien+'"\
                                data-nmjnskelamin="'+row.nmjnskelamin+'"\
                                data-nmpenjamin="'+row.nmpenjamin+'"\
                                data-nmcaradatang="'+row.nmcaradatang+'"\
                                data-catatan="'+row.catatan+'"\
                                data-nmshift="'+row.nmshift+'"\
                                data-tglreg="'+row.tglreg+'"\
                                data-jamreg="'+row.jamreg+'"\
                                data-nmbagian="'+row.nmbagian+'"\
                                data-nmdokter="'+row.nmdoktergelar+'"\
                                data-noantrian="'+row.noantrian+'"\
                                data-noreg="'+row.noreg+'"\
                                data-nonota="'+row.nonota+'"\
                                >Pilih</a>';
                                if(row.status == 1){
                                    result += '&nbsp; <button type="button"\
                                    class="btn waves-effect waves-light btn-danger btn-sm btn-batal">Batalkan</button>';
                                }

                                return result;
                         }
                    },
            {data: 'noreg', name: 'noreg', orderable: true,searchable: true},
            {data: 'nmpasien', name: 'nmpasien', orderable: true,searchable: true},
            {data: 'kdjnskelamin', name: 'kdjnskelamin', orderable: true,searchable: true},
            {data: 'nmbagian', name: 'nmbagian', orderable: true,searchable: true},
            {data: 'nmdoktergelar', name: 'nmdoktergelar', orderable: true,searchable: true},
            {
                        data: 'status', name: 'status', searchable: true,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if(row.status == 1){
                                return 'Belum Diproses';
                            } else{
                                return 'Sudah Diproses';
                            }

                        }
                },
          ],
          responsive: true,
          columnDefs: [
                {
                    targets: [0],
                    className: "no-wrap",
                    width: "300px",
                }
          ],

      });
}

$('#btn_cari').click(function(){
          $('#tbl_list_registrasi').DataTable().destroy();
          loadRegistrasi();
});

$('body').on('click', '.btn-pilih-reg', function () {
           var nonota = $(this).data('nonota');
        $("#norm").val($(this).data('norm'));
        $("#nmpasien").val($(this).data('nmpasien'));
        $("#noreg").val($(this).data('noreg'));
        $("#tglreg").val($(this).data('tglreg'));
        $("#jamreg").val($(this).data('jamreg'));
        $("#shift").val($(this).data('nmshift'));
        $("#penjamin").val($(this).data('nmpenjamin'));
        $("#caradatang").val($(this).data('nmcaradatang'));
        $("#catatan_regis").val($(this).data('catatan'));
        $("#bagian").val($(this).data('nmbagian'));
        $("#dokter").val($(this).data('nmdokter'));
        $("#noantrian").val($(this).data('noantrian'));
        $("#nonota").val(nonota);
        if(nonota != null){
            $('.panel_tindakan').show();
            $('.panel_obat').show();
            $('.btn-draft-trx').hide();
            $('.btn-finish-trx').show();
            //load tindakan
            loadTindakanByNota();
            loadObatByNota();
        }
        $('#modalRegistrasi').modal('hide');
    });

    $('.btn-add-item').click(function(){
        $("#idnotadet_item").val('');
        var norm = $("#norm").val();
        if(norm == ''){
            Swal.fire("Error!", "Pasien belum dipilih", "error");
        }else{
            $('#modalTindakan').modal('show');
            $('#InputModalTindakanLabel').text('Tambah Tindakan');
        }

    });

    $('.btn-pilih-tindakan').click(function(){
       $('#modalListTindakan').modal('show');
       loadTindakan();

    });

    $('.btn-pilih-obat').click(function(){
       $('#modalListObat').modal('show');
       loadObat();

    });

    $('.btn-add-obat').click(function(){
        $("#idnotadet_brg").val('');
        var norm = $("#norm").val();
        if(norm == ''){
            Swal.fire("Error!", "Pasien belum dipilih", "error");
        }else{
            $('#InputModalObatLabel').text('Tambah Obat');
            $('#modalObat').modal('show');
        }

    });

function loadTindakan(){
    $("#tbl_list_tindakan").dataTable().fnDestroy();
    const page_url = "{{ route('rj.get-tindakan') }}";

    var table = $('#tbl_list_tindakan').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
            //   data: {
            //     peg_id :  $("#peg_id").val(),
            //   }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-pilih-item" href="#"\
                                data-kditem="'+row.kditem+'"\
                                data-nmitem="'+row.nmitem+'"\
                                data-ttltarif="'+row.ttltarif+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'nmitem', name: 'nmitem', orderable: false,searchable: true},
            {data: 'ttltarif', name: 'ttltarif', orderable: true,searchable: true},
          ],
          responsive: true,
          columnDefs: [
            {
                  render:function (data, type, row) {
                        return commaSeparateNumber(data);
                  },
                  targets:[2]
              },
          ],

      });
}

function loadTindakanByNota(){
    $("#tbl_tindakan_bynota").dataTable().fnDestroy();
    const page_url = "{{ route('rj.get-tindakan-bynota') }}";

    var table = $('#tbl_tindakan_bynota').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                nonota :  $("#nonota").val(),
              }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-warning btn-sm btn-edit-item" href="#"\
                                data-kditem="'+row.kditem+'"\
                                data-nmitem="'+row.nmitem+'"\
                                data-qty="'+row.qty+'"\
                                data-tarif="'+row.tarif+'"\
                                data-subtotal="'+row.tarif2+'"\
                                data-dijaminprsn="'+row.dijaminprsn+'"\
                                data-dijaminrp="'+row.dijaminrp+'"\
                                data-selisih="'+row.selisih+'"\
                                data-iddokter="'+row.iddokter+'"\
                                data-id="'+row.idnotadet+'"\
                                >Edit</a>';
                                result += '&nbsp;<a class="btn btn-danger btn-sm btn-delete-item"\
                                data-id="' + row.idnotadet + '" href="javascript:void(0);">Hapus</a>';
                                return result;
                         }
                    },
            {data: 'nmitem', name: 'nmitem', orderable: false,searchable: false},
            {data: 'nmdoktergelar', name: 'nmdoktergelar', orderable: false,searchable: false},
            {data: 'tarif', name: 'tarif', orderable: false,searchable: false},
            {data: 'qty', name: 'qty', orderable: false,searchable: false},
            {data: 'diskon', name: 'diskon', orderable: false,searchable: false},
            {data: 'diskon', name: 'diskon', orderable: false,searchable: false},
            {data: 'tarif2', name: 'tarif2', orderable: false,searchable: false},
            {data: 'dijaminprsn', name: 'dijaminprsn', orderable: false,searchable: false},
            {data: 'dijaminrp', name: 'dijaminrp', orderable: false,searchable: false},
            {data: 'selisih', name: 'selisih', orderable: false,searchable: false},
            {data: 'dijamin', name: 'dijamin', orderable: false,searchable: false},
            {data: 'tgl_diberikan', name: 'tgl_diberikan', orderable: false,searchable: false},
          ],
          responsive: true,
          columnDefs: [
            {
                  render:function (data, type, row) {
                        return commaSeparateNumber(data);
                  },
                  targets:[3,7,9,10]
              },
          ],
          dom:'t'

      });
}


function loadObatByNota(){
    $("#tbl_obat_bynota").dataTable().fnDestroy();
    const page_url = "{{ route('rj.get-obat-bynota') }}";

    var table = $('#tbl_obat_bynota').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                nonota :  $("#nonota").val(),
              }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-warning btn-sm btn-edit-obat" href="#"\
                                data-kditem="'+row.kditem+'"\
                                data-nmitem="'+row.nmitem+'"\
                                data-aturanpakai="'+row.aturanpakai+'"\
                                data-qty="'+row.qty+'"\
                                data-tarif="'+row.tarif+'"\
                                data-subtotal="'+row.tarif2+'"\
                                data-dijaminprsn="'+row.dijaminprsn+'"\
                                data-dijaminrp="'+row.dijaminrp+'"\
                                data-selisih="'+row.selisih+'"\
                                data-id="'+row.idnotadet+'"\
                                >Edit</a>';
                                result += '&nbsp;<a class="btn btn-danger btn-sm btn-delete-obat"\
                                data-id="' + row.idnotadet + '" href="javascript:void(0);">Hapus</a>';
                                return result;
                         }
                    },
            {data: 'aturanpakai', name: 'aturanpakai', orderable: false,searchable: false},
            {data: 'nmitem', name: 'nmitem', orderable: false,searchable: false},
            {data: 'aturanpakai', name: 'aturanpakai', orderable: false,searchable: false},
            {data: 'aturanpakai', name: 'aturanpakai', orderable: false,searchable: false},
            {data: 'tarif', name: 'tarif', orderable: false,searchable: false},
            {data: 'qty', name: 'qty', orderable: false,searchable: false},
            {data: 'nmsatuan', name: 'nmsatuan', orderable: false,searchable: false},
            {data: 'tarif2', name: 'tarif2', orderable: false,searchable: false},
            {data: 'dijaminprsn', name: 'dijaminprsn', orderable: false,searchable: false},
            {data: 'dijaminrp', name: 'dijaminrp', orderable: false,searchable: false},
            {data: 'selisih', name: 'selisih', orderable: false,searchable: false},
            {data: 'dijamin', name: 'dijamin', orderable: false,searchable: false},
            {data: 'tgl_diberikan', name: 'tgl_diberikan', orderable: false,searchable: false}
          ],
          responsive: true,
          columnDefs: [

          ],
          dom:'t'

      });
}
function loadObat(){
    $("#tbl_list_obat").dataTable().fnDestroy();
    const page_url = "{{ route('rj.get-obat') }}";

    var table = $('#tbl_list_obat').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
            //   data: {
            //     peg_id :  $("#peg_id").val(),
            //   }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-added-obat" href="#"\
                                data-kdbrg="'+row.kdbrg+'"\
                                data-nmbrg="'+row.barang.nmbrg+'"\
                                data-hrgjual="'+row.barang.hrgjual+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'barang.nmbrg', name: 'barang.nmbrg', orderable: true,searchable: true},
            {data: 'barang.hrgjual', name: 'barang.hrgjual', orderable: true,searchable: true},
            {data: 'stoknowbagian', name: 'stoknowbagian', orderable: true,searchable: true},
          ],
          responsive: true,
          columnDefs: [
            {
                  render:function (data, type, row) {
                        return commaSeparateNumber(data);
                  },
                  targets:[2]
              },
          ],

      });
}

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

$('body').on('click', '.btn-pilih-item', function () {
        $("#kditem").val($(this).data('kditem'));
        $("#nmitem").val($(this).data('nmitem'));
        $("#tarif").val($(this).data('ttltarif'));
        $("#qty_item").val('1');
        $("#dijamin_rp_item").val(0);
        $("#dijamin_persen_item").val(0);
        $("#subtotal_item").val($(this).data('ttltarif'));
        $("#selisih_item").val($(this).data('ttltarif'));
        $('#modalListTindakan').modal('hide');
});

$('body').on('click', '.btn-added-obat', function () {
        $("#kdbrg").val($(this).data('kdbrg'));
        $("#nmbrg").val($(this).data('nmbrg'));
        $("#hrgjual").val($(this).data('hrgjual'));
        $("#qty_brg").val('1');
        $("#dijamin_rp_brg").val(0);
        $("#dijamin_persen_brg").val(0);
        $("#subtotal_brg").val($(this).data('hrgjual'));
        $("#selisih_brg").val($(this).data('hrgjual'));
        $('#modalListObat').modal('hide');
});

$('body').on('click', '.btn-edit-item', function () {
        $("#idnotadet_item").val($(this).data('id'));
        $("#kditem").val($(this).data('kditem'));
        $("#nmitem").val($(this).data('nmitem'));
        $("#tarif").val($(this).data('tarif'));
        $("#qty_item").val($(this).data('qty'));
        $("#dijamin_rp_item").val($(this).data('dijaminprsn'));
        $("#dijamin_persen_item").val($(this).data('dijaminrp'));
        $("#subtotal_item").val($(this).data('subtotal'));
        $("#selisih_item").val($(this).data('selisih'));
        $("#dokter_item").val($(this).data('iddokter'));
        $('#InputModalTindakanLabel').text('Edit Tindakan');
        $('#modalTindakan').modal('show');
});

$('body').on('click', '.btn-edit-obat', function () {
        $("#idnotadet_brg").val($(this).data('id'));
        $("#kdbrg").val($(this).data('kditem'));
        $("#nmbrg").val($(this).data('nmitem'));
        $("#hrgjual").val($(this).data('tarif'));
        $("#qty_brg").val($(this).data('qty'));
        $("#dijamin_rp_brg").val($(this).data('dijaminprsn'));
        $("#dijamin_persen_brg").val($(this).data('dijaminrp'));
        $("#subtotal_brg").val($(this).data('subtotal'));
        $("#selisih_brg").val($(this).data('selisih'));
        $("#dokter_brg").val($(this).data('iddokter'));
        $('#InputModalObatLabel').text('Edit Obat');
        $('#modalObat').modal('show');
});

function saveDraftNota(){
       var norm = $('#norm').val();
       if(norm == ''){
          Swal.fire("Error!", "Pasien belum dipilih", "error");
       }else{
                    var form_data = new FormData();
                    form_data.append("noreg", $('#noreg').val());
                    form_data.append("nonota", $('#nonota').val());
           $.ajax({
            type: "POST",
            url: "{{ route('rj.insert-draft-nota-rj') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
          //  data: $('#frm_pengajuan').serialize(),
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                if(result.message != "") {
                    Swal.fire("Success!", result.message, "success");
                    $('#nonota').val(result.nota.nonota);
                    $('.panel_tindakan').show();
                    $('.panel_obat').show();
                    $('.btn-finish-trx').show();
                    $('.btn-draft-trx').hide();
                } else {
                    Swal.fire("Error!", result.error, "error");
                }
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
       }

    }

  $("#qty_item").change(function(){
    var qty =  $( this ).val();
    var tarif = $('#tarif').val();
    var subtotal = parseInt(tarif) * qty;
    var dijaminrp = $('#dijamin_rp_item').val();
    var selisih = subtotal - parseInt(dijaminrp);
    $('#subtotal_item').val(subtotal);
    $('#selisih_item').val(selisih);
  });

  $("#dijamin_rp_item").change(function(){
    var qty =  $('#qty_item').val();
    var tarif = $('#tarif').val();
    var subtotal = parseInt(tarif) * qty;
    var dijaminrp = $( this ).val();
    var selisih = subtotal - parseInt(dijaminrp);
    $('#subtotal_item').val(subtotal);
    $('#selisih_item').val(selisih);
  });

  $("#qty_brg").change(function(){
    var qty =  $( this ).val();
    var tarif = $('#hrgjual').val();
    var subtotal = parseInt(tarif) * qty;
    var dijaminrp = $('#dijamin_rp_brg').val();
    var selisih = subtotal - parseInt(dijaminrp);
    $('#subtotal_brg').val(subtotal);
    $('#selisih_brg').val(selisih);
  });

  $("#dijamin_rp_brg").change(function(){
    var qty =  $('#qty_brg').val();
    var tarif = $('#hrgjual').val();
    var subtotal = parseInt(tarif) * qty;
    var dijaminrp = $( this ).val();
    var selisih = subtotal - parseInt(dijaminrp);
    $('#subtotal_brg').val(subtotal);
    $('#selisih_brg').val(selisih);
  });

function saveTindakan(){
    //    var norm = $('#norm').val();
    //    if(norm == ''){
    //       Swal.fire("Error!", "Pasien belum dipilih", "error");
    //    }else{
                    var form_data = new FormData();
                    form_data.append("idnotadet", $('#idnotadet_item').val());
                    form_data.append("noreg", $('#noreg').val());
                    form_data.append("kditem", $('#kditem').val());
                    form_data.append("nonota", $('#nonota').val());
                    form_data.append("qty", $('#qty_item').val());
           $.ajax({
            type: "POST",
            url: "{{ route('rj.insert-tindakan') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
          //  data: $('#frm_pengajuan').serialize(),
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                if(result.message != "") {
                    iziToast.show({
                    title: 'Sukses!',
                    message: result.message+'!',
                    position: 'topRight',
                    color: 'green'
                    });
                    var oTable = $('#tbl_tindakan_bynota').dataTable();
                    oTable.fnDraw(false);
                    $("#modalTindakan").modal('hide');
                } else {
                    Swal.fire("Error!", result.error, "error");
                }
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
      // }

    }


function saveObat(){
    //    var norm = $('#norm').val();
    //    if(norm == ''){
    //       Swal.fire("Error!", "Pasien belum dipilih", "error");
    //    }else{
                    var form_data = new FormData();
                    form_data.append("idnotadet", $('#idnotadet_brg').val());
                    form_data.append("noreg", $('#noreg').val());
                    form_data.append("kditem", $('#kdbrg').val());
                    form_data.append("nonota", $('#nonota').val());
                    form_data.append("qty", $('#qty_brg').val());
                    form_data.append("hrgjual", $('#hrgjual').val());
                    form_data.append("aturan_pakai", $('#aturan_pakai').val());
           $.ajax({
            type: "POST",
            url: "{{ route('rj.insert-obat') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
          //  data: $('#frm_pengajuan').serialize(),
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                if(result.message != "") {
                    iziToast.show({
                    title: 'Sukses!',
                    message: result.message+'!',
                    position: 'topRight',
                    color: 'green'
                    });
                     var oTable = $('#tbl_obat_bynota').dataTable();
                     oTable.fnDraw(false);
                     $("#modalObat").modal('hide');
                } else {
                    Swal.fire("Error!", result.error, "error");
                }
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
      // }

    }



$('body').on('click', '.btn-delete-item', function () {

var id = $(this).data("id");
Swal.fire({
          title: "Apakah anda yakin?",
          text: "",
          type: 'warning',
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "get",
                    url: "{{ url('rj/transaksi/hapus-notadet') }}"+'/'+id,
                    success: function (data) {
                        iziToast.show({
                                title: 'Sukses!',
                                message: 'Hapus Berhasil!',
                                position: 'topRight',
                                color: 'green'
                                });
                        var oTable = $('#tbl_tindakan_bynota').dataTable();
                                    oTable.fnDraw(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });


});

$('body').on('click', '.btn-delete-obat', function () {

var id = $(this).data("id");
Swal.fire({
          title: "Apakah anda yakin?",
          text: "",
          type: 'warning',
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "get",
                    url: "{{ url('rj/transaksi/hapus-notadet') }}"+'/'+id,
                    success: function (data) {
                        iziToast.show({
                                title: 'Sukses!',
                                message: 'Hapus Berhasil!',
                                position: 'topRight',
                                color: 'green'
                                });
                        var oTable = $('#tbl_obat_bynota').dataTable();
                                    oTable.fnDraw(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
});




</script>
@endpush
