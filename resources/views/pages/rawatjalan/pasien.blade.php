@extends('layouts.main')

@section('page.title', 'Pendaftaran')
@section('page.heading', 'Pendaftaran')

@push('top.styles')
<style>

</style>
@endpush

@section('page.content')
<div class="col-lg-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Pasien</h4>
        </div>
        <div class="card-body">
            <form method="POST" id="frm_pasien" class="form-horizontal" enctype="multipart/form-data">
                @csrf
            <div class="button-group">
                <button type="button" class="btn waves-effect waves-light btn-secondary" onclick="clearForm()">Baru</button>
                <button type="button" class="btn waves-effect waves-light btn-info" onclick="submitPasien()">Simpan</button>
                <button type="button" class="btn waves-effect waves-light btn-warning" onclick="cetakKartuPasien()">Cetak Kartu Pasien</button>
                <button type="button" class="btn waves-effect waves-light btn-primary" onclick="inputRegistrasi()">Input Pendaftaran</button>
            </div>

                <div class="form-row mt-1">

                    <label for="nip" class="col-md-2 text-right">No RM</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" placeholder="Otomatis" readOnly>
                    </div>
                    <div class="col-md-2">
                    <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pasien">...</button>
                    </div>
                    <label class="control-label text-right col-md-2">Agama</label>
                    <div class="col-md-1">
                      <select class="form-control form-control-sm" name="idagama" id="idagama">
                        <option value="">Pilih</option>
                            @foreach ($agama as $row)
                                <option value="{{ $row->idagama }}">{{ $row->nmagama }}</option>
                            @endforeach
                      </select>
                   </div>
                    <label class="control-label text-right col-xs-1">Gol. Darah</label>
                    <div class="col-md-1">
                      <select class="form-control form-control-sm" name="idgoldarah" id="idgoldarah">
                        <option value="">Pilih</option>
                            @foreach ($goldarah as $row)
                                <option value="{{ $row->idgoldarah }}">{{ $row->nmgoldarah }}</option>
                            @endforeach
                      </select>
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Nama Pasien</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien">
                    </div>
                    <label for="noidentitas" class="col-md-2 text-right">No. Identitas</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="noidentitas" name="noidentitas">
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label for="jns_kelamin" class="col-md-2 text-right">Jenis Kelamin</label>
                    <div class="col-mt-1">
                        <select class="form-control form-control-sm" name="jkelamin" id="jkelamin" required>
                            <option value="">Pilih</option>
                            <option value="1">Laki-laki</option>
                            <option value="2">Perempuan</option>
                          </select>
                    </div>
                    <label for="st_kawin" class="col-xs-1 text-right">St. Kawin</label>
                    <div class="col-mt-2">
                        <select class="form-control form-control-sm" name="idstkawin" id="idstkawin">
                            <option value="">Pilih</option>
                            @foreach ($stkawin as $row)
                                <option value="{{ $row->idstkawin }}">{{ $row->nmstkawin }}</option>
                            @endforeach
                          </select>
                    </div>
                    <label class="control-label text-right col-md-2">Pendidikan</label>
                    <div class="col-md-1">
                      <select class="form-control form-control-sm" name="idpendidikan" id="idpendidikan">
                        <option value="">Pilih</option>
                            @foreach ($pendidikan as $row)
                                <option value="{{ $row->idpendidikan }}">{{ $row->nmpendidikan }}</option>
                            @endforeach
                      </select>
                   </div>
                    <label class="control-label text-right col-xs-1">Pekerjaan</label>
                    <div class="col-mt-1">
                      <select class="form-control form-control-sm" name="idpekerjaan" id="idpekerjaan">
                        <option value="">Pilih</option>
                            @foreach ($pekerjaan as $row)
                                <option value="{{ $row->idpekerjaan }}">{{ $row->nmpekerjaan }}</option>
                            @endforeach
                      </select>
                   </div>

                </div>
                <div class="form-row mt-1" border="1">
                    <label class="col-md-2 text-right">Tempat Lahir</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tptlahir" name="tptlahir">
                    </div>
                    <label class="col-xs-1 text-right">Tgl Lahir</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgllahir" name="tgllahir">
                    </div>

                    <label class="control-label text-right col-mt-2">Kebangsaan</label>
                    <div class="col-mt-1">
                      <select class="form-control form-control-sm" name="idwn" id="idwn">
                        <option value="">Pilih</option>
                        <option value=1>WNI</option>
                        <option value=2>WNA</option>
                      </select>
                   </div>
                    <label class="control-label text-right col-xs-1">Negara</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="negara" name="negara">
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Umur</label>
                    <div class="col-md-1">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="umur_thn" name="umur_thn">
                    </div>
                    <label class="control-label text-right col-xs-1">Tahun</label>
                    <div class="col-md-1">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="umur_bln" name="umur_bln">
                    </div>
                    <label class="control-label text-right col-xs-1">Bulan</label>
                    <div class="col-md-1">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="umur_hari" name="umur_hari">
                    </div>
                    <div class="col-md-1">
                    <label class="control-label text-right col-xs-1">Hari</label>
                    </div>

                    <label class="col-mt-1 text-right">Daerah</label>
                    <div class="col-md-3">
                      <input type="hidden" id="iddaerah_origin" name="iddaerah_origin">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="iddaerah" name="iddaerah">
                    </div>
                    <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-daerah">...</button>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Alamat</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="alamat" name="alamat">
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Nama Orangtua</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmortu" name="nmortu">
                    </div>
                    <label class="col-md-2 text-right">Tgl. Lahir Ortu</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgllahir_ortu" name="tgllahir_ortu">
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Nama Pasangan</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasangan" name="nmpasangan">
                    </div>
                    <label class="col-md-2 text-right">Tgl. Lahir Pasangan</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgllahir_pasangan" name="tgllahir_pasangan">
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">No HP</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nohp" name="nohp">
                    </div>
                    <label class="col-md-2 text-right">Catatan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="catatan" name="catatan">
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">Alergi Obat</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="alergi" name="alergi">
                    </div>
                </div>

            </form>

        </div>
    </div>
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Riwayat Kedatangan Pasien</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive m-t-60">
                <table id="tbl_riw_pasien" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No. Reg</th>
                            <th>Tgl</th>
                            <th>Jam</th>
                            <th>Nama Penjamin</th>
                            <th>Unit / Ruangan Pelayanan</th>
                            <th>Jenis Pelayanan</th>
                            <th>Nama Dokter</th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPasien" tabindex="-1" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Pasien</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-bordered" id='tbl_pasien' width="100%">
                 <thead>
                   <tr>
                     <th class="text-center">Aksi</th>
                     <th class="text-center">No RM</th>
                     <th class="text-center">Nama</th>
                     <th class="text-center">L/P</th>
                     <th class="text-center">Tanggal Lahir</th>
                     <th class="text-center">Alamat Pasien</th>
                     <th class="text-center">No. HP</th>
                   </tr>
                 </thead>
               </table>
             </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="modalDaerah" tabindex="-1" role="dialog" aria-labelledby="modalDaerahLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">Data Daerah</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-bordered" id='tbl_daerah' width="100%">
                 <thead>
                   <tr>
                     <th class="text-center">Aksi</th>
                     <th class="text-center">Daerah</th>
                     <th class="text-center">Level Daerah</th>
                   </tr>
                 </thead>
               </table>
             </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
  </div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('#tgllahir').datepicker({
            format: 'dd-mm-yyyy',
          //  startDate: '1d',
            autoclose: true
        });
        $('#tgllahir_ortu').datepicker({
            format: 'dd-mm-yyyy',
           // startDate: '1d',
            autoclose: true
        });
        $('#tgllahir_pasangan').datepicker({
            format: 'dd-mm-yyyy',
           // startDate: '1d',
            autoclose: true
        });

        loadPasien();
        loadDaerah();

    });

    $('body').on('click', '.btn-pasien', function () {
       $('#modalPasien').modal('show');

    });

    $('body').on('click', '.btn-daerah', function () {
       $('#modalDaerah').modal('show');
    });


function loadPasien(){
    const page_url = "{{ route('pasien.get-data') }}";

    var table = $('#tbl_pasien').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
            //   data: {
            //     peg_id :  $("#peg_id").val(),
            //   }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-pilih-pasien" href="#"\
                                data-norm="'+row.norm+'"\
                                data-nmpasien="'+row.nmpasien+'"\
                                data-idjnskelamin="'+row.idjnskelamin+'"\
                                data-idstkawin="'+row.idstkawin+'"\
                                data-idagama="'+row.idagama+'"\
                                data-idgoldarah="'+row.idgoldarah+'"\
                                data-noidentitas="'+row.noidentitas+'"\
                                data-idpendidikan="'+row.idpendidikan+'"\
                                data-idpekerjaan="'+row.idpekerjaan+'"\
                                data-tptlahir="'+row.tptlahir+'"\
                                data-tgllahir="'+row.tgllahir+'"\
                                data-idwn="'+row.idwn+'"\
                                data-negara="'+row.negara+'"\
                                data-iddaerah="'+row.iddaerah+'"\
                                data-nmdaerah="'+row.nmdaerah+'"\
                                data-alamat="'+row.alamat+'"\
                                data-nmibu="'+row.nmibu+'"\
                                data-tgllahirortu="'+row.tgllahirortu+'"\
                                data-nmpasangan="'+row.nmpasangan+'"\
                                data-tgllahirpasangan="'+row.tgllahirpasangan+'"\
                                data-nohp="'+row.nohp+'"\
                                data-catatan="'+row.catatan+'"\
                                data-alergi="'+row.alergi+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'norm', name: 'norm', orderable: false,searchable: true},
            {data: 'nmpasien', name: 'nmpasien', orderable: true,searchable: true},
            {data: 'kdjnskelamin', name: 'kdjnskelamin', orderable: false,searchable: true},
            {data: 'tgllahir', name: 'tgllahir', orderable: false,searchable: true},
            {data: 'alamat', name: 'alamat', orderable: false,searchable: true},
            {data: 'nohp', name: 'nohp', orderable: false,searchable: true}
          ],
          responsive: true,
          columnDefs: [
              {
                render:function(data){
                    return data.toString();
                }, targets:[1],
              },
              {
                render:function(data){
                    return moment(data).format('MM-DD-YYYY');
                }, targets:[4],
              }
          ],
         // dom: 't',

      });
}

function loadDaerah(){
    const page_url = "{{ route('rj.get-daerah') }}";

    var table = $('#tbl_daerah').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
            //   data: {
            //     peg_id :  $("#peg_id").val(),
            //   }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-pilih-daerah" href="#"\
                                data-iddaerah="'+row.iddaerah+'"\
                                data-nmdaerah="'+row.nmdaerah+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'nmdaerah', name: 'nmdaerah', orderable: false,searchable: true},
            {data: 'nmlvldaerah', name: 'nmlvldaerah', orderable: true,searchable: true},
          ],
          responsive: true,
          columnDefs: [

          ],

      });
}

$('#tgllahir').on('changeDate', function(e) {
       umur(new Date(e.dates));
    });

function submitPasien(){
                var form_data = new FormData();
                    form_data.append("norm", $('#norm').val());
                    form_data.append("nmpasien", $('#nmpasien').val());
                    form_data.append("jkelamin", $('#jkelamin').val());
                    form_data.append("idstkawin", $('#idstkawin').val());
                    form_data.append("idagama", $('#idagama').val());
                    form_data.append("idgoldarah", $('#idgoldarah').val());
                    form_data.append("noidentitas", $('#noidentitas').val());
                    form_data.append("idpendidikan", $('#idpendidikan').val());
                    form_data.append("idpekerjaan", $('#idpekerjaan').val());
                    form_data.append("tptlahir", $('#tptlahir').val());
                    form_data.append("tgllahir", $('#tgllahir').val());
                    form_data.append("idwn", $('#idwn').val());
                    form_data.append("negara", $('#negara').val());
                    form_data.append("iddaerah", $('#iddaerah_origin').val());
                    form_data.append("alamat", $('#alamat').val());
                    form_data.append("nmortu", $('#nmortu').val());
                    form_data.append("nmpasangan", $('#nmpasangan').val());
                    form_data.append("tgllahir_ortu", $('#tgllahir_ortu').val());
                    form_data.append("tgllahir_pasangan", $('#tgllahir_pasangan').val());
                    form_data.append("nohp", $('#nohp').val());
                    form_data.append("catatan", $('#catatan').val());
                    form_data.append("alergi", $('#alergi').val());
           $.ajax({
            type: "POST",
            url: "{{ route('pasien.store') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
          //  data: $('#frm_pengajuan').serialize(),
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                if(result.message != "") {
                    Swal.fire("Success!", result.message, "success");
                } else {
                    Swal.fire("Error!", result.error, "error");
                }
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });

    }

    function inputRegistrasi()
    {
        var norm = $("#norm").val();
        window.location = "{{ url('rj/detail-registrasi') }}"+'/'+norm;
    }

    $('body').on('click', '.btn-pilih-pasien', function () {
        $("#norm").val($(this).data('norm'));
        $("#nmpasien").val($(this).data('nmpasien'));
        $("#jkelamin").val($(this).data('idjnskelamin'));
        $("#idstkawin").val($(this).data('idstkawin'));
        $("#idagama").val($(this).data('idagama'));
        $("#idgoldarah").val($(this).data('idgoldarah'));
        $("#noidentitas").val($(this).data('noidentitas'));
        $("#idpendidikan").val($(this).data('idpendidikan'));
        $("#idpekerjaan").val($(this).data('idpekerjaan'));
        $("#tptlahir").val($(this).data('tptlahir'));
        $("#tgllahir").val($(this).data('tgllahir'));
        $("#idwn").val($(this).data('idwn'));
        $("#negara").val($(this).data('negara'));
        $("#iddaerah").val($(this).data('iddaerah'));
        $("#nmdaerah").val($(this).data('nmdaerah'));
        $("#alamat").val($(this).data('alamat'));
        $("#nmortu").val($(this).data('nmibu'));
        $("#tgllahir_ortu").val($(this).data('tgllahirortu'));
        $("#nmpasangan").val($(this).data('nmpasangan'));
        $("#tgllahir_pasangan").val($(this).data('tgllahirpasangan'));
        $("#nohp").val($(this).data('nohp'));
        $("#catatan").val($(this).data('catatan'));
        $("#alergi").val($(this).data('alergi'));
        umur(new Date($(this).data('tgllahir')));

        loadRiwayatPasien($(this).data('norm'));

        $('#modalPasien').modal('hide');
    });

    $('body').on('click', '.btn-pilih-daerah', function () {
        $("#iddaerah_origin").val($(this).data('iddaerah'));
        $("#iddaerah").val($(this).data('nmdaerah'));
        $('#modalDaerah').modal('hide');
    });

    function clearForm()
    {
        $("#norm").val('');
        $("#nmpasien").val('');
        $("#jkelamin").val('');
        $("#idstkawin").val('');
        $("#idagama").val('');
        $("#idgoldarah").val('');
        $("#noidentitas").val('');
        $("#idpendidikan").val('');
        $("#idpekerjaan").val('');
        $("#tptlahir").val('');
        $("#tgllahir").val('');
        $("#idwn").val('');
        $("#negara").val('');
        $("#iddaerah").val('');
        $("#alamat").val('');
        $("#nmibu").val('');
        $("#tgllahir_ortu").val('');
        $("#nmpasangan").val('');
        $("#tgllahir_pasangan").val('');
        $("#nohp").val('');
        $("#catatan").val('');
        $("#alergi").val('');
        $('#tbl_riw_pasien tbody').empty();
    }

    function cetakKartuPasien()
    {
        Swal.fire("Error!", "Masih pengembangan", "error");
    }

    function umur(val) {
		var date = new Date();
		var td = date.getDate();var d = val.getDate();
		var tm = date.getMonth();var m = val.getMonth();
		var ty = date.getFullYear();var y = val.getFullYear();

		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
        $("#umur_thn").val(year);
        $("#umur_bln").val(month);
        $("#umur_hari").val(day);
	}

    function loadRiwayatPasien(norm){
        $("#tbl_riw_pasien").dataTable().fnDestroy();
    const page_url = "{{ route('rj.get-riwayat-pasien') }}";

    var table = $('#tbl_riw_pasien').DataTable({
          processing: true,
          serverSide: true,
          paging: false,
            searching: false,
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                norm :  norm,
              }
          },
          columns: [
            {data: 'noreg', name: 'noreg', orderable: false,searchable: false},
            {data: 'get_reg_det.tglreg', name: 'get_reg_det.tglreg', orderable: false,searchable: false},
            {data: 'get_reg_det.jamreg', name: 'get_reg_det.jamreg', orderable: false,searchable: false},
            {data: 'nmpenjamin', name: 'nmpenjamin', orderable: false,searchable: false},
            {data: 'get_reg_det.get_bagian.nmbagian', name: 'get_reg_det.get_bagian.nmbagian', orderable: false,searchable: false},
            {data: 'nmjnspelayanan', name: 'nmjnspelayanan', orderable: false,searchable: false},
            {data: 'get_reg_det.get_dokter.nmdoktergelar', name: 'get_reg_det.get_dokter.nmdoktergelar', orderable: false,searchable: false},
          ],
          responsive: true,
          columnDefs: [

          ],

      });
    }
</script>
@endpush
