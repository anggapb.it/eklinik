@extends('layouts.main')

@section('page.title', 'Pembayaran Transaksi')
@section('page.heading', 'Pembayaran Transaksi')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Detail Transaksi Pasien</h4>
        </div>
        <div class="card-body">
            <form method="POST" id="frm_trx_pasien" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="button-group">
                    <button type="button" class="btn waves-effect waves-light btn-warning btn-bayar">Bayar</button>
                    <button type="button" class="btn waves-effect waves-light btn-primary">Cetak Nota </button>
                    <button type="button" class="btn waves-effect waves-light btn-primary">Cetak Kwitansi</button>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-2 text-right">No RM</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" value="{{ $reg->norm }}" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">No. Registrasi</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="noreg" name="noreg" value="{{ $noreg }}" readOnly>
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Nama Pasien</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmpasien" name="nmpasien" value="{{ $reg->get_pasien->nmpasien }}" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">Tgl/Jam/Shift<span class="text-danger">*</span></label>
                    <div class="col-md-2">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglreg" name="tglreg" value="{{ $reg->get_reg_det->tglreg }}" readOnly>
                   </div>
                    <label class="control-label text-right col-xs-1">/</label>
                    <div class="col-md-1">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="jamreg" name="jamreg" value="{{ $reg->get_reg_det->jamreg }}" readOnly>
                   </div>
                    <label class="control-label text-right col-xs-1">/</label>
                    <div class="col-md-1">
                        <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="idshift" name="idshift">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="shift" name="shift" value="{{ $reg->get_reg_det->get_shift->nmshift }}" readOnly>
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Unit Pelayanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian" value="{{ $reg->get_reg_det->get_bagian->nmbagian }}" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">Atas Nama</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="atasnama" name="atasnama" value="{{ $reg->get_pasien->nmpasien }}">
                   </div>
                </div>
                <div class="form-row mt-1">
                    <label class="control-label text-right col-md-2">Dokter</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="dokter" name="dokter" value="{{ $reg->get_reg_det->get_dokter->nmdoktergelar }}" readOnly>
                   </div>
                    <label for="nip" class="col-md-2 text-right">No. Nota</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control form-control-sm" autocomplete="off"  id="nonota" name="nonota" value="{{ $transaksi->nonota }}" readOnly>
                    </div>
                    <label class="col-md-1 text-right">No. Kuitansi</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" placeholder="Otomatis"  id="nokuitansi" name="nokuitansi" value="{{ $transaksi->nokuitansi}}" readOnly>
                    </div>

                </div>

                <div class="form-row mt-1">
                    <div class="col-md-12">
                            <table id="tbl_kuitansi_det" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Opsi</th>
                                        <th>Bayar</th>
                                        <th>Bank</th>
                                        <th>No. Kartu</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>

                            </table>

                    </div>
                </div>
                <div class="form-row mt-1">
                    <label class="col-md-8 text-right">Total Tagihan</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="tottransaksi" name="tottransaksi" value="{{ number_format($transaksi->total_tagihan,0,',','.') }}" readOnly>
                      </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-8 text-right">Total Dijamin</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="totdijamin" name="totdijamin" value="{{ number_format($transaksi->total_dijamin,0,',','.') }}" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-8 text-right">Jasa Racik</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="totjasaracik" name="totjasaracik" value="{{ number_format($transaksi->jasa_racik,0,',','.') }}" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-8 text-right">Total Diskon</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="totdiskon" name="totdiskon" value="{{ number_format($transaksi->total_diskon,0,',','.') }}" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-8 text-right">Total Bayar</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="totbayar" name="totbayar" value="{{ number_format($total_bayar,0,',','.') }}" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-8 text-right">Total Tagihan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tottagihan" name="tottagihan" value="{{ number_format($sisa_tagihan,0,',','.') }}" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label class="col-md-8 text-right">Total Pembulatan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="totpembulatan" name="totpembulatan" value="{{ number_format($sisa_tagihan,0,',','.') }}" readOnly>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modalByr" tabindex="-1" aria-labelledby="modal_label_byr" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_byr"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="#" enctype="multipart/form-data">
            @csrf
          <div class="modal-body">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Cara Bayar <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idcarabayar" id="idcarabayar">
                    <option value="">Pilih</option>
                        @foreach ($carabayar as $row)
                            <option value="{{ $row->idcarabayar }}">{{ $row->nmcarabayar }}</option>
                        @endforeach
                  </select>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">Bank</label>
              <div class="col-sm-8">
                <select class="form-control form-control-sm" name="idbank" id="idbank">
                    <option value="">Pilih</option>
                        @foreach ($bank as $row)
                            <option value="{{ $row->idbank }}">{{ $row->nmbank }}</option>
                        @endforeach
                  </select>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">No. Kartu</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nokartu" name="nokartu">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Sisa tagihan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="sisa_tagihan" name="sisa_tagihan" value="Rp. {{ number_format($sisa_tagihan,0,',','.') }}" readOnly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jumlah Bayar <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nominal" name="nominal">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-primary" onclick="submitPembayaran();">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });

$(".select2").select2();
var tottagihan = $("#tottagihan").val();
if(tottagihan == 0){
    $('.btn-bayar').attr("disabled", true);
}

loadKuitansiDet();

});

function loadKuitansiDet(){
    const page_url = "{{ route('kasir.list-kuitansidet') }}";

    var table = $('#tbl_kuitansi_det').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                        nokuitansi :  $("#nokuitansi").val(),


                }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                            var result = '<a class="btn btn-danger btn-sm btn-delete-kuidet"\
                                data-id="' + row.idkuitansidet + '" href="javascript:void(0);"\
                                >Hapus</a>';


                                return result;
                         }
                    },
            {data: 'carabayar.nmcarabayar', name: 'carabayar.nmcarabayar', orderable: true,searchable: true},
            {data: 'nmbank', name: 'nmbank', orderable: true,searchable: true},
            {data: 'nokartu', name: 'nokartu', orderable: true,searchable: true},
            {data: 'jumlah', name: 'jumlah', orderable: true,searchable: true},
          ],
          responsive: true,
          columnDefs: [
            {
                  render:function (data, type, row) {
                        return commaSeparateNumber(data);
                  },
                  targets:[4]
              },
          ],
          dom:'t'

      });
}

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

$('.btn-bayar').click(function(){
    $("#modalByr").modal('show');
});

function submitPembayaran(){
    var carabayar = $('#idcarabayar').val();
    var nominal = $('#nominal').val();
       if(carabayar == '' && nominal == ''){
          Swal.fire("Error!", "Kolom yang bertanda (*) wajib diisi", "error");
       }else{
        var form_data = new FormData();
                    form_data.append("nokuitansi", $('#nokuitansi').val());
                    form_data.append("idcarabayar", $('#idcarabayar').val());
                    form_data.append("idbank", $('#idbank').val());
                    form_data.append("nokartu", $('#nokartu').val());
                    form_data.append("sisa_tagihan", $('#sisa_tagihan').val());
                    form_data.append("nominal", $('#nominal').val());
                    form_data.append("noreg", $('#noreg').val());
                    form_data.append("norm", $('#norm').val());
                    form_data.append("atasnama", $('#atasnama').val());
                    form_data.append("tagihan", $('#tottagihan').val());
                    form_data.append("nonota", $('#nonota').val());

           $.ajax({
            type: "POST",
            url: "{{ route('kasir.insert-bayar') }}",
          //  data: $('#frm_pengajuan').serialize(),
            data: form_data,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                if(result.message != "") {
                    $("#modalByr").modal('hide');
                   // $("#nokuitansi").val(result.list.nokuitansi);
                    // var oTable = $('#tbl_kuitansi_det').dataTable();
                    // oTable.fnDraw(false);
                    Swal.fire("Success!", result.message, "success");
                    location.reload();

                } else {
                    Swal.fire("Error!", result.error, "error");
                }
            } ,error: function(xhr, status, error) {
                alert(error);
            },

        });
       }


    }

$('body').on('click', '.btn-delete-kuidet', function () {

var id = $(this).data("id");
Swal.fire({
          title: "Apakah anda yakin?",
          text: "",
          type: 'warning',
          showCancelButton: true,
          showCloseButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "get",
                    url: "{{ url('kasir/hapus-kuidet') }}"+'/'+id,
                    success: function (data) {
                        // iziToast.show({
                        //         title: 'Sukses!',
                        //         message: 'Hapus Berhasil!',
                        //         position: 'topRight',
                        //         color: 'green'
                        //         });
                        Swal.fire("Success!", result.message, "success");
                        location.reload();
                        // var oTable = $('#tbl_kuitansi_det').dataTable();
                        //             oTable.fnDraw(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
});
</script>
@endpush
