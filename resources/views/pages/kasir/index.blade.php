@extends('layouts.main')

@section('page.title', 'Pembayaran Transaksi')
@section('page.heading', 'Pembayaran Transaksi')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Daftar registrasi pasien yang sudah di proses</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                        <h5 class="box-title m-t-30">Periode</h5>

                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" autocomplete="off" class="form-control" id="tglawal" />
                            <div class="input-group-append">
                                <span class="input-group-text bg-info b-0 text-white">s/d</span>
                            </div>
                            <input type="text" autocomplete="off" class="form-control" id="tglakhir" />
                        </div>
                </div>
                <div class="col-md-4">
                    <div class="example">
                        <h5 class="box-title m-t-30">Unit Pelayanan</h5>
                        <select id="filter_bagian" class="select2 form-control custom-select" style="width: 100%; height:30px;">
                            <option value="">Pilih</option>
                            @foreach($bagian as $item)
                                <option value="{{ $item->idbagian }}">{{ $item->nmbagian }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-mt-1">
                        <h5 class="box-title m-t-30">&nbsp;</h5>
                        <button type="button" id="btn_cari" class="btn btn-primary">Cari</button>

                </div>
                <div class="col-md-1">
                        <h5 class="box-title m-t-30">&nbsp;</h5>
                        <button type="button" id="btn_refresh" class="btn btn-info">Refresh</button>
                </div>
            </div>

            <div class="table-responsive m-t-40">
                <table id="tbl_list_registrasi" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>No Reg</th>
                            <th>Pasien</th>
                            <th>L/P</th>
                            <th>Unit Pelayanan</th>
                            <th>Dokter</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){

     $(".select2").select2();

    $('#date-range').datepicker({
        toggleActive: true
    });

    $('#tglawal').datepicker('update', new Date());
    $('#tglakhir').datepicker('update', new Date());

    loadRegistrasi();

});


function loadRegistrasi(){
    const page_url = "{{ route('rj.list-registrasi-proses') }}";

    var table = $('#tbl_list_registrasi').DataTable({
          processing: true,
          serverSide: true,
         // language: { processing: getTemplate('fa-spinner') },
          ajax: {
              url: page_url,
              type: 'GET',
              data: {
                        tglawal :  $("#tglawal").val(),
                        tglakhir :  $("#tglakhir").val(),
                        bagian :  $("#filter_bagian").val()
                }
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                            var result = '<a class="btn btn-info btn-primary"\
                                 href="{{ url("kasir/detail") }}'+'/'+ row.noreg + '"\
                                >Detail</a>';


                                return result;
                         }
                    },
            {data: 'noreg', name: 'noreg', orderable: true,searchable: true},
            {data: 'nmpasien', name: 'nmpasien', orderable: true,searchable: true},
            {data: 'kdjnskelamin', name: 'kdjnskelamin', orderable: true,searchable: true},
            {data: 'nmbagian', name: 'nmbagian', orderable: true,searchable: true},
            {data: 'nmdoktergelar', name: 'nmdoktergelar', orderable: true,searchable: true},
            {
                        data: 'status', name: 'status', searchable: true,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if(row.status == 1){
                                return 'Belum Diproses';
                            } else{
                                return 'Sudah Diproses';
                            }

                        }
                },
          ],
          responsive: true,
          columnDefs: [
                {
                    targets: [0],
                    className: "no-wrap",
                    width: "200px",
                }
          ],

      });
}

$('#btn_cari').click(function(){
          $('#tbl_list_registrasi').DataTable().destroy();
          loadRegistrasi();
       });
$('#btn_refresh').click(function(){
    var oTable = $('#tbl_list_registrasi').dataTable();
                    oTable.fnDraw(false);
       });
</script>
@endpush
