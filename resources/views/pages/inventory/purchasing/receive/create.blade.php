@extends('layouts.main')

@section('page.title', 'Penerimaan Barang')
@section('page.heading', 'Penerimaan Barang')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Form Penerimaan Barang</h4>
        </div>
        <div class="card-body">
            <form method="POST" id="frm_pasien" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="button-group">
                    <button type="button" class="btn waves-effect waves-light btn-info" onclick="submitPo()">Simpan</button>
                    <button type="button" class="btn waves-effect waves-light btn-warning" onclick="javascript:location.reload()">Reset</button>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">No Penerimaan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="norm" name="norm" placeholder="Otomatis" readOnly>
                    </div>

                    <label class="control-label text-right col-md-2">Supplier <span class="text-danger">*</span></label>
                    <div class="col-md-3">
                        <select class="form-control form-control-sm select2" name="supplier" id="supplier" required>
                            <option value="">--Pilih--</option>
                                @foreach ($supplier as $row)
                                    <option value="{{ $row->kdsupplier }}">{{ $row->nmsupplier }}</option>
                                @endforeach
                          </select>
                   </div>

                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Tgl. Terima <span class="text-danger">*</span></label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgl_pembelian" name="tgl_pembelian" required>
                    </div>

                    <label class="control-label text-right col-md-2">Syarat Pembayaran <span class="text-danger">*</span></label>
                    <div class="col-md-4">
                        <select class="form-control form-control-sm" name="syarat_pembayaran" id="syarat_pembayaran" required>
                            <option value="">--Pilih--</option>
                                @foreach ($syarat_pembayaran as $row)
                                    <option value="{{ $row->idsypembayaran }}">{{ $row->nmsypembayaran }}</option>
                                @endforeach
                          </select>
                   </div>

                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">No. Pesanan <span class="text-danger">*</span></label>
                    <div class="col-md-2">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="nopesanan" name="nopesanan" required>
                      </div>
                      <div class="col-md-1">
                          <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-nopesanan">...</button>
                          </div>

                    <label class="control-label text-right col-md-2">Jenis Pembayaran <span class="text-danger">*</span></label>
                    <div class="col-md-4">
                        <select class="form-control form-control-sm" name="jenis_pembayaran" id="jenis_pembayaran" required>
                            <option value="">--Pilih--</option>
                                @foreach ($jpembayaran as $row)
                                    <option value="{{ $row->idjnspembayaran }}">{{ $row->nmjnspembayaran }}</option>
                                @endforeach
                          </select>
                   </div>

                </div>
                <div class="form-row mt-1">

                    <label class="control-label text-right col-md-2">No. Faktur</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="noreff" name="noreff">
                   </div>
                   <label class="control-label text-right col-md-2">Bagian</label>
                   <div class="col-md-4">
                       <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian" value="INSTALASI FARMASI" readOnly>
                  </div>

                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Tgl. Pesanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgl_pesanan" name="tgl_pesanan">
                    </div>
                    <label for="nip" class="col-md-2 text-right">Tgl. Jatuh Tempo <span class="text-danger">*</span></label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tgl_jthtempo" name="tgl_jthtempo" required>
                    </div>

                </div>

            </form>
        </div>
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">List Barang</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive m-t-60">
                    <table id="tbl_brg_receive" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Satuan <br> Besar</th>
                                <th>Satuan <br> Kecil</th>
                                <th>Isi <br> Per Box</th>
                                <th>Qty Barang</th>
                                <th>Qty Retur</th>
                                <th>Harga Beli</th>
                                <th>Diskon (%)</th>
                                <th>Diskon (Rp.)</th>
                                <th>PPN (10%)</th>
                                <th>Subtotal</th>
                                <th>Harga Jual</th>
                                <th>Opsi</th>

                            </tr>
                        </thead>
                        <tbody id="tbody_brg_receive">

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <div class="form-row mt-1">
            <label for="nip" class="col-md-2 text-right">Keterangan</label>
            <div class="col-md-3">
              <input type="text" class="form-control form-control-sm" autocomplete="off" id="keterangan" name="keterangan">
            </div>
            <label class="control-label text-right col-md-2">Total Diskon</label>
            <div class="col-md-4">
                <input type="text" class="form-control form-control-sm" autocomplete="off" id="totdiskon" name="totdiskon" readOnly>
           </div>

        </div>
        <div class="form-row mt-1">
            <label for="nip" class="col-md-2 text-right">Approval <span class="text-danger">*</span></label>
            <div class="col-md-3">
              <input type="text" class="form-control form-control-sm" autocomplete="off" id="approval" name="approval" required>
            </div>
            <label for="nip" class="col-md-2 text-right">Total PPN (10%)</label>
            <div class="col-md-4">
              <input type="text" class="form-control form-control-sm" autocomplete="off" id="ppn" name="ppn" readOnly>
            </div>

        </div>
        <div class="form-row mt-1">
            <label class="control-label text-right col-md-7">Total </label>
            <div class="col-md-4">
                <input type="text" class="form-control form-control-sm" autocomplete="off" id="grandtotal" name="grandtotal" readOnly>
           </div>
        </div>
        <br>
    </div>

</div>

<div class="modal fade" id="modalBarang" tabindex="-1" role="dialog" aria-labelledby="modalBrgLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalBarangLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <form method="POST" action="#" enctype="multipart/form-data">
                @csrf
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Barang</label>
                    <div class="col-md-6">
                      <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="kdbrg" name="kdbrg" readOnly>
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmbrg" name="nmbrg" readOnly>
                    </div>
                    <div class="col-md-2">
                    <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pilih-barang">...</button>
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Harga Beli</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="hrgbeli" name="hrgbeli" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Harga Jual</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="hrgjual" name="hrgjual" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Qty</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="qty_kecil" name="qty_kecil">
                    </div>

                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success btn-add-row-brg">Tambahkan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalPemesanan" tabindex="-1" role="dialog" aria-labelledby="modalPemesananLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Pesanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_pesanan' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">No. Pemesanan</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Supplier</th>
                    <th class="text-center">Status Pesanan</th>
                    <th class="text-center">Bagian</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalListBarang" tabindex="-1" role="dialog" aria-labelledby="modalListBarangLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_barang' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Nama</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
     $(document).ready(function(){

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
        $('#tgl_pembelian').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d',
            autoclose: true
        });
        $('#tgl_pesanan').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d',
            autoclose: true
        });
        $('#tgl_jthtempo').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d',
            autoclose: true
        });


     });

     $('body').on('click', '.btn-nopesanan', function () {
        loadPemesanan();
       $('#modalPemesanan').modal('show');

    });

    function loadPemesanan()
    {
        $("#tbl_list_pesanan").dataTable().fnDestroy();
        const page_url = "{{ route('inv.receive-getpesanan') }}";

        var table = $('#tbl_list_pesanan').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET'
            },
            columns: [
                { "data": null,"sortable": false,
                            render: function (data, type, row, meta) {
                                    var result = '<a class="btn btn-success btn-sm btn-set-pesanan" href="#"\
                                    data-nopp="'+row.nopp+'"\
                                    data-tglpp="'+row.tglpp+'"\
                                    data-supplier="'+row.kdsupplier+'"\
                                    >Pilih</a>';
                                    return result;
                            }
                        },
                {data: 'nopp', name: 'nopp', orderable: true,searchable: true},
                {data: 'tglpp', name: 'tglpp', orderable: true,searchable: true},
                {data: 'supplier.nmsupplier', name: 'supplier.nmsupplier', orderable: true,searchable: true},
                {data: 'stsetuju.nmstsetuju', name: 'stsetuju.nmstsetuju', orderable: true,searchable: true},
                {data: 'bagian.nmbagian', name: 'bagian.nmbagian', orderable: true,searchable: true},
            ],
            responsive: true,
            columnDefs: [

            ],

        });
    }



    $('.btn-add-barang').click(function(){
        var jpesanan = $("#jpesanan").val();
        var supplier = $("#supplier").val();
        if(supplier == ''){
            Swal.fire("Error!", "Supplier belum dipilih", "error");
        }else if(jpesanan == ''){
            Swal.fire("Error!", "Jenis Pesanan belum dipilih", "error");
        }else{
            //$('#modalBarang').modal('show');
            //$('#InputModalBarangLabel').text('Tambah Barang');
            loadBarang();
            $('#modalListBarang').modal('show');

        }

        $('.btn-pilih-barang').click(function(){
       $('#modalListBarang').modal('show');
       loadBarang();

    });



});

var rowIdx = 0;

$('body').on('click', '.btn-set-pesanan', function () {
        var nopp = $(this).data('nopp');
        var tglpp = $(this).data('tglpp');
        var kdsupplier = $(this).data('supplier');

        $('#nopesanan').val(nopp);
        $('#tgl_pesanan').val(tglpp);
        $("#supplier").val(kdsupplier).change();
        loadDataPemesananDetail(nopp);


        $('#modalPemesanan').modal('hide');
      //  $("#jumlah").val(sumJumlah());


    });

    function loadDataPemesananDetail(nopp)
    {
        $("#tbl_brg_receive").dataTable().fnDestroy();
        const page_url = "{{ route('inv.list-ppdet') }}";

        var table = $('#tbl_brg_receive').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: page_url,
                type: 'GET',
                data:{
                  nopp : nopp
                }
            },
            pageLength: 100,
            columns: [
            //    { "render": function ( data, type, full, meta ) {
            //         return  meta.row;
            //     } },

                {data: 'barang.nmbrg', name: 'barang.nmbrg', orderable: true,searchable: true},
                {data: 'barang.jsatuan_besar.nmsatuan', name: 'barang.jsatuan_besar.nmsatuan', orderable: true,searchable: true},
                {data: 'barang.jsatuan.nmsatuan', name: 'barang.jsatuan.nmsatuan', orderable: true,searchable: true},
                {data: 'barang.jmlperbox', name: 'barang.jmlperbox', orderable: true,searchable: true},
                { "data":null,"sortable": false,
                    render: function(data,type,row,meta){
                        var result = '<input type="text" class="form-control form-control-sm qty" value="'+row.qty+'">';
                        return result;
                    }
                },
                { "data":null,"sortable": false,
                    render: function(data,type,row,meta){
                        var result = '<input type="text" class="form-control form-control-sm qtyretur" value="0">';
                        return result;
                    }
                },
                { "data":null,"sortable": false,
                    render: function(data,type,row,meta){
                        var result = '<input type="text" class="form-control form-control-sm hrgbeli" value="'+row.barang.hrgbeli+'">';
                        return result;
                    }
                },
                { "data":null,"sortable": false,
                    render: function(data,type,row,meta){
                        var result = '<input type="text" class="form-control form-control-sm diskon_persen" value="0">';
                        return result;
                    }
                },
                { "data":null,"sortable": false,
                    render: function(data,type,row,meta){
                        var result = '<input type="text" class="form-control form-control-sm diskon_rp" value="0">';
                        return result;
                    }
                },
                { "data":null,"sortable": false,
                    render: function(data,type,row,meta){
                        var result = '<input type="checkbox" class="ppn" style="opacity:inherit; position:inherit;" id="ppn" name="ppn">';
                        return result;
                    }
                },
                { "data":null,"sortable": false,
                    render: function(data,type,row,meta){
                        var subtotal = parseFloat(row.barang.hrgbeli) * parseInt(row.qty);
                        var result = '<div class="subtotal">'+subtotal+'</div>';
                        return result;
                    }
                },
                {data: 'barang.hrgjual', name: 'barang.hrgjual', orderable: true,searchable: false},
                { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-danger btn-sm btn-delete-barang row-index"\
                                                href="javascript:void(0);">Hapus</a>';
                                return result;
                         }
                    },

            ],
            responsive: true,
            columnDefs: [

            ],

        });


    }


     // Find and remove selected table rows
     $('#tbody_brg_receive').on('click', '.btn-delete-barang', function () {
         // Getting all the rows next to the row
        // containing the clicked button
        var child = $(this).closest('tr').nextAll();

        // Iterating across all the rows
        // obtained to change the index
        // child.each(function () {

        //   // Getting <tr> id.
        //   var id = $(this).attr('id');
        //   // Gets the row number from <tr> id.
        //   var dig = parseInt(id.substring(1));
        //   // Modifying row id.
        //   $(this).attr('id', `R${dig - 1}`);
        // });

        // Removing the current row.
        $(this).closest('tr').remove();

        // Decreasing total number of rows by 1.
        rowIdx--;

   //     $("#jumlah").val(sumJumlah());
        $("#totdiskon").val(sumDiskon());
        $("#grandtotal").val(sumColumn(11));
    });

    function sumColumn(index) {
        var total = 0;
        $("td:nth-child(" + index + ")").each(function() {
            total += parseInt($(this).text(), 10) || 0;
        });
        return total;
    }
    function sumDiskon() {
        var total = 0;
        $(".diskon_rp").each(function() {
            total += parseInt($(this).val(), 10) || 0;
        });
        return total;
    }
    function sumJumlah() {
        var total = 0;
        $(".subtotal").each(function() {
            total += parseInt($(this).text(), 10) || 0;
        });
        return total;
    }

    $('#tbl_brg_receive').delegate('.qty','change', function(){
        var qty = parseInt($(this).val());
		var index = $('.qty').index(this);
        var hrgbeli = $( ".hrgbeli" ).eq( index ).val();
        var diskon_rp = $( ".diskon_rp" ).eq( index ).val();
        var subtotal = (qty * parseInt(hrgbeli)) - diskon_rp;
      //  $('.subtotal_origin').eq( index ).val((qty * parseInt(hrgbeli)));
        $('.subtotal').eq( index ).text(subtotal);


        //$("#jumlah").val(sumJumlah());
        $("#totdiskon").val(sumDiskon());
        $("#grandtotal").val(sumColumn(11));

    });

    $('#tbl_brg_receive').delegate('.hrgbeli','change', function(){
       var index = $('.hrgbeli').index(this);
       $( ".diskon_rp" ).eq( index ).val(0);
       $( ".diskon_persen" ).eq( index ).val(0);
       var qty = $( ".qty" ).eq( index ).val();
        var hrgbeli = parseInt($(this).val());
        var diskon_rp = $( ".diskon_rp" ).eq( index ).val();

        var subtotal = (qty * parseInt(hrgbeli)) - diskon_rp;
      //  $('.subtotal_origin').eq( index ).val((qty * parseInt(hrgbeli)));
        $('.subtotal').eq( index ).text(subtotal);

       // $("#jumlah").val(sumJumlah());
        $("#totdiskon").val(sumDiskon());
        $("#grandtotal").val(sumColumn(11));

    });
    $('#tbl_brg_receive').delegate('.diskon_rp','change', function(){
        var diskon_rp = parseInt($(this).val());
		var index = $('.diskon_rp').index(this);
        var hrgbeli = $( ".hrgbeli" ).eq( index ).val();
        var qty = $( ".qty" ).eq( index ).val();
        var diskon_persen = (diskon_rp / (hrgbeli * qty)) * 100;
        $( ".diskon_persen" ).eq( index ).val(diskon_persen);
        var subtotal = (qty * parseInt(hrgbeli)) - diskon_rp;
        $('.subtotal').eq( index ).text(subtotal);

      //  $("#jumlah").val(sumJumlah());
        $("#totdiskon").val(sumDiskon());
        $("#grandtotal").val(sumColumn(11));

    });


    $('#tbl_brg_receive').delegate('.diskon_persen','change', function(){
        var diskon_persen = parseInt($(this).val());
		var index = $('.diskon_persen').index(this);
        var hrgbeli = $( ".hrgbeli" ).eq( index ).val();
        var qty = $( ".qty" ).eq( index ).val();
        var diskon = (qty * parseInt(hrgbeli)) * diskon_persen/100;
        $( ".diskon_rp" ).eq( index ).val(diskon);
        var subtotal = (qty * parseInt(hrgbeli)) - diskon;
        $('.subtotal').eq( index ).text(subtotal);

      //  $("#jumlah").val(sumJumlah());
        $("#totdiskon").val(sumDiskon());
        $("#grandtotal").val(sumColumn(11));

    });


    $('#tbl_brg_receive').delegate('.ppn','change', function(){
        if(this.checked) {
            //$(this).attr("checked", true);
             var diskon_persen = parseInt($(this).val());
            var index = $('.diskon_persen').index(this);
            var hrgbeli = $( ".hrgbeli" ).eq( index ).val();
            var qty = $( ".qty" ).eq( index ).val();
            var diskon = (qty * parseInt(hrgbeli)) * diskon_persen/100;
            $( ".diskon_rp" ).eq( index ).val(diskon);
            var subtotal = ((qty * parseInt(hrgbeli)) - diskon); //ppn
            $('.subtotal').eq( index ).text(subtotal);

            $("#totdiskon").val(sumDiskon());
            $("#grandtotal").val(sumColumn(11));
        }
        // var diskon_persen = parseInt($(this).val());
		// var index = $('.diskon_persen').index(this);
        // var hrgbeli = $( ".hrgbeli" ).eq( index ).val();
        // var qty = $( ".qty" ).eq( index ).val();
        // var diskon = (qty * parseInt(hrgbeli)) * diskon_persen/100;
        // $( ".diskon_rp" ).eq( index ).val(diskon);
        // var subtotal = (qty * parseInt(hrgbeli)) - diskon;
        // $('.subtotal').eq( index ).text(subtotal);

        // $("#totdiskon").val(sumDiskon());
        // $("#grandtotal").val(sumColumn(11));

    });



function loadBarang(){
    $("#tbl_list_barang").dataTable().fnDestroy();
    const page_url = "{{ route('inv.get-barang') }}";

    var table = $('#tbl_list_barang').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
              url: page_url,
              type: 'GET',
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-added-barang" href="#"\
                                data-kdbrg="'+row.kdbrg+'"\
                                data-nmbrg="'+row.nmbrg+'"\
                                data-hrgbeli="'+row.hrgbeli+'"\
                                data-hrgjual="'+row.hrgjual+'"\
                                data-nmsatuan_bsr="'+row.jsatuan_besar.nmsatuan+'"\
                                data-nmsatuan_kcl="'+row.jsatuan.nmsatuan+'"\
                                data-jmlperbox="'+row.jmlperbox+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'kdbrg', name: 'kdbrg', orderable: true,searchable: true},
            {data: 'nmbrg', name: 'nmbrg', orderable: true,searchable: true},
          ],
          responsive: true,
          columnDefs: [

          ],

      });
}



function cetakPo()
{

}

</script>
@endpush
