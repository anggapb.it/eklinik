@extends('layouts.main')

@section('page.title', 'Edit Pemesanan Barang')
@section('page.heading', 'Edit Pemesanan Barang')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Edit Pemesanan Barang</h4>
        </div>
        <div class="card-body">
            @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif
        <form id="submit_pp" method="POST" action="{{ route('inv.update-pp') }}" id="frm_pp" class="form-horizontal" enctype="multipart/form-data">
            @csrf
                <div class="button-group">
                    <button type="button" class="btn waves-effect waves-light btn-info" onclick="submitPp()">Update</button>
                    <a href="{{ route('inv.pp') }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">No pesanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nopp" name="nopp" value="{{ $pp->nopp }}" readOnly>
                    </div>

                    <label class="control-label text-right col-md-2">Supplier</label>
                    <div class="col-md-4">
                        <select class="form-control custom-select select2" name="supplier" id="supplier" tabindex="1" required>
                            <option value="">--Pilih Supplier--</option>
                            @foreach($supplier as $item)
                            <option value="{{ $item->kdsupplier }}" {{ isset($pp->supplier->kdsupplier) && $pp->supplier->kdsupplier == $item->kdsupplier ? 'selected="selected"' : '' }}>{{ $item->nmsupplier }}</option>
                              @endforeach

                          </select>
                   </div>

                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Tgl. Kirim</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglkirim" name="tglkirim" value="{{ date("d-m-Y", strtotime($pp->tglpp))  }}">
                    </div>
                    <label for="nip" class="col-md-2 text-right">Bagian</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian" value="INSTALASI FARAMSI" readOnly>
                    </div>

                </div>


                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Status Pesanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="stpesanan" name="stpesanan" value="{{ $pp->stpo->nmstpo }}" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">Keterangan</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="keterangan" name="keterangan" value="{{ $pp->keterangan }}">
                   </div>

                </div>

        <br>
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">List Barang</h4>
            </div>
            <div class="card-body">
                <div class="button-group">
                    <button type="button" class="btn waves-effect waves-light btn-primary btn-sm btn-add-barang">Tambah Barang</button>
                </div>
                <div class="table-responsive m-t-60">
                    <table id="tbl_brg_pp" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Opsi</th>
                                <th>Nama Barang</th>
                                <th>Satuan <br> Besar</th>
                                <th>Satuan <br> Kecil</th>
                                <th class="align-middle text-center">Jumlah</th>
                                <th>Isi <br> Per Box</th>
                                <th class="align-middle text-center">Qty</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_brg_pp">
                            <?php $i=0; ?>
                            @foreach($ppdet as $item)
                            <?php $i++ ?>
                            <tr id="R"{{$i}}>
                                <td><a class='btn btn-danger btn-sm btn-delete-barang row-index'
                                href='javascript:void(0);'>Hapus</a>
                                <input type='hidden' name="kdbrg[]" value="{{ $item->kdbrg }}" />
                                </td>
                                <td class="row-index text-center">
                                    {{ $item->barang->nmbrg }}
                                </td>
                                <td class="text-center">
                                    {{ $item->barang->jsatuan_besar->nmsatuan }}
                                </td>
                                <td class="text-center">
                                    {{ $item->barang->jsatuan->nmsatuan }}
                                </td>
                                <td class="align-middle text-center">
                                    <input type="text" name="jumlah[]" class="form-control form-control-sm jumlah" style="width:35%" value="{{ $item->qty_besar }}">
                                </td>
                                <td class="text-center">
                                    <input type="text" name="jml_per_box[]" class="form-control form-control-sm jml_per_box" value="{{ $item->jml_per_box }}" readOnly>
                                </td>
                                <td class="align-middle text-center">
                                    <input type="text" name="qty[]" class="form-control form-control-sm qty_brg"  value="{{ $item->qty }}" readOnly>
                                </td>
                                <td class="text-center">
                                    <input type="text" name="catatan[]" class="form-control form-control-sm catatan" value="{{ $item->catatan }}">
                                </td>
                               </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        </form>

</div>
    </div>

</div>

<div class="modal fade" id="modalListBarang" tabindex="-1" role="dialog" aria-labelledby="modalListBarangLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_barang' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Nama</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>


@endsection

@push('bottom.scripts')
<script type="text/javascript">
     $(document).ready(function(){

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
         $('#tglkirim').datepicker({
            format: 'dd-mm-yyyy',
           //startDate: '1d',
            autoclose: true
        });

        // loadData();
         $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });

});


function submitPp()
{
    $('#submit_pp').submit();
}

var rowIdx = 0;

$('body').on('click', '.btn-added-barang', function () {
        var kdbrg = $(this).data('kdbrg');
        var nmbrg = $(this).data('nmbrg');
        var nmsatuan_bsr = $(this).data('nmsatuan_bsr');
        var nmsatuan_kcl = $(this).data('nmsatuan_kcl');
        var jmlperbox = ($(this).data('jmlperbox'))?$(this).data('jmlperbox'):1;
        var hrgbeli = $(this).data('hrgbeli');
        var hrgjual = $(this).data('hrgjual');
        $('#modalListBarang').modal('hide');
        $('#tbody_brg_pp').append(`<tr id="R${++rowIdx}">
            <td><a class='btn btn-danger btn-sm btn-delete-barang row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="kdbrg[]" value="${kdbrg}" />
            </td>
            <td class="row-index text-center">
                ${nmbrg}
            </td>
            <td class="text-center">
                ${nmsatuan_bsr}
            </td>
            <td class="text-center">
                ${nmsatuan_kcl}
            </td>
            <td class="align-middle text-center">
                <input type="text" name="jumlah[]" class="form-control form-control-sm jumlah" style="width:35%"   value="1">
            </td>
            <td class="text-center">
                <input type="text" name="jml_per_box[]" class="form-control form-control-sm jml_per_box"  value="${jmlperbox}" readOnly>
            </td>
            <td class="align-middle text-center">
                <input type="text" name="qty[]" class="form-control form-control-sm qty_brg" value="1" readOnly>
            </td>
            <td class="text-center">
                <input type="text" name="catatan[]" class="form-control form-control-sm catatan">
            </td>
           </tr>`);


    });

    $('#tbody_brg_pp').on('click', '.btn-delete-barang', function () {
         // Getting all the rows next to the row
        // containing the clicked button
        var child = $(this).closest('tr').nextAll();

        // Iterating across all the rows
        // obtained to change the index
        child.each(function () {

          // Getting <tr> id.
          var id = $(this).attr('id');
          // Gets the row number from <tr> id.
          var dig = parseInt(id.substring(1));
          // Modifying row id.
          $(this).attr('id', `R${dig - 1}`);
        });

        // Removing the current row.
        $(this).closest('tr').remove();

        // Decreasing total number of rows by 1.
        rowIdx--;
    });

    $('#tbl_brg_pp').delegate('.jumlah','change', function(){
        var jml = parseInt($(this).val());
		var index = $('.jumlah').index(this);
        var jml_per_box = $( ".jml_per_box" ).eq( index ).val();
        var qty = (jml * parseInt(jml_per_box));
         $('.qty_brg').eq( index ).val(qty);
    });


function loadData()
{
    const page_url = "{{ route('inv.list-ppdet') }}";

var table = $('#tbl_brg_pp').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET',
          data:{
            nopp :  $("#nopp").val()
          }
      },
      columns: [
        {data: 'barang.nmbrg', name: 'barang.nmbrg', orderable: true,searchable: true},
        {data: 'barang.jsatuan_besar.nmsatuan', name: 'barang.jsatuan_besar.nmsatuan', orderable: true,searchable: true},
        {data: 'barang.jsatuan.nmsatuan', name: 'barang.jsatuan.nmsatuan', orderable: true,searchable: true},
        {data: 'qty_besar', name: 'qty_besar', orderable: true,searchable: true},
        {data: 'jml_per_box', name: 'jml_per_box', orderable: true,searchable: true},
        {data: 'qty', name: 'qty', orderable: true,searchable: true},
        {data: 'catatan', name: 'catatan', orderable: true,searchable: true}
      ]


  });
}

$('.btn-add-barang').click(function(){
        var supplier = $("#supplier").val();
        if(supplier == ''){
            Swal.fire("Error!", "Supplier belum dipilih", "error");
        }else{
            loadBarang();
            $('#modalListBarang').modal('show');

        }



    });


function loadBarang(){
    $("#tbl_list_barang").dataTable().fnDestroy();
    const page_url = "{{ route('inv.get-barang') }}";

    var table = $('#tbl_list_barang').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
              url: page_url,
              type: 'GET',
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-added-barang" href="#"\
                                data-kdbrg="'+row.kdbrg+'"\
                                data-nmbrg="'+row.nmbrg+'"\
                                data-nmsatuan_bsr="'+row.jsatuan_besar.nmsatuan+'"\
                                data-nmsatuan_kcl="'+row.jsatuan.nmsatuan+'"\
                                data-jmlperbox="'+row.jmlperbox+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'kdbrg', name: 'kdbrg', orderable: true,searchable: true},
            {data: 'nmbrg', name: 'nmbrg', orderable: true,searchable: true},
          ],
          responsive: true,
          columnDefs: [

          ],

      });
}




</script>
@endpush
