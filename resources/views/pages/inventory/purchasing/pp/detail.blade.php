@extends('layouts.main')

@section('page.title', 'Detail Pemesanan Barang')
@section('page.heading', 'Detail Pemesanan Barang')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Detail Pemesanan Barang</h4>
        </div>
        <div class="card-body">
            @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif
                <div class="button-group">
                    <a href="{{ route('inv.pp') }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">No pesanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nopp" name="nopp" value="{{ $pp->nopp }}" readOnly>
                    </div>

                    <label class="control-label text-right col-md-2">Supplier</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="supplier" name="supplier" value="{{ $pp->supplier->nmsupplier }}" readOnly>
                   </div>

                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Tgl. Kirim</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglkirim" name="tglkirim" value="{{ $pp->tglpp }}" readOnly>
                    </div>
                    <label for="nip" class="col-md-2 text-right">Bagian</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian" value="INSTALASI FARAMSI" readOnly>
                    </div>

                </div>


                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Status Pesanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="stpesanan" name="stpesanan" value="{{ $pp->stpo->nmstpo }}" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">Keterangan</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="keterangan" name="keterangan" value="{{ $pp->keterangan }}" readOnly>
                   </div>

                </div>

        <br>
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">List Barang</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive m-t-60">
                    <table id="tbl_brg_pp" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Satuan <br> Besar</th>
                                <th>Satuan <br> Kecil</th>
                                <th class="align-middle text-center">Jumlah</th>
                                <th>Isi <br> Per Box</th>
                                <th class="align-middle text-center">Qty</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>

</div>
    </div>

</div>



@endsection

@push('bottom.scripts')
<script type="text/javascript">
     $(document).ready(function(){

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });

         loadData();

     });


function loadData()
{
    const page_url = "{{ route('inv.list-ppdet') }}";

var table = $('#tbl_brg_pp').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET',
          data:{
            nopp :  $("#nopp").val()
          }
      },
      columns: [
        {data: 'barang.nmbrg', name: 'barang.nmbrg', orderable: true,searchable: true},
        {data: 'barang.jsatuan_besar.nmsatuan', name: 'barang.jsatuan_besar.nmsatuan', orderable: true,searchable: true},
        {data: 'barang.jsatuan.nmsatuan', name: 'barang.jsatuan.nmsatuan', orderable: true,searchable: true},
        {data: 'qty_besar', name: 'qty_besar', orderable: true,searchable: true},
        {data: 'jml_per_box', name: 'jml_per_box', orderable: true,searchable: true},
        {data: 'qty', name: 'qty', orderable: true,searchable: true},
        {data: 'catatan', name: 'catatan', orderable: true,searchable: true}
      ]


  });
}




</script>
@endpush
