@extends('layouts.main')

@section('page.title', 'Buat Pemesanan Barang')
@section('page.heading', 'Buat Pemesanan Barang')

@section('page.content')
<div class="col-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Form Pemesanan Barang</h4>
        </div>
        <div class="card-body">
            @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif
            <form method="POST" action="{{ route('inv.insert-pp') }}" id="frm_pp" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="button-group">
                    <button type="submit" class="btn waves-effect waves-light btn-info">Simpan</button>
                    <a href="{{ route('inv.pp') }}" class="btn waves-effect waves-light btn-warning">Kembali</a>
                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">No pesanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nopesanan" name="nopesanan" placeholder="Otomatis" readOnly>
                    </div>

                    <label class="control-label text-right col-md-2">Supplier</label>
                    <div class="col-md-3">
                        <select class="form-control form-control-sm select2" name="supplier" id="supplier" required>
                            <option value="">--Pilih--</option>
                                @foreach ($supplier as $row)
                                    <option value="{{ $row->kdsupplier }}">{{ $row->nmsupplier }}</option>
                                @endforeach
                          </select>
                   </div>

                </div>
                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Tgl. Kirim</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="tglkirim" name="tglkirim" required>
                    </div>
                    <label for="nip" class="col-md-2 text-right">Bagian</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="bagian" name="bagian" value="INSTALASI FARAMSI" readOnly>
                    </div>

                </div>


                <div class="form-row mt-1">
                    <label for="nip" class="col-md-2 text-right">Status Pesanan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="stpesanan" name="stpesanan" value="Waiting" readOnly>
                    </div>
                    <label class="control-label text-right col-md-2">Keterangan</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control form-control-sm" autocomplete="off" id="keterangan" name="keterangan">
                   </div>

                </div>

        <br>
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">List Barang</h4>
            </div>
            <div class="card-body">
                <div class="button-group">
                    <button type="button" class="btn waves-effect waves-light btn-primary btn-sm btn-add-barang">Tambah Barang</button>
                </div>
                <div class="table-responsive m-t-60">
                    <table id="tbl_brg_pp" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Opsi</th>
                                <th>Nama Barang</th>
                                <th>Satuan <br> Besar</th>
                                <th>Satuan <br> Kecil</th>
                                <th class="align-middle text-center">Jumlah</th>
                                <th>Isi <br> Per Box</th>
                                <th class="align-middle text-center">Qty</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_brg_pp">

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
    </div>

</div>

<div class="modal fade" id="modalBarang" tabindex="-1" role="dialog" aria-labelledby="modalBrgLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalBarangLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <form method="POST" action="#" enctype="multipart/form-data">
                @csrf
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Barang</label>
                    <div class="col-md-6">
                      <input type="hidden" class="form-control form-control-sm" autocomplete="off" id="kdbrg" name="kdbrg" readOnly>
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="nmbrg" name="nmbrg" readOnly>
                    </div>
                    <div class="col-md-2">
                    <button type="button" class="btn waves-effect waves-light btn-sm btn-success btn-pilih-barang">...</button>
                    </div>
                </div>
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Harga Beli</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="hrgbeli" name="hrgbeli" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Harga Jual</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="hrgjual" name="hrgjual" readOnly>
                    </div>

                </div>
                <div class="form-row mt-1">
                    <label for="tindakan" class="col-md-3 text-right">Qty</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control form-control-sm" autocomplete="off" id="qty_kecil" name="qty_kecil">
                    </div>

                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success btn-add-row-brg">Tambahkan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

<div class="modal fade" id="modalListBarang" tabindex="-1" role="dialog" aria-labelledby="modalListBarangLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="InputModalLabel">List Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body">
            <table class="table table-bordered" id='tbl_list_barang' width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Nama</th>
                  </tr>
                </thead>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>

      </div>
    </div>
</div>

@endsection

@push('bottom.scripts')
<script type="text/javascript">
     $(document).ready(function(){

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
        $('#tglkirim').datepicker({
            format: 'dd-mm-yyyy',
           //startDate: '1d',
            autoclose: true
        });



     });



    $('.btn-add-barang').click(function(){
        var supplier = $("#supplier").val();
        if(supplier == ''){
            Swal.fire("Error!", "Supplier belum dipilih", "error");
        }else{
            //$('#modalBarang').modal('show');
            //$('#InputModalBarangLabel').text('Tambah Barang');
            loadBarang();
            $('#modalListBarang').modal('show');

        }

        $('.btn-pilih-barang').click(function(){
       $('#modalListBarang').modal('show');
       loadBarang();

    });



});

var rowIdx = 0;

$('body').on('click', '.btn-added-barang', function () {
        var kdbrg = $(this).data('kdbrg');
        var nmbrg = $(this).data('nmbrg');
        var nmsatuan_bsr = $(this).data('nmsatuan_bsr');
        var nmsatuan_kcl = $(this).data('nmsatuan_kcl');
        var jmlperbox = ($(this).data('jmlperbox'))?$(this).data('jmlperbox'):1;
        var hrgbeli = $(this).data('hrgbeli');
        var hrgjual = $(this).data('hrgjual');
        $('#modalListBarang').modal('hide');
        $('#tbody_brg_pp').append(`<tr id="R${++rowIdx}">
            <td><a class='btn btn-danger btn-sm btn-delete-barang row-index'
            href='javascript:void(0);'>Hapus</a>
            <input type='hidden' name="kdbrg[]" value="${kdbrg}" />
            </td>
            <td class="row-index text-center">
                ${nmbrg}
            </td>
            <td class="text-center">
                ${nmsatuan_bsr}
            </td>
            <td class="text-center">
                ${nmsatuan_kcl}
            </td>
            <td class="align-middle text-center">
                <input type="text" name="jumlah[]" class="form-control form-control-sm jumlah" style="width:25%" value="1">
            </td>
            <td class="text-center">
                <input type="text" name="jml_per_box[]" class="form-control form-control-sm jml_per_box" style="width:25%" value="${jmlperbox}" readOnly>
            </td>
            <td class="align-middle text-center">
                <input type="text" name="qty[]" class="form-control form-control-sm qty_brg" style="width:30%" value="1" readOnly>
            </td>
            <td class="text-center">
                <input type="text" name="catatan[]" class="form-control form-control-sm catatan">
            </td>
           </tr>`);

        //    $("#jumlah").val(sumJumlah());
        // $("#totdiskon").val(sumDiskon());
        // $("#grandtotal").val(sumColumn(12));

    });

     // Find and remove selected table rows
     $('#tbody_brg_pp').on('click', '.btn-delete-barang', function () {
         // Getting all the rows next to the row
        // containing the clicked button
        var child = $(this).closest('tr').nextAll();

        // Iterating across all the rows
        // obtained to change the index
        child.each(function () {

          // Getting <tr> id.
          var id = $(this).attr('id');
          // Gets the row number from <tr> id.
          var dig = parseInt(id.substring(1));
          // Modifying row id.
          $(this).attr('id', `R${dig - 1}`);
        });

        // Removing the current row.
        $(this).closest('tr').remove();

        // Decreasing total number of rows by 1.
        rowIdx--;

        // $("#jumlah").val(sumJumlah());
        // $("#totdiskon").val(sumDiskon());
        // $("#grandtotal").val(sumColumn(12));
    });


    $('#tbl_brg_pp').delegate('.jumlah','change', function(){
        var jml = parseInt($(this).val());
		var index = $('.jumlah').index(this);
        var jml_per_box = $( ".jml_per_box" ).eq( index ).val();
        var qty = (jml * parseInt(jml_per_box));
         $('.qty_brg').eq( index ).val(qty);
    });





function loadBarang(){
    $("#tbl_list_barang").dataTable().fnDestroy();
    const page_url = "{{ route('inv.get-barang') }}";

    var table = $('#tbl_list_barang').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
              url: page_url,
              type: 'GET',
          },
          columns: [
            { "data": null,"sortable": false,
                        render: function (data, type, row, meta) {
                                var result = '<a class="btn btn-success btn-sm btn-added-barang" href="#"\
                                data-kdbrg="'+row.kdbrg+'"\
                                data-nmbrg="'+row.nmbrg+'"\
                                data-nmsatuan_bsr="'+row.jsatuan_besar.nmsatuan+'"\
                                data-nmsatuan_kcl="'+row.jsatuan.nmsatuan+'"\
                                data-jmlperbox="'+row.jmlperbox+'"\
                                >Pilih</a>';
                                return result;
                         }
                    },
            {data: 'kdbrg', name: 'kdbrg', orderable: true,searchable: true},
            {data: 'nmbrg', name: 'nmbrg', orderable: true,searchable: true},
          ],
          responsive: true,
          columnDefs: [

          ],

      });
}



function submitPo()
{

}

function cetakPo()
{

}

</script>
@endpush
