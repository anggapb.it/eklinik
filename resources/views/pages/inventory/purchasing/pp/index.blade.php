@extends('layouts.main')

@section('page.title', 'Pemesanan Barang')
@section('page.heading', 'Pemesanan Barang')

@section('page.content')
<div class="col-12">
    <div class="card">
        <div class="card-body">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                 {{Session::get('message')}}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
        @endif

        <div class="row">
            <a href="{{ route('inv.create-pp') }}" class="btn btn-primary mt-4 ml-4" id="btn_tambah">Tambah</a>
          </div>
            <div class="table-responsive m-t-10">

                <table id="tbl_pp" class="table table-bordered table-striped display nowrap">
                    <thead>
                        <tr>
                            <th class="align-middle text-center">Opsi</th>
                            <th>No. Pemesanan</th>
                            <th>Tgl Kirim</th>
                            <th>Bagian</th>
                            <th>Status Pesanan</th>
                            <th>Status Pembelian</th>
                            <th>Supplier</th>
                            <th>Keterangan</th>

                        </tr>
                    </thead>

                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
$(document).ready(function(){


loadData();

});

function loadData()
{
    const page_url = "{{ route('inv.list-pp') }}";

var table = $('#tbl_pp').DataTable({
   // dom: 'lrtip',
    dom: 'B<br>lfrtip',
        buttons: [
            'print','pdf','excel'
        ],
      processing: true,
      serverSide: true,
      ajax: {
          url: page_url,
          type: 'GET'
      },
      columns: [
        { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<a href="{{ url('inv/detailpp').'/' }}'+row.nopp+'" target="_blank" class="btn waves-effect waves-light btn-warning btn-sm">Detail</a>';
                    if(row.idstpo != 2){
                        result += '&nbsp;<a href="{{ url('inv/editpp').'/' }}'+row.nopp+'" class="btn waves-effect waves-light btn-info btn-sm">Ubah</a>';
                         result += '&nbsp;<a class="btn waves-effect waves-light btn-danger btn-sm" href="{{ url('inv/deletepp').'/' }}'+row.nopp+'">Hapus</a>';
                    }
                    result += '&nbsp;<a class="btn waves-effect waves-light btn-primary btn-sm" href="#">Cetak</a>';

                    return result;
                  }
              },
        {data: 'nopp', name: 'nopp', orderable: true,searchable: true},
        {data: 'tglpp', name: 'tglpp', orderable: true,searchable: true},
        {data: 'bagian.nmbagian', name: 'bagian.nmbagian', orderable: true,searchable: true},
        {data: 'stsetuju.nmstsetuju', name: 'stsetuju.nmstsetuju', orderable: true,searchable: true},
        {data: 'stpo.nmstpo', name: 'stpo.nmstpo', orderable: true,searchable: true},
        {data: 'supplier.nmsupplier', name: 'supplier.nmsupplier', orderable: true,searchable: true},
        {data: 'keterangan', name: 'keterangan', orderable: true,searchable: true},

      ],
      columnDefs: [
        { className: "dt-nowrap", "targets": [ 1 ] }
      ]


  });
}

</script>
@endpush
