@extends('layouts.main')

@section('page.title', 'Barang Bagian')
@section('page.heading', 'Barang Bagian')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            <h4 class="card-title"></h4>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                     {{Session::get('message')}}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('error')}}
                </div>
            @endif

           
              <div class="table-responsive">
                <table  id="tbl_barang_bagian" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="align-middle" scope="col">No</th>
                      <th class="align-middle" scope="col">Opsi</th>
                      <th class="align-middle" scope="col">Kode</th>
                      <th class="align-middle" scope="col">Barang</th>
                      <th class="align-middle" scope="col">Bagian</th>
                      <th class="align-middle" scope="col">Jenis <br> Barang</th>
                      <th class="align-middle" scope="col">Satuan Besar</th>
                      <th class="align-middle" scope="col">Satuan Kecil</th>
                      <th class="align-middle" scope="col"><b>Stok Sekarang</b></th>
                      <th class="align-middle" scope="col">Stok Minimal</th>  
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modal_barang_bagian" tabindex="-1" aria-labelledby="modal_label_barang_bagian" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_label_barang_bagian"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ url('inv/barang-bagian/update') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="idbagian" name="idbagian">
          <div class="modal-body">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Kode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kdbrg" name="kdbrg" readOnly>
              </div>
             </div> 
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Barang</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nmbrg" name="nmbrg" readOnly>
              </div>
            </div>
         
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Stok Sekarang</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="stoknowbagian" name="stoknowbagian" required>
              </div>
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>


@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
      loadList();

    });

    function loadList() {
        const page_url = '{{ url('inv/get-barang-bagian') }}';

        var table = $('#tbl_barang_bagian').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                // data : {usulan_id : $('#usulan_id').val()}
            },
            columns: [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                    var result = '<button class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_barang_bagian"\
                                  data-idbagian="'+row.idbagian+'" \
                                  data-kdbrg="'+row.kdbrg+'" \
                                  data-nmbrg="'+row.barang.nmbrg+'" \
                                  data-stoknowbagian="'+row.stoknowbagian+'" \
                                  onclick="edit_barang_bagian(this)">Ubah</button>';

                    return result;
                  }
              },
              {data: 'kdbrg', name: 'kdbrg'},
              {data: 'barang.nmbrg', name: 'barang.nmbrg'},
              {data: 'bagian.nmbagian', name: 'bagian.nmbagian'},
              {data: 'barang.jbarang.nmjnsbrg', name: 'barang.jbarang.nmjnsbrg'},
              {data: 'barang.jsatuan_besar.nmsatuan', name: 'barang.jsatuan_besar.nmsatuan'},
              {data: 'barang.jsatuan.nmsatuan', name: 'barang.jsatuan.nmsatuan'},
              {data: 'stoknowbagian', name: 'stoknowbagian',
                render: function (data, type, row, meta) {
                    // var result;
                    // if(row.stoknowbagian < row.stokminbagian)
                    // {
                    //     result = '<span class="btn-danger">'+row.stoknowbagian+'</span>';
                    // } else{
                    //     result = '<span class="btn-info">'+row.stoknowbagian+'</span>';
                    // }
                    var result = '<span class="btn-info">'+row.stoknowbagian+'</span>';
                    return result;
                }
              },
              {data: 'stokminbagian', name: 'stokminbagian'}   

            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }

    function edit_barang_bagian(e) {
      $("#idbagian").val($(e).data('idbagian'));
      $("#kdbrg").val($(e).data('kdbrg'));
      $("#nmbrg").val($(e).data('nmbrg'));
      $("#stoknowbagian").val($(e).data('stoknowbagian'));
    }
</script>
@endpush
