@extends('layouts.main')

@section('page.title', 'Pendaftaran')
@section('page.heading', 'Pendaftaran')

@push('top.styles')
<style>

</style>
@endpush

@section('page.content')
<div class="col-lg-12">
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Form Input Pendaftaran</h4>
        </div>
        <div class="card-body">
            <form method="POST" id="frm_reg_ri" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <div class="button-group">
                <a href="{{ route('ri.registrasi-pasien-ri') }}" class="btn waves-effect waves-light btn-secondary btn-sm">Kembali</a>
                <button type="button" class="btn waves-effect waves-light btn-info btn-sm" onclick="submitRegistrasi()">Simpan</button>
                <button type="button" class="btn waves-effect waves-light btn-danger btn-sm">Batal</button>
                <button type="button" class="btn waves-effect waves-light btn-primary btn-sm">Cari Registrasi</button>
                <button type="button" class="btn waves-effect waves-light btn-primary btn-sm">Cari Reservasi</button>
                <button type="button" class="btn waves-effect waves-light btn-success btn-sm">Cetak No Antrian</button>
                <button type="button" class="btn waves-effect waves-light btn-success btn-sm">Cetak Label</button>
            </div>
            <div class="form-row mt-1">
                <label for="nip" class="col-md-2 text-right">No RM</label>
                <div class="col-md-3">
                  <input type="text" class="form-control " autocomplete="off" id="norm" name="norm" value="{{ $norm }}" readOnly>
                </div>
                <label class="control-label text-right col-md-2">No. Registrasi</label>
                <div class="col-md-4">
                    <input type="text" class="form-control " autocomplete="off" id="noreg" name="noreg" placeholder="Otomatis" readOnly>
               </div>
            </div>
            <div class="form-row mt-1">
                <label for="nip" class="col-md-2 text-right">Nama Pasien</label>
                <div class="col-md-3">
                  <input type="text" class="form-control " autocomplete="off" id="nmpasien" name="nmpasien" value="{{ $nmpasien }}" readOnly>
                </div>
                <label class="control-label text-right col-md-2">Tgl/Jam/Shift<span class="text-danger">*</span></label>
                <div class="col-md-2">
                    <input type="text" class="form-control " autocomplete="off" id="tglreg" name="tglreg">
               </div>
                <label class="control-label text-right col-xs-1">/</label>
                <div class="col-md-1">
                    <input type="text" class="form-control " autocomplete="off" id="jamreg" name="jamreg" readOnly>
               </div>
                <label class="control-label text-right col-xs-1">/</label>
                <div class="col-md-1">
                    <input type="hidden" class="form-control " autocomplete="off" id="idshift" name="idshift">
                    <input type="text" class="form-control " autocomplete="off" id="shift" name="shift" readOnly>
               </div>
            </div>
            <div class="form-row mt-1">
                <label class="col-md-2 text-right">Jenis Kelamin</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" autocomplete="off" id="jkelamin" name="jkelamin" value="{{ $jnskelamin }}" readOnly>
                </div>
                <label class="col-md-2 text-right">Unit Pelayanan<span class="text-danger">*</span></label>
                <div class="col-md-3">
                    <select class="form-control select2" name="idbagian" id="idbagian">
                        <option value="">--Pilih--</option>
                        @foreach ($bagian as $row)
                            <option value="{{ $row->idbagian }}">{{ $row->nmbagian }}</option>
                        @endforeach
                      </select>
                </div>
            </div>
            <div class="form-row mt-1">
                <label class="col-md-2 text-right">Penjamin<span class="text-danger">*</span></label>
                <div class="col-md-3">
                    <select class="form-control select2  " name="idpenjamin" id="idpenjamin">

                        @foreach ($penjamin as $row)
                            <option value="{{ $row->idpenjamin }}">{{ $row->nmpenjamin }}</option>
                        @endforeach
                      </select>
                </div>
                <label class="col-md-2 text-right">Dokter<span class="text-danger">*</span></label>
                <div class="col-md-3">
                    <select class="form-control select2" name="iddokter" id="iddokter">
                        <option value="">--Pilih--</option>
                        @foreach ($dokter_praktek as $row)
                            <option value="{{ $row->iddokter }}">{{ $row->nmdoktergelar }}</option>
                        @endforeach
                      </select>
                </div>
            </div>
            <div class="form-row mt-1">
                <label class="col-md-2 text-right">Cara Datang<span class="text-danger">*</span></label>
                <div class="col-md-3">
                    <select class="form-control " name="idcaradatang" id="idcaradatang">
                        <option value="">--Pilih--</option>
                        @foreach ($caradatang as $row)
                            <option value="{{ $row->idcaradatang }}">{{ $row->nmcaradatang }}</option>
                        @endforeach
                      </select>
                </div>
                <label class="col-md-2 text-right">Jml. Pasien Antri</label>
                <div class="col-md-3">
                    <input type="text" class="form-control " autocomplete="off" id="jml_pasien_antri" name="jml_pasien_antri" readOnly>
                </div>
            </div>
            <div class="form-row mt-1">
                <label class="col-md-2 text-right">Catatan Diagnosa</label>
                <div class="col-md-3">
                    <input type="text" class="form-control " autocomplete="off" id="catatan_regis" name="catatan_regis">
                </div>
                <label class="col-md-2 text-right">No. Antrian</label>
                <div class="col-md-3">
                    <input type="text" class="form-control " autocomplete="off" id="noantrian" name="noantrian" readOnly>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">
    $.fn.ready(function() {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
        $('.select2').select2({ width: '100%',allowClear: true, placeholder :'--Pilih--' });
        $('#tglreg').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d',
            autoclose: true
        });
        $('#tglreg').datepicker('update', new Date());
        loadShift();
    });

    function loadShift(){
        var currentdate = new Date();
        var jamreg = currentdate.getHours() + ":"
                + currentdate.getMinutes();
        $.ajax({
            type: "GET",
            url: "{{ route('api.get-shift') }}",
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function(result){
                $("#idshift").val(result.data.idshift);
                $("#shift").val(result.data.nmshift);
                $("#jamreg").val(jamreg);
            } ,error: function(xhr, error) {
                alert(error);
            },

        });
    }

    function submitRegistrasi()
    {
        var idcaradatang = $('#idcaradatang').val();
        var bagian = $('#idbagian').val();
        var dokter = $('#iddokter').val();

        if(idcaradatang == ''){
            Swal.fire("Error!", 'Cara datang harus di isi', "error");
        }else if(bagian == ''){
            Swal.fire("Error!", 'Unit pelayanan harus di isi', "error");
        }else if(dokter == ''){
            Swal.fire("Error!", 'Dokter harus di isi', "error");
        } else{
                var form_data = new FormData();
                form_data.append("norm", $('#norm').val());
                form_data.append("nmpasien", $('#nmpasien').val());
                form_data.append("idpenjamin", $('#idpenjamin').val());
                form_data.append("idbagian", $('#idbagian').val());
                form_data.append("idcaradatang", $('#idcaradatang').val());
                form_data.append("catatan", $('#catatan_regis').val());
                form_data.append("idshift", $('#idshift').val());
                form_data.append("iddokter", $('#iddokter').val());
                form_data.append("noreg", $('#noreg').val());
                form_data.append("tglreg", $('#tglreg').val());
                form_data.append("jamreg", $('#jamreg').val());

                $.ajax({
                type: "POST",
                url: "{{ route('ri.store') }}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: form_data,
                dataType: "json",
                contentType: false,
                cache : false,
                processData : false,
                success: function(result){
                    if(result.message != "") {
                        Swal.fire("Success!", result.message, "success");
                        $("#noreg").val(result.reg.noreg);
                    } else {
                        Swal.fire("Error!", result.error, "error");
                    }
                } ,error: function(xhr, status, error) {
                    alert(error);
                },

            });
        }

    }

</script>
@endpush
