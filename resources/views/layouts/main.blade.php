<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @stack('page.meta')

    {{-- <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png"> --}}

    @hasSection('page.title')
    <title>@yield('page.title') | E-KLINIK</title>
    @else
    <title>E-KLINIK</title>
    @endif

    {!! cache_assets([
        'public/assets/plugins/bootstrap/css/bootstrap.min.css',
        'public/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css',
        'public/assets/plugins/sweetalert2/sweetalert2.min.css',
        'public/assets/plugins/select2/dist/css/select2.min.css',
        'public/assets/css/style-side.css',
        'public/assets/css/colors-side/green.css',
        'public/assets/css/iziToast.min.css',
    ], 'css') !!}
    <!-- Bootstrap Core CSS -->
    {{-- <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet"> --}}
    @stack('top.styles')
</head>

<body class="fix-header card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            E-KLINIK
                            <!--img src="../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />

                            <! Light Logo icon -->
                        </b>
                            <!--img src="../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <!--img src="../assets/images/logo-text.png" alt="homepage" class="dark-logo" /!-->
                         <!-- Light Logo text -->
                         <!--img src="../assets/images/logo-light-text.png" class="light-logo" alt="homepage" /!--></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item">
                            <a
                                class="
                                    nav-link nav-toggler hidden-md-up text-muted
                                    waves-effect waves-dark
                                "
                                href="javascript:void(0)"
                            >
                                @icon('mdi', 'menu')
                            </a>
                        </li>
                        <li class="nav-item m-l-10">
                            <a
                                class="
                                    nav-link sidebartoggler hidden-sm-down text-muted
                                    waves-effect waves-dark
                                "
                                href="javascript:void(0)"
                            >
                                @icon('ti', 'menu')
                            </a>
                        </li>
                        {{-- <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li> --}}
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>

                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-us"></i></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up"> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-in"></i> India</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> China</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> Dutch</a> </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/1.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="../assets/images/users/1.jpg" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Uwa Tere</h4>
                                                <p class="text-muted">varun@gmail.com</p><a href="pages-profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <!--li role="separator" class="divider"></li!-->
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li!-->
                                    <!--li><a href="#"><i class="ti-wallet"></i> My Balance</a></li!-->
                                    <!--li><a href="#"><i class="ti-email"></i> Inbox</a></li!-->
                                    <!--li role="separator" class="divider"></li!-->
                                    <!--li><a href="#"><i class="ti-settings"></i> Account Setting</a></li!-->
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('layouts.sidemenu-left')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">
                        @yield('page.heading')
                    </h3>
                </div>


            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    @yield('page.content')

                </div>
            </div>
            <footer class="footer">
                2021 © OKSIP
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    {!! cache_assets([
        'public/assets/plugins/jquery/jquery.min.js',
        'public/assets/plugins/bootstrap/js/popper.min.js',
        'public/assets/plugins/bootstrap/js/bootstrap.min.js',
        'public/assets/js/jquery.slimscroll.js',
        'public/assets/js/waves.js',
        'public/assets/js/sidebarmenu-side.js',
        'public/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
        'public/assets/plugins/sparkline/jquery.sparkline.min.js',
        'public/assets/plugins/select2/dist/js/select2.full.min.js',
        'public/assets/plugins/peity/jquery.peity.min.js'
    ], 'js') !!}

     {{-- Custom JavaScript --}}
     {!! cache_assets([
        'public/assets/js/custom-side.js',
        'public/assets/plugins/sweetalert2/sweetalert2.all.min.js',
        'public/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
        'public/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.id.min.js',
        'public/assets/js/vue'
            . (app()->environment('production') ? '.min' : '' )
            . '.js',
        'public/assets/js/moment.min.js',
        'public/assets/js/moment-locale-id.min.js',
        'public/assets/js/iziToast.min.js',
        'public/assets/js/js.cookie.js',
        'public/assets/plugins/datatables/jquery.dataTables.min.js',
        'public/assets/plugins/socket.io-client/dist/socket.io.js',

    ], 'js') !!}

    {{-- start - This is for export functionality only --}}
    {!! cache_assets([
        'public/vendor/datatables/1.2.2/buttons/js/dataTables.buttons.min.js',
        'public/vendor/datatables/1.2.2/buttons/js/buttons.flash.min.js',
        'public/vendor/jszip/2.5.0/jszip.min.js',
        'public/vendor/pdfmake8/0.1.1/build/pdfmake.min.js',
        'public/vendor/pdfmake8/0.1.1/build/vfs_fonts.js',
        'public/vendor/datatables/1.2.2/buttons/js/buttons.html5.min.js',
        'public/vendor/datatables/1.2.2/buttons/js/buttons.print.min.js',
    ], 'js') !!}    
    <!--Custom JavaScript -->
    @stack('bottom.scripts')
</body>

</html>
