@php
    $user = auth()->user();
@endphp
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->

        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                @hasanyrole('Super Admin')
                 <li class="nav-devider"></li>
                 <li>
                     <a href="{{ route('dashboard') }}">
                        @icon('mdi', 'gauge')

                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"
                    >
                        <span class="hide-menu">Master</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Kelompok</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('master.bidang-perawatan') }}">Bidang Perawatan</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.cara-bayar') }}">Cara Bayar</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.cara-datang') }}">Cara Datang</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.cara-keluar') }}">Cara Keluar</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.golongan-darah') }}">Golongan Darah</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.hubungan-keluarga') }}">Hubungan Keluarga</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.jam-praktek') }}">Jam Praktek</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.jenis-barang') }}">Jenis Barang</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.jenis-identitas') }}">Jenis Identitas</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.jenis-kelamin') }}">Jenis Kelamin</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.jenis-pembayaran') }}">Jenis Pembayaran</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.jenis-penjamin') }}">Jenis Penjamin</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.jenis-tenaga-medis') }}">Jenis Tenaga Medis</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.kelas-tarif') }}">Kelas Tarif</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.kelompok-barang') }}">Kelompok Barang</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.pekerjaan') }}">Pekerjaan</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.pendidikan') }}">Pendidikan</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.satuan') }}">Satuan</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.shift') }}">Shift</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.spesialisasi-dokter') }}">Spesialisasi Dokter</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-dokter') }}">Status Dokter</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-keluar') }}">Status Keluar</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-pasien') }}">Status Pasien</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-pelayanan') }}">Status Pelayanan</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-perkawinan') }}">Status Perkawinan</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-pesanan') }}">Status Pesanan</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-posisi-pasien') }}">Status Posisi Pasien</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.status-stok-opname') }}">Status Stok Opname</a>
                                </li>
                                  <li>
                                    <a href="{{ route('master.syarat-pembayaran') }}">Syarat Pembayaran</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Data</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('master.bagian') }}">Bagian</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.bank') }}">Bank</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.barang') }}">Barang</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.daerah') }}">Daerah</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.pabrik') }}">Pabrik</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.kamar') }}">Kamar</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.master-template-obat') }}">Master Template Obat</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.master-template-pelayanan') }}">Master Template Pelayanan</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.pelayanan') }}">Pelayanan</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.penjamin') }}">Penjamin</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.penyakit') }}">Penyakit</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.supplier') }}">Supplier</a>
                                </li>
                                <li>
                                    <a href="{{ route('master.dokter') }}">Dokter</a>
                                </li>
                             </ul>
                        </li>
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Pengaturan</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('master.jadwal-praktek') }}">Jadwal Praktek</a>
                                </li>
                             </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    {{-- <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">@icon('fa/regular', 'address-card')Rawat Jalan</a> --}}
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">Rawat Jalan</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('rj.registrasi-pasien') }}">Pendaftaran</a></li>
                        <li><a href="{{ route('rj.transaksi-pasien') }}">Transaksi</a></li>
                        <li><a href="index3.html">Reservasi</a></li>
                    </ul>
                 </li>
                 <li>
                    {{-- <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">@icon('fa/regular', 'address-card')Rawat Jalan</a> --}}
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">Rawat Inap</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('ri.registrasi-pasien-ri') }}">Pendaftaran</a></li>
                        <li><a href="{{ route('ri.input-tindakan-pasien') }}">Input Tindakan</a></li>
                        <li><a href="index3.html">Akomodasi</a></li>
                    </ul>
                 </li>
                <li>
                    {{-- <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">@icon('fa/regular', 'address-card')Rawat Jalan</a> --}}
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">Rekam Medis</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="#">Pemeriksaan Pasien</a></li>
                    </ul>
                 </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">Farmasi</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="#">Farmasi Rawat Jalan </a></li>
                    </ul>
                 </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">Kasir</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('kasir.index') }}">Pembayaran Transaksi </a></li>
                    </ul>
                 </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">Inventory</a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Purchasing</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('laporan.daftar-pasien') }}">Purchase Order</a>
                                </li>
                                <li>
                                    <a href="{{ route('inv.pp') }}">Pemesanan Barang</a>
                                </li>
                                <li>
                                    <a href="{{ route('inv.receive') }}">Penerimaan Barang</a>
                                </li>
                             </ul>
                        </li>
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Logistik</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('inv.barang-bagian') }}">Barang Bagian</a>
                                </li>
                             </ul>
                        </li>
                    </ul>
                  </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">Laporan</a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Lap. Registrasi</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('laporan.daftar-pasien') }}">Daftar Pasien</a>
                                </li>
                                <li>
                                    <a href="{{ route('laporan.kunjungan-pasien-rj-per-poliklinik') }}">Kunjungan Per Poliklinik</a>
                                </li>
                             </ul>   
                        </li>
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Lap. Rekam Medis</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('laporan.penyakit-terbanyak-rawat-jalan') }}">Penyakit Terbanyak Rawat Jalan</a>
                                </li>
                             </ul>   
                        </li>

                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Lap. Persediaan</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('laporan.daftar-barang') }}">Daftar Barang</a>
                                </li>
                                <li>
                                    <a href="{{ route('laporan.daftar-supplier') }}">Daftar Supplier</a>
                                </li>
                                 <li>
                                    <a href="{{ route('laporan.distribusi-barang') }}">Distribusi Barang</a>
                                </li>
                                <li>
                                    <a href="{{ route('laporan.pembuatan-po-per-periode') }}">Pembuatan PO Per Periode</a>
                                </li>
                             </ul>   
                        </li>
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Lap. Farmasi</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('laporan.obat-dokter') }}">Obat Dokter</a>
                                </li>
                             </ul>   
                        </li>
                        <li>
                            <a
                            class="has-arrow waves-effect waves-dark"
                            href="#"
                            aria-expanded="false"
                            >
                            <span class="hide-menu">Lap. Keuangan</span>
                            </a>
                             <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ route('laporan.penerimaan-setoran-kasir') }}">Penerimaan Setoran Kasir</a>
                                </li>
                             </ul>   
                        </li>

                    </ul> 

                 </li>
                 
                @endhasanyrole




            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
