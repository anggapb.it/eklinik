@extends('layouts.main')

@section('page.title', 'Dashboard')
@section('page.heading', 'Dashboard')

@section('page.content')
<div class="col-12">
    <div class="card">

        <div class="card-body">
            This is some text within a card block.
        </div>
    </div>
</div>
@endsection

@push('bottom.scripts')
<script type="text/javascript">

</script>
@endpush
