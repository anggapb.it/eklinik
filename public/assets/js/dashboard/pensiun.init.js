$(document).ready(function() {
    window.base_url = document.querySelector('meta[name="base-url"]').getAttribute('content');
    window.csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    axios.defaults.baseURL = window.base_url;
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common['X-CSRF-TOKEN'] = window.csrf_token;

    getPegawaiPensiun();
    getRiwayatPensiunPerBulan();
    getPresiksiPensiunStruktural();
    getPresiksiPensiunKepalaSekolah();
    getPresiksiPensiunFungsional();
    getPresiksiPensiunPelaksana();

    async function getPegawaiPensiun() {
        try {
            const response = await axios.get('/api/dashboard/pensiun/jumlah-jenis-pensiun-satu-tahun');
            const data = response.data.data;
            let total = data[0].total + data[1].total + data[2].total;

            $('#txtPegawaiPensiunBUP').text(data[0].total);
            $('#txtPegawaiPensiunMeninggal').text(data[1].total);
            $('#txtPegawaiPensiunPindah').text(data[2].total);

            $('#txtPegawaiPensiunBUPPersen').text(getPercentage(data[0].total, total)+' %');
            $('#txtPegawaiPensiunMeninggalPersen').text(getPercentage(data[1].total, total)+' %');
            $('#txtPegawaiPensiunPindahPersen').text(getPercentage(data[2].total, total)+' %');

            configPegawaiPensiun.data.datasets[0].data = [data[0].total, data[1].total, data[2].total];
            piePegawaiPensiun.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getRiwayatPensiunPerBulan() {
        try {
            const response = await axios.get('/api/dashboard/pensiun/jumlah-jenis-pensiun-perbulan');
            const data = response.data.data;
            let table = $('#tblRiwayatPensiun1Tahun tbody');
            table.empty();
            
            let table_value = []; let labels = []; let bup_value = []; let meninggal_value = []; let pindah_value = [];
            for (const [tahun, v_tahun] of Object.entries(data)) {
                for(const [bulan, v_bulan] of Object.entries(v_tahun)){
                    let total = 0;
                    for(const [id, value] of Object.entries(v_bulan)){
                        if(id==1){ bup_value.push(value); }
                        if(id==2){ meninggal_value.push(value); }
                        if(id==6){ pindah_value.push(value); }
                        total += value;
                    }
                    labels.push(window.months[parseInt(bulan)]+' '+tahun);
                    table_value.push([tahun, window.months[parseInt(bulan)], total]);
                    row = "<tr><td class='small p-t-0 p-b-5'>"+tahun+"</td><td class='small p-t-0 p-b-5'>"+window.months[parseInt(bulan)]+"</td><td class='small p-t-0 p-b-5'>"+total+"</td></tr>";
                    table.append(row);
                }
            }
            configRiwayatPensiunPerbulan.data.labels = labels;
            configRiwayatPensiunPerbulan.data.datasets[0].data = bup_value;
            configRiwayatPensiunPerbulan.data.datasets[1].data = meninggal_value;
            configRiwayatPensiunPerbulan.data.datasets[2].data = pindah_value;
            barRiwayatPensiunPerbulan.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPresiksiPensiunStruktural() {
        try {
            const response = await axios.get('/api/dashboard/pensiun/prediksi-pensiun-struktural');
            const data = response.data.data;

            labels = []; datasets = [];
            for(const[key, value] of Object.entries(data)){
                labels.push(window.months[value.pensiun_bulan]+' '+value.pensiun_tahun);
                datasets.push(value.pensiun_total);
            }

            configPresiksiPensiunStruktural.data.labels = labels;
            configPresiksiPensiunStruktural.data.datasets[0].data = datasets;
            barPresiksiPensiunStruktural.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPresiksiPensiunKepalaSekolah() {
        try {
            const response = await axios.get('/api/dashboard/pensiun/prediksi-pensiun-kepala-sekolah');
            const data = response.data.data;

            labels = []; datasets = [];
            for(const[key, value] of Object.entries(data)){
                labels.push(window.months[value.pensiun_bulan]+' '+value.pensiun_tahun);
                datasets.push(value.pensiun_total);
            }

            configPresiksiPensiunKepalaSekolah.data.labels = labels;
            configPresiksiPensiunKepalaSekolah.data.datasets[0].data = datasets;
            barPresiksiPensiunKepalaSekolah.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPresiksiPensiunFungsional() {
        try {
            const response = await axios.get('/api/dashboard/pensiun/prediksi-pensiun-fungsional');
            const data = response.data.data;

            labels = []; datasets = [];
            for(const[key, value] of Object.entries(data)){
                labels.push(window.months[value.pensiun_bulan]+' '+value.pensiun_tahun);
                datasets.push(value.pensiun_total);
            }

            configPresiksiPensiunFungsional.data.labels = labels;
            configPresiksiPensiunFungsional.data.datasets[0].data = datasets;
            barPresiksiPensiunFungsional.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPresiksiPensiunPelaksana() {
        try {
            const response = await axios.get('/api/dashboard/pensiun/prediksi-pensiun-pelaksana');
            const data = response.data.data;

            labels = []; datasets = [];
            for(const[key, value] of Object.entries(data)){
                labels.push(window.months[value.pensiun_bulan]+' '+value.pensiun_tahun);
                datasets.push(value.pensiun_total);
            }

            configPresiksiPensiunPelaksana.data.labels = labels;
            configPresiksiPensiunPelaksana.data.datasets[0].data = datasets;
            barPresiksiPensiunPelaksana.update();
        } catch (error) {
            console.error(error);
        }
    }

    var configPegawaiPensiun = {
        type: 'pie',
        data: {
            datasets: [{
                data: [10,10,10],
                backgroundColor: ['#26dad2', '#ef5350', '#ffb22b'],
                label: 'Total'
            }],
            labels: ['BUP', 'Meninggal', 'Pindah']
        },
        options: {
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return data.labels[index]+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
        }
    };
    var ctxPegawaiPensiun = document.getElementById('chartJenisPensiun1Tahun');
    piePegawaiPensiun = new Chart(ctxPegawaiPensiun, configPegawaiPensiun);
    piePegawaiPensiun.update();

    var configRiwayatPensiunPerbulan = {
        type: 'bar', 
        data: {
            labels: ['label 1'],
            datasets: [{
                label : 'BUP', backgroundColor : '#26dad2', data : [],
            },{
                label : 'Meninggal', backgroundColor : '#ef5350', data : [],
            },{
                label : 'Pindah', backgroundColor : '#ffb22b',data : [],
            }]
        }, 
        options: {
            hover: { mode: 'nearest', intersect: false },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var value = data.datasets[datasetIndex].data[index];
                        var sumValues = 0;
                        for(var i=0; i<data.datasets.length; i++){
                            sumValues += data.datasets[i].data[index];
                        }
                        var percentage = ((value / sumValues) * 100).toFixed(2);
                        return label+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
            legend : {
                position : 'top',
                labels: { fontStyle: 'bold', }
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    stacked: true,
                }]
            },
        }
    };
    var ctxRiwayatPensiunPerbulan = document.getElementById('chartRiwayatPensiun1Tahun');
    barRiwayatPensiunPerbulan = new Chart(ctxRiwayatPensiunPerbulan, configRiwayatPensiunPerbulan);
    barRiwayatPensiunPerbulan.update();

    var configPresiksiPensiunStruktural = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai Pensiun', backgroundColor: '#26dad2', data: []
            }]
        },
        options: {
            legend: {
                display: true,
                position: 'top',
            },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: { beginAtZero: true }
                }]
            },
            hover: { mode: 'nearest', intersect: false },
            tooltips: { intersect: false },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                page='prediksi-pensiun-struktural';
                status = array[0]._chart.data.labels[array[0]._index].toLowerCase();
                window.location = '/dashboard/pensiun/detail?page='+page+'&status='+status;
            }
        },
    };
    var ctxPresiksiPensiunStruktural = document.getElementById('chartPrediksiPensiunStruktural');
    barPresiksiPensiunStruktural = new Chart(ctxPresiksiPensiunStruktural, configPresiksiPensiunStruktural);
    barPresiksiPensiunStruktural.update();

    var configPresiksiPensiunKepalaSekolah = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai Pensiun', backgroundColor: '#26dad2', data: []
            }]
        },
        options: {
            legend: {
                display: true,
                position: 'top',
            },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: { beginAtZero: true }
                }]
            },
            hover: { mode: 'nearest', intersect: false },
            tooltips: { intersect: false },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                page='prediksi-pensiun-kepala-sekolah';
                status = array[0]._chart.data.labels[array[0]._index].toLowerCase();
                window.location = '/dashboard/pensiun/detail?page='+page+'&status='+status;
            }
        },
    };
    var ctxPresiksiPensiunKepalaSekolah = document.getElementById('chartPrediksiPensiunKepalaSekolah');
    barPresiksiPensiunKepalaSekolah = new Chart(ctxPresiksiPensiunKepalaSekolah, configPresiksiPensiunKepalaSekolah);
    barPresiksiPensiunKepalaSekolah.update();

    var configPresiksiPensiunFungsional = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai Pensiun', backgroundColor: '#26dad2', data: []
            }]
        },
        options: {
            legend: {
                display: true,
                position: 'top',
            },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: { beginAtZero: true }
                }]
            },
            hover: { mode: 'nearest', intersect: false },
            tooltips: { intersect: false },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                page='prediksi-pensiun-fungsional';
                status = array[0]._chart.data.labels[array[0]._index].toLowerCase();
                window.location = '/dashboard/pensiun/detail?page='+page+'&status='+status;
            }
        },
    };
    var ctxPresiksiPensiunFungsional = document.getElementById('chartPrediksiPensiunFungsional');
    barPresiksiPensiunFungsional = new Chart(ctxPresiksiPensiunFungsional, configPresiksiPensiunFungsional);
    barPresiksiPensiunFungsional.update();

    var configPresiksiPensiunPelaksana = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai Pensiun', backgroundColor: '#26dad2', data: []
            }]
        },
        options: {
            legend: {
                display: true,
                position: 'top',
            },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: { beginAtZero: true }
                }]
            },
            hover: { mode: 'nearest', intersect: false },
            tooltips: { intersect: false },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                page='prediksi-pensiun-pelaksana';
                status = array[0]._chart.data.labels[array[0]._index].toLowerCase();
                window.location = '/dashboard/pensiun/detail?page='+page+'&status='+status;
            }
        },
    };
    var ctxPresiksiPensiunPelaksana = document.getElementById('chartPrediksiPensiunPelaksana');
    barPresiksiPensiunPelaksana = new Chart(ctxPresiksiPensiunPelaksana, configPresiksiPensiunPelaksana);
    barPresiksiPensiunPelaksana.update();
});