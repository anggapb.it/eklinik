$(document).ready(function() {
    window.base_url = document.querySelector('meta[name="base-url"]').getAttribute('content');
    window.csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    var url = document.querySelector('meta[name="ajax-url"]').getAttribute('content');
    var detailTable = $('#detailTable').DataTable({
        pageLength: 5,
        lengthMenu: [5, 10, 15, 20, 30, 50],
        ajax: url,
        columns: [
            {data: 'satuan_kerja_nama'},
            {data: 'unit_kerja_nama'},
            {data: 'jabatan_nama'},
            {data: 'nama'},
        ],
        columnDefs: [
            {
                targets: [1, 2, 3],
                orderable: false,
            }
        ],
        order: [[0, 'asc']],
    });

    $('#skpd_select').on('change', function(e){
        var satuan_kerja_id = $('#skpd_select option:selected').val();
        if(satuan_kerja_id == 0){
            url = document.querySelector('meta[name="ajax-url"]').getAttribute('content');
            detailTable.ajax.url(url).load();
        }else{
            url = document.querySelector('meta[name="ajax-url"]').getAttribute('content');
            url += '&satuan_kerja_id='+satuan_kerja_id;
            detailTable.ajax.url(url).load();
        }
    });
});