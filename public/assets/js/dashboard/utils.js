window.months = [
    ''
    , 'Januari'
    , 'Februari'
    , 'Maret'
    , 'April'
    , 'Mei'
    , 'Juni'
    , 'Juli'
    , 'Agustus'
    , 'September'
    , 'Oktober'
    , 'November'
    , 'Desember'
]

function getPercentage(value, total){
    return ((value/total)*100).toFixed(2);
}