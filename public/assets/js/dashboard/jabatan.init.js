$(document).ready(function() {
    window.base_url = document.querySelector('meta[name="base-url"]').getAttribute('content');
    window.csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    axios.defaults.baseURL = window.base_url;
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common['X-CSRF-TOKEN'] = window.csrf_token;

    $('#golongan_select').on('change', function(){
        golongan = $('#golongan_select option:selected').val();
        getPegawaiJenisJabatanDanGolongan(golongan);
    });

    getJenisJabatan();
    getFormasiJabatanEselon();
    getFormasiJabatanNonEselon();
    getMasaKerjaJabatanStruktural();
    getJabatanStrukturalEselon();
    getPegawaiJenisJabatanDanGolongan('IV');

    async function getJenisJabatan() {
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-jabatan');
            const data = response.data.data[0];
            $('#txtJabatanStruktural').text(data.pegawai_struktural);
            $('#txtJabatanFungsionalTertentu').text(data.pegawai_fungsional_tertentu);
            $('#txtJabatanFungsionalUmum').text(data.pegawai_fungsional_umum);
            configJenisJabatan.data.datasets[0].data = [data.pegawai_struktural, data.pegawai_fungsional_tertentu, data.pegawai_fungsional_umum];
            pieJenisJabatan.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getFormasiJabatanEselon() {
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-formasi-eselon');
            const data = response.data.data[0];
            $('#txtFormasiJabatanEselonTerisi').text(data.total_formasi_terisi);
            $('#txtFormasiJabatanEselonKosong').text(data.total_formasi_kosong);
            configFormasiJabatanEselon.data.datasets[0].data = [data.total_formasi_terisi, data.total_formasi_kosong];
            pieFormasiJabatanEselon.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getFormasiJabatanNonEselon() {
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-formasi-non-eselon');
            const data = response.data.data[0];
            $('#txtFormasiJabatanNonEselonTerisi').text(data.total_formasi_terisi);
            $('#txtFormasiJabatanNonEselonKosong').text(data.total_formasi_kosong);
            configFormasiJabatanNonEselon.data.datasets[0].data = [data.total_formasi_terisi, data.total_formasi_kosong];
            pieFormasiJabatanNonEselon.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getMasaKerjaJabatanStruktural() {
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-masa-jabatan-struktural');
            const data = response.data.data;
            let table = $('#tblMasaKerjaJabatanStruktural tbody');
            table.empty();

            let labels = []; let datasets = [];
            for (const [index, tmp] of Object.entries(data)) {
                for (const [key, value] of Object.entries(tmp)) {
                    let row = "<tr><td class='small p-t-0 p-b-5'>"+key+" Tahun </td><td class='small p-t-0 p-b-5'>"+value+"</td></tr>";
                    table.append(row); labels.push(key); datasets.push(value);
                }
            }

            configMasaKerjaJabatanStruktural.data.labels = labels;
            configMasaKerjaJabatanStruktural.data.datasets[0].data = datasets;
            barMasaKerjaJabatanStruktural.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getJabatanStrukturalEselon(){
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-eselon');
            const data = response.data.data;

            let labels = []; let datasets = [];
            for (const [key, value] of Object.entries(data)) {
                if(value.eselon == 'II'){ $('#txtJabatanEselonII').text(value.total); }
                if(value.eselon == 'III'){ $('#txtJabatanEselonIII').text(value.total); }
                if(value.eselon == 'IV'){ $('#txtJabatanEselonIV').text(value.total); }
                labels.push(value.eselon); datasets.push(value.total);
            }

            configJabatanStrukturalEselon.data.datasets[0].labels = labels;
            configJabatanStrukturalEselon.data.datasets[0].data = datasets;
            pieJabatanStrukturalEselon.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPegawaiJenisJabatanDanGolongan(golongan = 'IV') {
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-jenis-jabatan-dan-golongan', { params: { golongan: golongan } });
            const data = response.data.data;
            let table = $('#tblPegawaiJenisJabatanGolongan tbody');
            table.empty();

            let labels = []; let label_rows = []; let data_rows = []; let datasets = [];
            let dataColors = ['#26dad2', '#5c4ac7', '#117a8b', '#028ee1', '#ffb22b', '#b21f2d'];

            for (const [jenis_jabatan, data_golongan] of Object.entries(data)) {
                labels.push(jenis_jabatan); rows = []; label_rows = [];
                for (const [golongan, value] of Object.entries(data_golongan)) {
                    rows.push(value);
                    label_rows.push(golongan);
                }
                data_rows.push(rows);
            }

            for (let i=0; i<label_rows.length; i++) {
                tmp = { label: '', backgroundColor: '', data: [] };
                for (let j=0; j<labels.length; j++) {
                    tmp.data.push(data_rows[j][i]);
                }
                table.append(createRowJenisJabatanDanGolongan(label_rows[i], tmp.data));
                tmp.label = label_rows[i];
                tmp.backgroundColor = dataColors[i];
                datasets.push(tmp);
            }

            configPegawaiJenisJabatanDanGolongan.data.labels = labels;
            configPegawaiJenisJabatanDanGolongan.data.datasets = datasets;
            barPegawaiJenisJabatanDanGolongan.update();
        } catch (error) {
            console.error(error);
        }
    }

    function createRowJenisJabatanDanGolongan(label, values) {
        row = "<tr><td class='small p-t-0 p-b-5'>"+label+"</td>"; 
        for (let i=0; i<values.length; i++) {
            row += "<td class='small p-t-0 p-b-5'>"+values[i]+"</td>";
        }
        return row += "</tr>";
    }

    var configJenisJabatan = {
        type: 'pie',
        data: {
            datasets: [{
                data: [10,10,10],
                backgroundColor: ['#26dad2', '#ffb22b', '#ef5350'],
                label: 'Total'
            }],
            labels: ['Struktural', 'Fungsional Tertentu', 'Fungsional Umum']
        },
        options: {
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return data.labels[index]+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
        }
    };
    var ctxJenisJabatan = document.getElementById('chartJenisJabatan');
    pieJenisJabatan = new Chart(ctxJenisJabatan, configJenisJabatan);
    pieJenisJabatan.update();

    var configFormasiJabatanEselon = {
        type: 'pie',
        data: {
            datasets: [{
                data: [10,10],
                backgroundColor: ['#26dad2', '#ef5350'],
                label: 'Total'
            }],
            labels: ['Terisi', 'Kosong']
        },
        options: {
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return data.labels[index]+' : '+value+' ('+percentage+'%)';
                    }
                }
            },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                page='formasi-jabatan-eselon';
                status = array[0]._chart.data.labels[array[0]._index].toLowerCase();
                window.location = '/dashboard/jabatan/detail?page='+page+'&status='+status;
            }
        }
    };
    var ctxFormasiJabatanEselon = document.getElementById('chartFormasiJabatanEselon');
    pieFormasiJabatanEselon = new Chart(ctxFormasiJabatanEselon, configFormasiJabatanEselon);
    pieFormasiJabatanEselon.update();

    var configFormasiJabatanNonEselon = {
        type: 'pie',
        data: {
            datasets: [{
                data: [10,10],
                backgroundColor: ['#26dad2', '#ef5350'],
                label: 'Total'
            }],
            labels: ['Terisi', 'Kosong']
        },
        options: {
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return data.labels[index]+' : '+value+' ('+percentage+'%)';
                    }
                }
            },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                page='formasi-jabatan-non-eselon';
                status = array[0]._chart.data.labels[array[0]._index].toLowerCase();
                window.location = '/dashboard/jabatan/detail?page='+page+'&status='+status;
            }
        }
    };
    var ctxFormasiJabatanNonEselon = document.getElementById('chartFormasiJabatanNonEselon');
    pieFormasiJabatanNonEselon = new Chart(ctxFormasiJabatanNonEselon, configFormasiJabatanNonEselon);
    pieFormasiJabatanNonEselon.update();

    var configMasaKerjaJabatanStruktural = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai',
                backgroundColor: '#26dad2',
                data: []
            }]
        },
        options: {
            legend: { display: true, position: 'top', },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Masa Kerja Jabatan (Tahun)',
                        fontStyle : 'bold'
                    },
                }],
                yAxes: [{
                    ticks: { beginAtZero: true }
                }]
            },
            hover: { mode: 'nearest', intersect: false },
            tooltips: {
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return label+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                page='masa-kerja-jabatan-struktural';
                status = array[0]._chart.data.labels[array[0]._index].toLowerCase();
                window.location = '/dashboard/jabatan/detail?page='+page+'&status='+status;
            }
        },
    };
    var ctxMasaKerjaJabatanStruktural = document.getElementById('chartMasaKerjaJabatanStruktural');
    barMasaKerjaJabatanStruktural = new Chart(ctxMasaKerjaJabatanStruktural, configMasaKerjaJabatanStruktural);
    barMasaKerjaJabatanStruktural.update();

    configJabatanStrukturalEselon = {
        type: 'pie',
        data: {
            datasets: [{
                data: [10,10,10],
                backgroundColor: ['#26dad2', '#ffb22b', '#ef5350'],
                label: 'Total'
            }],
            labels: ['II', 'III', 'IV']
        },
        options: {
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return data.labels[index]+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
        }
    };
    var ctxJabatanStrukturalEselon = document.getElementById('chartJabatanStrukturalEselon');
    pieJabatanStrukturalEselon = new Chart(ctxJabatanStrukturalEselon, configJabatanStrukturalEselon);
    pieJabatanStrukturalEselon.update();

    var configPegawaiJenisJabatanDanGolongan = {
        type: 'bar', 
        data: {
            labels: ['label 1'],
            datasets: []
        }, 
        options: {
            hover: { mode: 'nearest', intersect: false },
            tooltips: { mode: 'index', intersect: false, },
            legend : {
                position : 'top',
                labels: {
                    fontStyle: 'bold',
                }
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Jenis Jabatan & Golongan',
                        fontStyle : 'bold'
                    },
                }],
                yAxes: [{
                    stacked: true,
                }]
            },
        }
    };
    var ctxPegawaiJenisJabatanDanGolongan = document.getElementById('chartPegawaiJenisJabatanGolongan');
    barPegawaiJenisJabatanDanGolongan = new Chart(ctxPegawaiJenisJabatanDanGolongan, configPegawaiJenisJabatanDanGolongan);
    barPegawaiJenisJabatanDanGolongan.update();

});