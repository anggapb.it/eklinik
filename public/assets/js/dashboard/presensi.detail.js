$(document).ready(function() {
    window.base_url = document.querySelector('meta[name="base-url"]').getAttribute('content');
    window.csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    axios.defaults.baseURL = window.base_url;
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common['X-CSRF-TOKEN'] = window.csrf_token;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var datasource = $('meta[name="datasource"]').attr('content');
    var table = $('#detailTable').DataTable({
        processing: true,
        serverSide: true,
        paging: true,
        lengthChange: true,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
        ajax: {
            url: datasource,
            method: 'POST',
            data: function(data) {
                data.satuan_kerja = $('meta[name="satuan_kerja"]').attr('content');
                data.status = $('#anomali_status option:selected').val();
            }
        },
        columns: [
            {data: 'DT_RowIndex', name: 'id', orderable : false, searchable : false },
            {data: 'nip', name: 'nip' },
            {data: 'nama', name: 'nama' },
            {data: 'action', name: 'action' },
        ]
    });

    var tableDetail = $('#detailTablePresensi').DataTable({
        pageLength: 5,
        lengthMenu: [5, 10, 15, 20, 30, 50],
        ajax: '',
        columns: [
            {data: 'id'},
            {data: 'checktime'},
            {data: 'modify'},
            {data: 'rank'},
        ],
        columnDefs: [
            {
                targets: [0, 2, 3],
                orderable: false,
            }
        ],
        order: [[1, 'asc']],
    });

    $('#anomali_status').on('change', function(){
        table.ajax.reload(null, false);
    });

    $('#detailTable tbody').on('click', '.btn-modal', function(e){
        var nip = $(this).data('nip');
        var url = window.base_url + '/api/dashboard/presensi/anomali-detail-tabel?nip=' + nip;
        tableDetail.ajax.url(url).load();
        getAnomaliPegawaiDetail(nip);
    });

    async function getAnomaliPegawaiDetail(nip){
        try {
            const response = await axios.get('/api/dashboard/presensi/anomali-detail-pegawai', { params: { nip: nip } });
            const data = response.data.data;
            configAnomaliDetail.data.labels = data.checktime;
            configAnomaliDetail.data.datasets[0].data = data.rank;
            lineAnomaliDetail.update();
            configAnomaliNormalisasiDetail.data.labels = data.checktime;
            configAnomaliNormalisasiDetail.data.datasets[0].data = data.rank_modify;
            lineAnomaliNormalisasiDetail.update();
            configAnomaliGabunganDetail.data.labels = data.checktime;
            configAnomaliGabunganDetail.data.datasets[0].data = data.rank_modify;
            configAnomaliGabunganDetail.data.datasets[1].data = data.rank;
            lineAnomaliGabunganDetail.update();
            $('#anomali-pegawai-detail-nama').text(data.nip+' - '+data.nama);
        } catch (error) {
            console.error(error);
        }
    }

    var configAnomaliDetail = {
        type: 'line',
        data: {
            labels: ['Checktime'],
            datasets: [{
                label: 'Presensi', 
                backgroundColor: '#ffb22b',
                borderColor: '#ffb22b',
                data: [], 
                fill: false,
            }],
        },
        options: {
            legend: {
                display: false,
                position: 'top',
            },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Checktime',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            hover: { mode: 'nearest', intersect: true },
        }
    };
    var ctxAnomaliDetail = document.getElementById('chartAnomaliPegawaiDetail');
    lineAnomaliDetail = new Chart(ctxAnomaliDetail, configAnomaliDetail);
    lineAnomaliDetail.update();

    var configAnomaliNormalisasiDetail = {
        type: 'line',
        data: {
            labels: ['Checktime'],
            datasets: [{
                label: 'Presensi', 
                backgroundColor: '#1976d2',
                borderColor: '#1976d2',
                data: [], 
                fill: false,
            }],
        },
        options: {
            legend: {
                display: false,
                position: 'top',
            },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Checktime',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            hover: { mode: 'nearest', intersect: true },
        }
    };
    var ctxAnomaliNormalisasiDetail = document.getElementById('chartAnomaliPegawaiDetailNormalisasi');
    lineAnomaliNormalisasiDetail = new Chart(ctxAnomaliNormalisasiDetail, configAnomaliNormalisasiDetail);
    lineAnomaliNormalisasiDetail.update();

    var configAnomaliGabunganDetail = {
        type: 'line',
        data: {
            labels: ['Checktime'],
            datasets: [{
                label: 'Normalisasi', 
                backgroundColor: '#1976d2',
                borderColor: '#1976d2',
                data: [], 
                fill: false,
            },{
                label: 'Default', 
                backgroundColor: '#ffb22b',
                borderColor: '#ffb22b',
                data: [], 
                fill: false,
            }],
        },
        options: {
            legend: {
                display: true,
                position: 'top',
            },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Checktime',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            hover: { mode: 'nearest', intersect: true },
        }
    };
    var ctxAnomaliGabunganDetail = document.getElementById('chartAnomaliPegawaiDetailGabungan');
    lineAnomaliGabunganDetail = new Chart(ctxAnomaliGabunganDetail, configAnomaliGabunganDetail);
    lineAnomaliGabunganDetail.update();
});