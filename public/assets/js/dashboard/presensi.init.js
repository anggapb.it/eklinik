$(document).ready(function() {
    window.base_url = document.querySelector('meta[name="base-url"]').getAttribute('content');
    window.csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    axios.defaults.baseURL = window.base_url;
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common['X-CSRF-TOKEN'] = window.csrf_token;

    var peityKehadiranHari = $('#peityKehadiranHari').peity('donut', {
        'width': 60, 'height': 60, 'fill': ['#26dad2', '#f2f2f2']
    });
    var peityKehadiranWaktu = $('#peityKehadiranWaktu').peity('donut', {
        'width': 60, 'height': 60, 'fill': ['#26dad2', '#f2f2f2']
    });
    var peityTanpaKeterangan = $('#peityTanpaKeterangan').peity('donut', {
        'width': 60, 'height': 60, 'fill': ['#ea553d', '#f2f2f2']
    });

    $('#periode_rekap').on('change', function(e){
        periode = $('#periode_rekap option:selected').val();
        getRekapPresensiPeriode(periode);
    });

    $('#presensi_select').on('change', function(e){
        kategori = $('#presensi_select option:selected').val();
        getRiwayatPresensiPerbulan(kategori);
    });

    $('#presensi_select_skpd').on('change', function(){
        kategori = $('#presensi_select_skpd option:selected').val();
        satuan_kerja = $('#skpd_select option:selected').val();
        getRiwayatPresensiPerbulanSKPD(kategori, satuan_kerja);
    });

    $('#skpd_select').on('change', function(){
        kategori = $('#presensi_skpd_select option:selected').val();
        satuan_kerja = $('#skpd_select option:selected').val();
        getRiwayatPresensiPerbulanSKPD(kategori, satuan_kerja);
    });

    getRekapPresensiPeriode(7);
    getRiwayatPresensiPerbulan('kehadiran_hari');
    getRiwayatPresensiPerbulanSKPD('kehadiran_hari', 6);
    getAnomaliPresensi();

    async function getRekapPresensiPeriode(periode) {
        try {
            const response = await axios.get('/api/dashboard/presensi/rekap-presensi-periode', { params: { periode: periode } });
            const data = response.data.data;
            const rekap_tgl = data.rekap_tanggal;
            const rekap_data = data.rekap_data;

            let kehadiranHari = rekap_data[0].kehadiran_hari.toFixed(2);
            let kehadiranWaktu = rekap_data[0].kehadiran_waktu.toFixed(2);
            let tanpaKeterangan = rekap_data[0].tanpa_keterangan.toFixed(2);

            $('#txtKehadiranHari').text(kehadiranHari+' %');
            $('#txtKehadiranWaktu').text(kehadiranWaktu+' %');
            $('#txtTanpaKeterangan').text(tanpaKeterangan+' %');

            peityKehadiranHari.text(kehadiranHari+'/100').change();
            peityKehadiranWaktu.text(kehadiranWaktu+'/100').change();
            peityTanpaKeterangan.text(tanpaKeterangan+'/100').change();
        } catch (error) {
            console.error(error);
        } 
    }

    async function getRiwayatPresensiPerbulan(kategori) {
        try {
            const response = await axios.get('/api/dashboard/presensi/rekap-presensi-perbulan', { params: { kategori: kategori } });
            const data = response.data.data;
            let table = $('#tblRiwayatPresensiPerBulan tbody');
            table.empty();

            for(const[key, value] of Object.entries(data.kategori)){
                row = "<tr><td class='small p-t-0 p-b-5'>"+key+"</td><td class='small p-t-0 p-b-5'>"+value.toFixed(2)+" %</td></tr>";
                table.append(row);
            }

            labels = []; datasets = [];
            for(const[key, value] of Object.entries(data.perbulan)){
                labels.push(window.months[value.bulan]+" "+value.tahun);
                datasets.push(value.presensi);
            }

            configRiwayatPresensiPerbulan.data.labels = labels.reverse();
            configRiwayatPresensiPerbulan.data.datasets[0].data = datasets.reverse();
            lineRiwayatPresensiPerbulan.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getRiwayatPresensiPerbulanSKPD(kategori, satuan_kerja){
        try {
            const response = await axios.get('/api/dashboard/presensi/rekap-presensi-perbulan-skpd', { params: { kategori: kategori, satuan_kerja: satuan_kerja } });
            const data = response.data.data;
            let table = $('#tblRiwayatPresensiPerBulanSKPD tbody');
            table.empty();

            for(const[key, value] of Object.entries(data.kategori)){
                row = "<tr><td class='small p-t-0 p-b-5'>"+key+"</td><td class='small p-t-0 p-b-5'>"+value.toFixed(2)+" %</td></tr>";
                table.append(row);
            }

            labels = []; datasets = [];
            for(const[key, value] of Object.entries(data.perbulan)){
                labels.push(window.months[value.bulan]+" "+value.tahun);
                datasets.push(value.presensi);
            }

            configRiwayatPresensiPerbulanSKPD.data.labels = labels.reverse();
            configRiwayatPresensiPerbulanSKPD.data.datasets[0].data = datasets.reverse();
            lineRiwayatPresensiPerbulanSKPD.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getAnomaliPresensi() {
        try {
            const response = await axios.get('/api/dashboard/presensi/anomali-presensi-skpd-persen');
            const data = response.data.data;
            
            let labels = []; let datasetsNormal = []; let datasetsAnomali = [];
            for (const [index, skpd] of Object.entries(data)) {
                labels.push(skpd.satuan_kerja_nama);
                datasetsAnomali.push(parseFloat(skpd.anomali_persen).toFixed(2));
                datasetsNormal.push(parseFloat(skpd.normal_persen).toFixed(2));
            }

            configAnomaliPresensi.data.labels = labels;
            configAnomaliPresensi.data.datasets[0].data = datasetsNormal;
            configAnomaliPresensi.data.datasets[1].data = datasetsAnomali;
            barAnomaliPresensi.update();
        } catch (error) {
            console.error(error);
        }
    }

    var configRiwayatPresensiPerbulan = {
        type: 'line',
        data: {
            labels: ['Bulan'],
            datasets: [{
                label: 'Presensi', 
                backgroundColor: '#26dad2',
                borderColor: '#26dad2',
                data: [], 
                fill: false,
            }],
        },
        options: {
            legend: { display: false, position: 'top', },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            hover: { mode: 'nearest', intersect: true },
            tooltips: {
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        return label+" : "+value+"%";
                    }
                }
            },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            }
        },
    };
    var ctxRiwayatPresensiPerbulan = document.getElementById('chartRiwayatPresensiPerBulan');
    ctxRiwayatPresensiPerbulan.height = 100;
    lineRiwayatPresensiPerbulan = new Chart(ctxRiwayatPresensiPerbulan, configRiwayatPresensiPerbulan);
    lineRiwayatPresensiPerbulan.update();

    var configRiwayatPresensiPerbulanSKPD = {
        type: 'line',
        data: {
            labels: ['Bulan'],
            datasets: [{
                label: 'Presensi', 
                backgroundColor: '#26dad2',
                borderColor: '#26dad2',
                data: [], 
                fill: false,
            }],
        },
        options: {
            legend: { display: false, position: 'top', },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        callback: function(value, index, values) {
                            return value.substring(0, value.indexOf(' '));
                        }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            hover: { mode: 'nearest', intersect: true },
            tooltips: {
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        return label+" : "+value+"%";
                    }
                }
            },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            }
        },
    };
    var ctxRiwayatPresensiPerbulanSKPD = document.getElementById('chartRiwayatPresensiPerBulanSKPD');
    ctxRiwayatPresensiPerbulanSKPD.height = 100;
    lineRiwayatPresensiPerbulanSKPD = new Chart(ctxRiwayatPresensiPerbulanSKPD, configRiwayatPresensiPerbulanSKPD);
    lineRiwayatPresensiPerbulanSKPD.update();

    var configAnomaliPresensi = {
        type: 'bar', 
        data: {
            labels: ['label 1'],
            datasets: [{
                label : 'Presensi Normal', 
                backgroundColor : '#26dad2', 
                data : [],
            },{
                label : 'Presensi Anomali', 
                backgroundColor : '#ffb22b', 
                data : [],
            }]
        },
        options: {
            hover: { mode: 'index', intersect: false },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var value = data.datasets[datasetIndex].data[index];
                        return label+' : '+value+" %";
                    }
                }
            },
            legend: {
                display: true,
                position: 'top',
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Satuan Kerja',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value.substring(0, 10)+' ...';
                        }
                    }
                }],
                yAxes: [{
                    stacked: true,
                }]
            },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                index = array[0]._index;
                datasetIndex = array[0]._datasetIndex;
                satuanKerja = array[0]._chart.data.labels[index];
                window.location = '/dashboard/presensi/detail?satuan_kerja='+satuanKerja;
            }
        }
    };
    var ctxAnomaliPresensi = document.getElementById('chartAnomaliPresensi');
    ctxAnomaliPresensi.height = 100;
    barAnomaliPresensi = new Chart(ctxAnomaliPresensi, configAnomaliPresensi);
    barAnomaliPresensi.update();
});