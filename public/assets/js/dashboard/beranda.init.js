$(document).ready(function() {
    window.base_url = document.querySelector('meta[name="base-url"]').getAttribute('content');
    window.csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    axios.defaults.baseURL = window.base_url;
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common['X-CSRF-TOKEN'] = window.csrf_token;

    getTotalPegawai();
    getPegawaiUmur();
    getPegawaiJenisKelamin();
    getPegawaiTingkatPendidikan();
    getPegawaiGolongan();
    getPegawaiJenisJabatanDanEselon();

    async function getTotalPegawai() {
        try {
            const response = await axios.get('/api/dashboard/pegawai/jumlah-pegawai');
            const data = response.data.data[0];
            
            $('#txtTotalPegawai').text(data.pegawai);
            $('#txtTotalPegawaiStruktural').text(data.pegawai_struktural);
            $('#txtTotalPegawaiFungsionalTertentu').text(data.pegawai_fungsional_tertentu);
            $('#txtTotalPegawaiFungsionalUmum').text(data.pegawai_fungsional_umum);
        } catch (error) {
            console.error(error);
        }
    }

    async function getPegawaiUmur() {
        try {
            const response = await axios.get('/api/dashboard/pegawai/jumlah-kelompok-umur');
            const data = response.data.data[0];
            let table = $('#tblPegawaiKelompokUmur tbody');
            table.empty();

            let labels = []; let datasets = [];
            for (const [key, value] of Object.entries(data)) {
                let row = "<tr><td class='small p-t-0 p-b-5'>"+key.toUpperCase()+"</td><td class='small p-t-0 p-b-5'>"+value+"</td></tr>";
                table.append(row); labels.push(key); datasets.push(value);
            }

            configPegawaiUmur.data.labels = labels;
            configPegawaiUmur.data.datasets[0].data = datasets;
            barPegawaiUmur.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPegawaiJenisKelamin() {
        try {
            const response = await axios.get('/api/dashboard/pegawai/jumlah-kelompok-jenis-kelamin');
            const data = response.data.data[0];
            
            $('#txtPegawaiLakilaki').text(data.laki_laki);
            $('#txtPegawaiPerempuan').text(data.perempuan);

            configPegawaiJenisKelamin.data.datasets[0].data = [data.laki_laki, data.perempuan];
            piePegawaiJenisKelamin.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPegawaiTingkatPendidikan() {
        try {
            const response = await axios.get('/api/dashboard/pendidikan/jumlah-tingkat-pendidikan');
            const data = response.data.data;
            let table = $('#tblPegawaiTingkatPendidikan tbody');
            table.empty();

            let labels = []; let datasets = [];
            for (const [key, value] of Object.entries(data)) {
                let row = "<tr><td class='small p-t-0 p-b-5'>"+value.tingkat_pendidikan+"</td><td class='small p-t-0 p-b-5'>"+value.count+"</td></tr>";
                table.append(row); labels.push(value.tingkat_pendidikan); datasets.push(value.count);
            }

            configPegawaiTingkatPendidikan.data.labels = labels;
            configPegawaiTingkatPendidikan.data.datasets[0].data = datasets;
            barPegawaiTingkatPendidikan.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPegawaiGolongan() {
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-golongan');
            const data = response.data.data;

            $('#txtGolI').text(data[0].total); $('#txtGolII').text(data[1].total); $('#txtGolIII').text(data[2].total); $('#txtGolIV').text(data[3].total);

            configPegawaiGolongan.data.datasets[0].data = [data[0].total, data[1].total, data[2].total, data[3].total];
            piePegawaiGolongan.update();
        } catch (error) {
            console.error(error);
        }
    }

    async function getPegawaiJenisJabatanDanEselon() {
        try {
            const response = await axios.get('/api/dashboard/jabatan/jumlah-jenis-jabatan-dan-eselon');
            const data = response.data.data;
            let table = $('#tblPegawaiJenisJabatanEselon tbody');
            table.empty();
            
            let labels = []; let datasets = [];
            for (const [index, item] of Object.entries(data)) {
                row = "<tr><td class='small p-t-0 p-b-5'>"+item.jenis_jabatan+"</td>"+"<td class='small p-t-0 p-b-5'>"+item.kategori+"</td>"+"<td class='small p-t-0 p-b-5'>"+(item.formasi ? item.formasi : '')+"</td>"+"<td class='small p-t-0 p-b-5'>"+item.total+"</td>"+"</tr>";
                table.append(row); labels.push(item.kategori); datasets.push(item.total);
            }

            configPegawaiJenisJabatanDanEselon.data.labels = labels;
            configPegawaiJenisJabatanDanEselon.data.datasets[0].data = datasets;
            barPegawaiJenisJabatanDanEselon.update();
        } catch (error) {
            console.error(error);
        }
    }

    var configPegawaiUmur = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai',
                backgroundColor: '#26dad2',
                data: []
            }]
        },
        options: {
            legend: { display: true, position: 'top', },
            title: { display: false, },
            scales: {
                xAxes: [{
                    stacked: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Umur (Tahun)',
                        fontStyle : 'bold'
                    },
                }],
                yAxes: [{
                    ticks: { beginAtZero: true }
                }]
            },
            hover: { mode: 'nearest', intersect: false },
            tooltips: {
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return a + b;
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return label+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
        },
    };
    var ctxPegawaiUmur = document.getElementById('chartPegawaiKelompokUmur');
    barPegawaiUmur = new Chart(ctxPegawaiUmur, configPegawaiUmur);
    barPegawaiUmur.update();

    var configPegawaiJenisKelamin = {
        type: 'pie',
        data: {
            datasets: [{
                data: [10,10],
                backgroundColor: ['#26dad2', '#ef5350'],
                label: 'Total Pegawai'
            }],
            labels: ['Laki-Laki', 'Perempuan']
        },
        options: {
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return data.labels[index]+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
        }
    };
    var ctxPegawaiJenisKelamin = document.getElementById('chartPegawaiJenisKelamin');
    piePegawaiJenisKelamin = new Chart(ctxPegawaiJenisKelamin, configPegawaiJenisKelamin);
    piePegawaiJenisKelamin.update();

    var configPegawaiTingkatPendidikan = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai',
                backgroundColor: '#26dad2',
                data: []
            }]
        },
        options: {
            legend: { display: true, position: 'top', },
            title: { display: false, },
            hover: { mode: 'nearest', intersect: false },
            scales: {
                xAxes: [{
                    stacked: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Tingkat Pendidikan',
                        fontStyle : 'bold'
                    },
                }],
                yAxes: [{
                    ticks: { beginAtZero: true }
                }]
            },
            tooltips: {
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return a + b;
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return label+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
        },
    };
    var ctxPegawaiTingkatPendidikan = document.getElementById('chartPegawaiTingkatPendidikan');
    barPegawaiTingkatPendidikan = new Chart(ctxPegawaiTingkatPendidikan, configPegawaiTingkatPendidikan);
    barPegawaiTingkatPendidikan.update();

    var configPegawaiGolongan = {
        type: 'pie',
        data: {
            datasets: [{
                data: [10,10,10,10],
                backgroundColor: ['#007bff', '#1976d2', '#26dad2', '#ef5350'],
                label: 'Total Pegawai'
            }],
            labels: ['Gol. I', 'Gol. II', 'Gol. III', 'Gol. IV']
        },
        options: {
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return data.labels[index]+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
        }
    };
    var ctxPegawaiGolongan = document.getElementById('chartPegawaiGolongan');
    piePegawaiGolongan = new Chart(ctxPegawaiGolongan, configPegawaiGolongan);
    piePegawaiGolongan.update();

    var configPegawaiJenisJabatanDanEselon = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Total Pegawai',
                backgroundColor: '#26dad2',
                data: []
            }]
        },
        options: {
            legend: { display: true, position: 'top', },
            title: { display: false, },
            hover: { mode: 'nearest', intersect: false },
            scales: {
                xAxes: [{
                    stacked: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Jenis Jabatan & Eselon',
                        fontStyle : 'bold'
                    },
                    // label for xAxes (vertical)
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90,
                        callback: function(value, index, values) {
                            return value;
                        }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            tooltips: {
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var index = tooltipItem.index;
                        var datasetIndex = tooltipItem.datasetIndex;
                        var label = data.datasets[datasetIndex].label;
                        var values = data.datasets[datasetIndex].data;
                        var value = data.datasets[datasetIndex].data[index];
                        var sum = values.reduce(function(a, b){
                            return parseInt(a) + parseInt(b);
                        });
                        var percentage = ((value / sum) * 100).toFixed(2);
                        return label+' : '+value+" ("+percentage+"%)";
                    }
                }
            },
            onHover: function(event) {
                var point = this.getElementAtEvent(event);
                event.target.style.cursor = (point.length) ? 'pointer' : 'default';
            },
            onClick : function(event, array) {
                index = array[0]._index;
                datasetIndex = array[0]._datasetIndex;
                console.log(index, datasetIndex);
                console.log(array[0]._chart.data.datasets[datasetIndex].data[index]);
                console.log(array[0]._chart.data.labels[index]);
            }
        },
    };
    var ctxPegawaiJenisJabatanDanEselon = document.getElementById('chartPegawaiJenisJabatanEselon');
    barPegawaiJenisJabatanDanEselon = new Chart(ctxPegawaiJenisJabatanDanEselon, configPegawaiJenisJabatanDanEselon);
    barPegawaiJenisJabatanDanEselon.update();

});