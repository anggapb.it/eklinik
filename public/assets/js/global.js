var base_url        = window.location.origin + "/";
var complete_url 	= (window.location + "/").replace(new RegExp("#", "g"), "").replace(window.location.search, '');
var method, url, tableData;

// Sweealert2
function swalLoading(text = ''){
    var swalText = (text) ? text : 'Sedang memproses permintaan anda.';
    Swal.fire({
        title: 'Harap tunggu',
        text: swalText,
        allowOutsideClick: false,
        allowEscapeKey: false,
        onBeforeOpen: function(e){
            Swal.showLoading();
        }
    });
}

function swalSuccess(title, text){
    Swal.fire({
        title: title,
        html: text,
        type: "success",
        timer: 1500,
        showConfirmButton:false
    });
}

function swalClose(){
    Swal.close();
}

function swalWarning(title, text){
    Swal.fire({
        title: title,
        html: text,
        type: "warning"
    });
}

function swalError(){
    Swal.fire({
        type: "error",
        title: "Kesalahan!",
        html: "Tidak dapat memproses permintaan anda.",
        allowOutsideClick: false,
        allowEscapeKey: false,
    });
}

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});

{
  const templates = {};

  window.getTemplate = function (id) {
    if (!templates[ id ]) {
      const eleTpl = document.getElementById(id);

      templates[ id ] = eleTpl.innerHTML;
    }

    return templates[ id ];
  };
}
